SampleApp::Application.routes.draw do

  resources :sessions, :only => [:new, :create, :destroy]
  resources :users do
    member do
      get :archive
      get :general_settings
      get :ebay_settings
      get :subscribe_settings
    end
  end
  resources :ebay_users, :only => [:new, :create, :destroy] do
       collection do
         get 'complete'
       end
  end
  resources :orders, :only => [:new, :edit, :update, :create, :destroy] do
    member do
      get :complete
    end
  end
  resources :forgot, :only => [:new, :show, :create, :update]
  resource :inbox, :controller => 'inbox', :only => [:show,:create]
  resource :track_updates, :controller => 'track_updates', :only => [:show,:create]
  resources :subscribers, :only => [:create] do
    member do
      get :unsubscribe
    end
  end

  root :to => 'pages#home'
  match '/contact', :to => 'pages#contact'
  match '/about', :to => 'pages#about'
  match '/help', :to => 'pages#help'
  match '/news', :to => 'pages#news'
  match '/terms', :to => 'pages#terms'
  match '/android_app', :to => 'pages#android_app'
  match '/ios_app', :to => 'pages#ios_app'
  match '/signup', :to => 'users#new'
  match '/signin', :to => 'sessions#new'
  match '/signin_lightbox', :to => 'sessions#new_lightbox'
  match '/signout', :to => 'sessions#destroy'
  match '/beta_thank_you', :to => 'pages#beta_thank_you'
  match 'bookmarklet_explain', :to => 'pages#bookmarklet_explain'

  match '/orders/save_order', :to => 'orders#save_order'
  match '/orders/order_saved', :to => 'orders#order_saved_msg'
  match '/orders/order_saved_chrome_ext', :to => 'orders#order_saved_msg_chrome_ext'
  match '/chrome_ext', :to => "pages#chrome_ext"
  match '/.well-known/acme-challenge/*pass', :to => "pages#letsencrypt_check"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
