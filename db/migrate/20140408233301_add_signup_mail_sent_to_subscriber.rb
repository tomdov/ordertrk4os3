class AddSignupMailSentToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :signup_mail_sent, :boolean, :default => false
  end
end
