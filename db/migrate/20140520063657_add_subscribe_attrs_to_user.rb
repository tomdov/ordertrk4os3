class AddSubscribeAttrsToUser < ActiveRecord::Migration
  def change
    add_column :users, :orders_notifications, :boolean, :default => true
    add_column :users, :messages_notifications, :boolean, :default => true
    add_column :users, :unsubscribe_leading_mail, :text
  end
end
