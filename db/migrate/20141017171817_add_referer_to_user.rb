class AddRefererToUser < ActiveRecord::Migration
  def change
    add_column :users, :referer, :text, :default => 'none'
  end
end
