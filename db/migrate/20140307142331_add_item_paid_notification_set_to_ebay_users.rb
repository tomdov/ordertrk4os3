class AddItemPaidNotificationSetToEbayUsers < ActiveRecord::Migration
  def change
    add_column :ebay_users, :item_paid_notification_set, :boolean, :default => false
  end
end
