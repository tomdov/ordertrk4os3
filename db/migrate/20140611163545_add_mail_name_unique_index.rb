class AddMailNameUniqueIndex < ActiveRecord::Migration
  def up
	  add_index :user_mails, :name, :unique => true
  end

  def down
	  remove_index :user_mails, :name
  end
end
