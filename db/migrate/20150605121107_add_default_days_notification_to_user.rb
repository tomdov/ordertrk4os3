class AddDefaultDaysNotificationToUser < ActiveRecord::Migration
  def change
    add_column :users, :default_days_notification, :integer
  end
end
