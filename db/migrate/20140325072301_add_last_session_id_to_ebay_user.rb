class AddLastSessionIdToEbayUser < ActiveRecord::Migration
  def change
    add_column :ebay_users, :last_session_id, :text
    add_column :ebay_users, :last_session_data, :text
    add_column :ebay_users, :last_login_time, :datetime
  end
end
