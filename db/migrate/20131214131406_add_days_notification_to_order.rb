class AddDaysNotificationToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :days_notification, :integer, :default => 0
    add_column :orders, :days_notification_sent, :boolean, :default => false
  end
end
