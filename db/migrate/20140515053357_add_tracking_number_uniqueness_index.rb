class AddTrackingNumberUniquenessIndex < ActiveRecord::Migration
  def up
	add_index :package_trackings, :tracking_number, :unique => true
  end

  def down
	remove_index :package_trackings, :tracking_number
  end
end
