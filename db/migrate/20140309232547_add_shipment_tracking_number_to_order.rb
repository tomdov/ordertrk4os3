class AddShipmentTrackingNumberToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :shipment_tracking_number, :string
    add_column :orders, :shipping_carrier, :string
  end
end
