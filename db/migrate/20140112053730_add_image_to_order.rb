class AddImageToOrder < ActiveRecord::Migration
  def change
    add_attachment :orders, :image
  end
end
