class AddUnsubscribeToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :key, :string
    add_column :subscribers, :subscribed, :boolean, :default => true
  end
end
