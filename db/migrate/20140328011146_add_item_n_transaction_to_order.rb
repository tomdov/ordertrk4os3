class AddItemNTransactionToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :ebay_item_n_transaction, :string
  end
end
