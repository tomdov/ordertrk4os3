class AddItemShippedNotificationSetToEbayUsers < ActiveRecord::Migration
  def change
    add_column :ebay_users, :item_shipped_notification_set, :boolean, :default => false
  end
end
