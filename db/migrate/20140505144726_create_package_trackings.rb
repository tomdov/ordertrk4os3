class CreatePackageTrackings < ActiveRecord::Migration
  def change
    create_table :package_trackings do |t|
      t.integer :order_id
      t.string :tracking_number
      t.string :tracking_carrier

      t.timestamps
    end
  end
end
