class RemoveUniqenessPackageTracking < ActiveRecord::Migration
  def change
	remove_index :package_trackings, :tracking_number
	add_index :package_trackings, :tracking_number, :unique => false
  end
end


