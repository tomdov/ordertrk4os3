class CreateEbayUsers < ActiveRecord::Migration
  def change
    create_table :ebay_users do |t|
      t.string :name
      t.text :token
      t.timestamp :expiration
      t.integer :user_id

      t.timestamps
    end
  end
end
