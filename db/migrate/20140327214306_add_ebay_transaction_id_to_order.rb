class AddEbayTransactionIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :ebay_transaction_id, :string
    add_column :orders, :ebay_item_id, :string
    add_column :orders, :ebay_item_and_transaction, :string
  end
end
