class CreateUserMails < ActiveRecord::Migration
  def change
    create_table :user_mails do |t|
      t.text :name

      t.timestamps
    end

    create_table :user_mails_users do |t|
      t.integer :user_mail_id
      t.integer :user_id
    end
  end
end
