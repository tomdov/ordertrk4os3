class AddEbayItemNTransactionUniqueIndex < ActiveRecord::Migration
  def up
    add_index :orders, :ebay_item_n_transaction, :unique => true
  end

  def down
    remove_index :orders, :ebay_item_n_transaction
  end
end
