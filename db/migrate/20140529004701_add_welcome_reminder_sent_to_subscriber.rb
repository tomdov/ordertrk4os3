class AddWelcomeReminderSentToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :welcome_reminder_sent, :boolean, :default => false
  end
end
