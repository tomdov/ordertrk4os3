class AddOrderIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :ebay_order_id, :string, :default => "0"
  end
end
