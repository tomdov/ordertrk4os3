class AddAllowTrackingInfoToUser < ActiveRecord::Migration
  def change
    add_column :users, :allow_tracking_info, :boolean, :default => false
  end
end
