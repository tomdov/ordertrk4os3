class AddLastGetOrdersTimeToEbayUser < ActiveRecord::Migration
  def change
    add_column :ebay_users, :last_get_orders_time, :datetime
  end
end
