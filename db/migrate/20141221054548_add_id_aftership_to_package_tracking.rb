class AddIdAftershipToPackageTracking < ActiveRecord::Migration
  def change
    add_column :package_trackings, :id_aftership, :string
  end
end
