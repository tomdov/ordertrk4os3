class CreateForgotKeys < ActiveRecord::Migration
  def change
    create_table :forgot_keys do |t|
      t.string :key
      t.integer :user_id

      t.timestamps
    end

    add_index :forgot_keys, :created_at
    add_index :forgot_keys, :user_id
    add_index :forgot_keys, :key
  end
end
