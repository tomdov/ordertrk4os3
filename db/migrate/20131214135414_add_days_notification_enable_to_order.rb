class AddDaysNotificationEnableToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :days_notification_enable, :boolean, :default => false
  end
end
