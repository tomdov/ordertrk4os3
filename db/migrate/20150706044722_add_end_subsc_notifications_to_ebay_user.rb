class AddEndSubscNotificationsToEbayUser < ActiveRecord::Migration
  def change
    add_column :ebay_users, :end_subsc_two_weeks_notification, :boolean, :default => false
    add_column :ebay_users, :end_subsc_one_week_notification, :boolean, :default => false
  end
end
