class AddAftershipCheckpointsToPackageTracking < ActiveRecord::Migration
  def change
    add_column :package_trackings, :aftership_checkpoints, :integer, :default => 0
  end
end
