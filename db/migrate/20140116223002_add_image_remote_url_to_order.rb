class AddImageRemoteUrlToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :image_remote_url, :text
  end
end
