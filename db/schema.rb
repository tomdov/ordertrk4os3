# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150706044722) do

  create_table "ebay_users", :force => true do |t|
    t.string   "name"
    t.text     "token"
    t.datetime "expiration"
    t.integer  "user_id"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.boolean  "item_paid_notification_set",       :default => false
    t.boolean  "item_shipped_notification_set",    :default => false
    t.text     "last_session_id"
    t.text     "last_session_data"
    t.datetime "last_login_time"
    t.datetime "last_get_orders_time"
    t.boolean  "end_subsc_two_weeks_notification", :default => false
    t.boolean  "end_subsc_one_week_notification",  :default => false
  end

  create_table "forgot_keys", :force => true do |t|
    t.string   "key"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "forgot_keys", ["created_at"], :name => "index_forgot_keys_on_created_at"
  add_index "forgot_keys", ["key"], :name => "index_forgot_keys_on_key"
  add_index "forgot_keys", ["user_id"], :name => "index_forgot_keys_on_user_id"

  create_table "orders", :force => true do |t|
    t.text     "description"
    t.text     "site"
    t.date     "purchase_date"
    t.string   "status"
    t.date     "status_date"
    t.text     "notes"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "price"
    t.integer  "user_id"
    t.integer  "days_notification",        :default => 0
    t.boolean  "days_notification_sent",   :default => false
    t.boolean  "days_notification_enable", :default => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "image_remote_url"
    t.string   "shipment_tracking_number"
    t.string   "shipping_carrier"
    t.string   "ebay_order_id",            :default => "0"
    t.string   "ebay_transaction_id"
    t.string   "ebay_item_id"
    t.string   "ebay_item_n_transaction"
  end

  add_index "orders", ["created_at"], :name => "index_orders_on_created_at"
  add_index "orders", ["ebay_item_n_transaction"], :name => "index_orders_on_ebay_item_n_transaction", :unique => true
  add_index "orders", ["user_id"], :name => "index_orders_on_user_id"

  create_table "package_trackings", :force => true do |t|
    t.integer  "order_id"
    t.string   "tracking_number"
    t.string   "tracking_carrier"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "aftership_checkpoints", :default => 0
    t.string   "id_aftership"
  end

  add_index "package_trackings", ["tracking_number"], :name => "index_package_trackings_on_tracking_number"

  create_table "subscribers", :force => true do |t|
    t.string   "email"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "signup_mail_sent",      :default => false
    t.string   "key"
    t.boolean  "subscribed",            :default => true
    t.boolean  "welcome_reminder_sent", :default => false
  end

  create_table "user_mails", :force => true do |t|
    t.text     "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_mails", ["name"], :name => "index_user_mails_on_name", :unique => true

  create_table "user_mails_users", :force => true do |t|
    t.integer "user_mail_id"
    t.integer "user_id"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "encrypted_password"
    t.string   "salt"
    t.boolean  "admin",                     :default => false
    t.boolean  "orders_notifications",      :default => true
    t.boolean  "messages_notifications",    :default => true
    t.text     "unsubscribe_leading_mail"
    t.text     "referer",                   :default => "none"
    t.boolean  "allow_tracking_info",       :default => false
    t.integer  "default_days_notification"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

end
