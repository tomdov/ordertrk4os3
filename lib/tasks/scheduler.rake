desc "This task is called by the Heroku scheduler add-on"

task :send_reminders => :environment do
  Order.send_days_notifications
end

task :poll_ebay_user_alerts => :environment do
  EbayUser.update
end

task :send_welcome_mails_tp_subscribers, [:all_time] => :environment do |task_name, args|
  if (args[:all_time] != nil  || Date.current.wday == 6)
    Subscriber.send_welcome_mails
  end
end

task :add_keys_for_subscribers => :environment do
  Subscriber.add_unsubscribe_keys
end

task :send_welcome_mails_reminders_to_subscribers => :environment do
  Subscriber.send_welcome_reminders
end

task :send_mail_to_users, [:mail_name,:max_sends] => :environment do |task_name, args|
  UserMail.send_mail_to_users(args[:mail_name], args[:max_sends].to_i)
end

task :send_end_of_subscription_mails_reminders_to_ebay_user => :environment do
  EbayUser.send_end_subsription_mail
end
