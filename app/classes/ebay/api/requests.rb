require 'ebay/api/classes_list'
require 'ebay/api/config_api'

module EbayAPI
  module Requests

    def login_url
      EbayAPI::ConfigAPI.instance.client_alerts_api_url
    end

    def logout_url
      EbayAPI::ConfigAPI.instance.client_alerts_api_url
    end

    def fetch_token_url
      EbayAPI::ConfigAPI.instance.client_alerts_api_url
    end

    def get_user_alerts_url
      EbayAPI::ConfigAPI.instance.client_alerts_api_url
    end

    def get_single_item_url
      EbayAPI::ConfigAPI.instance.shopping_api_url
    end

    def initialize_options
      hash = {}
      hash
    end

    def add_url_to_options(options, url)
      options[:url] = url
      options
    end

    def add_json_forcing(options)
      options[:force_json] = true
      options
    end


    def set_notification_preferences(params = {})
      commit(:POST, EbayAPI::Requests::SetNotificationPreferences, params)
    end

    def get_notification_preferences(params = {})
      commit(:POST, EbayAPI::Requests::GetNotificationPreferences, params)
    end

    def get_client_alerts_auth_token(params = {})
      commit(:POST, EbayAPI::Requests::GetClientAlertsAuthToken, params)
    end

    def get_ebay_official_time(params = {})
      commit(:POST, EbayAPI::Requests::GeteBayOfficialTime, params)
    end

    def login(params = {})
      options = initialize_options
      options = add_url_to_options(options, login_url)
      commit(:GET, EbayAPI::Requests::Login, params, options)
    end

    def logout(params = {})
      options = initialize_options
      options = add_url_to_options(options, logout_url)
      commit(:GET, EbayAPI::Requests::Logout, params, options)
    end

    def get_session_id(params = {})
      commit(:POST, EbayAPI::Requests::GetSessionID, params)
    end

    def fetch_token(params = {})
      options = initialize_options
      options = add_url_to_options(options, fetch_token_url)
      commit(:POST, EbayAPI::Requests::FetchToken, params, options)
    end

    def get_user_alerts(params = {})
      options = initialize_options
      options = add_url_to_options(options, get_user_alerts_url)
      commit(:GET, EbayAPI::Requests::GetUserAlerts, params, options)
    end

    def get_single_item(params = {})
      options = initialize_options
      options = add_url_to_options(options, get_single_item_url)
      commit(:GET, EbayAPI::Requests::GetSingleItem, params, options)
    end

    def get_orders(params = {})
      commit(:POST, EbayAPI::Requests::GetOrders, params)
    end
  end
end