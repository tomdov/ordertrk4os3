require 'ebay/api/responses/abstract'
require 'ebay/api/initializer'
require 'ebay/api/classes/application_delivery_preferences'

module EbayAPI # :nodoc:
  module Responses # :nodoc:
                   # == Attributes
    class SetNotificationPreferences < Abstract
      include XML::Mapping
      include Initializer

      object_node :application_delivery_preferences, 'ApplicationDeliveryPreferences', :class => ApplicationDeliveryPreferences, :optional => true
      root_element_name 'SetNotificationPreferencesResponse'
    end
  end
end