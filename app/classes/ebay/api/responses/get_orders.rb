require 'ebay/api/requests/abstract'
require 'ebay/api/classes/order'


module EbayAPI # :nodoc:
  module Responses # :nodoc:
    # == Attributes
    #  boolean_node :has_more_orders, 'HasMoreOrders', 'true', 'false', :optional => true
    #  array_node :orders, 'OrderArray', 'Order', :class => Order, :default_value => []
    #  numeric_node :orders_per_page, 'OrdersPerPage', :optional => true
    #  numeric_node :page_number, 'PageNumber', :optional => true
    #  numeric_node :returned_order_count_actual, 'ReturnedOrderCountActual', :optional => true
    class GetOrders < Abstract
      include XML::Mapping
      include Initializer
      root_element_name 'GetOrdersResponse'
      boolean_node :has_more_orders, 'HasMoreOrders', 'true', 'false', :optional => true
      array_node :orders, 'OrderArray', 'Order', :class => Order, :default_value => []
      numeric_node :orders_per_page, 'OrdersPerPage', :optional => true
      numeric_node :page_number, 'PageNumber', :optional => true
      numeric_node :returned_order_count_actual, 'ReturnedOrderCountActual', :optional => true
    end
  end
end


