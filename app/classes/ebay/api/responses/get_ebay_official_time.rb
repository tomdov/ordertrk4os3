require 'ebay/api/responses/abstract'
require 'ebay/api/initializer'

module EbayAPI # :nodoc:
  module Responses # :nodoc:
    # == Attributes
    class GeteBayOfficialTime < Abstract
      include XML::Mapping
      include Initializer
      root_element_name 'GeteBayOfficialTimeResponse'
    end
  end
end


