require 'singleton'

module EbayAPI
  class ConfigAPI
    include Singleton

    def initialize
      if !@config_file
        @config_file  = YAML.load_file('config/ebay.yml')
        @env_settings = @config_file[Rails.env]
      end
    end

    def schema_version
      @env_settings["schema_version"]
    end

    def client_alerts_api_url
      @env_settings["client_alerts_api_url"]
    end

    def shopping_api_url
      @env_settings["shopping_api_url"]
    end

    def ru_name
      @env_settings["ru_name"]
    end

    def signin_address
      @env_settings["signin_address"]
    end

    def auth_token
      @env_settings["auth_token"]
    end


  end


end