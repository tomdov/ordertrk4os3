require 'ebay/api/initializer'

module EbayAPI # :nodoc:
  module Classes # :nodoc:
               # == Attributes
               #  text_node :event_type, 'EventType', :optional => true
               #  text_node :event_enable, 'EventEnable', :optional => true, :default_value => true
               #  text_node :name, 'Name', :optional => true
               #  text_node :value, 'Value', :optional => true
    class NotificationEnable
      include XML::Mapping
      include Initializer
      root_element_name 'NotificationEnable'
      text_node :event_type, 'EventType', :optional => true
      text_node :event_enable, 'EventEnable', :optional => true, :default_value => true
      text_node :name, 'Name', :optional => true
      text_node :value, 'Value', :optional => true
    end
  end
end

