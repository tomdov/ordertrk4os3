require 'ebay/api/classes/notification_enable'
require 'ebay/api/initializer'

module EbayAPI # :nodoc:
  module Classes # :nodoc:
               # == Attributes
               #  object_node :notification_enable, 'NotificationEnable', :class => NotificationEnable
               #  text_node :name, 'Name', :optional => true
               #  text_node :value, 'Value', :optional => true
    class UserDeliveryPreferenceArray
      include XML::Mapping
      include Initializer
      root_element_name 'UserDeliveryPreferenceArray'
      array_node :notification_enable, 'NotificationEnable', :class => NotificationEnable
      text_node :name, 'Name', :optional => true
      text_node :value, 'Value', :optional => true
    end
  end
end

