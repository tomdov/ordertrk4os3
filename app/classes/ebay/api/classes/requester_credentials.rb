require 'ebay/api/initializer'

module EbayAPI # :nodoc:
  module Classes # :nodoc:
               # == Attributes
               # text_node :ebay_auth_token, 'ebayAuthToken', :optional => true
    class RequesterCredentials
      include XML::Mapping
      include EbayAPI::Initializer
      root_element_name 'RequesterCredentials'
      text_node :ebay_auth_token, 'eBayAuthToken', :optional => true
    end
  end
end

