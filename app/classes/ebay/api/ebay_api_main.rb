require 'uri'
require 'zlib'
require 'stringio'
require 'ebay/connection/connection.rb'
require 'ebay/api/requests'
require 'ebay/api/requests_list'
require 'ebay/api/responses_list'
require 'ebay/api/support/xml_mapping'
require 'ebay/api/config_api'

#require 'ebay/api_methods'

module EbayAPI #:nodoc:
  class EbayError < StandardError #:nodoc:
  end

  class RequestError < EbayError #:nodoc:
    attr_reader :errors
    def initialize(errors)
      @errors = errors
    end
  end

  # == Overview
  # Api is the main proxy class responsible for instantiating and invoking
  # the correct Ebay::Requests object for the method called.
  # All of the available method calls are included from the module Ebay::ApiMethods
  #   ebay = Ebay::Api.new
  #   response = ebay.get_ebay_official_time
  #   puts response.timestamp # => 2006-08-13T21:28:39.515Z
  #
  # All Ebay API calls have a corresponding request and response object.
  # In the example above the request objects is
  # Ebay::Requests::GeteBayOfficialTime and the response object is
  # Ebay::Responses::GeteBayOfficialTime
  #
  # == Official Input / Output Reference
  # The official input / output reference provided by eBay is a good way to get familiar
  # with the API calls.
  #
  # http://developer.ebay.com/DevZone/XML/docs/Reference/eBay/index.html
  class Api
    #include Inflections
    include Requests
    #require 'ebay/api/requests'
    XmlNs = 'urn:ebay:apis:eBLBaseComponents'

    #
    ## Simply yields the Ebay::Api class itself.  This makes configuration a bit nicer looking:
    ##
    ##  Ebay::Api.configure do |ebay|
    ##    ebay.auth_token = 'YOUR AUTH TOKEN HERE'
    ##    ebay.dev_id = 'YOUR DEVELOPER ID HERE'
    ##    ebay.app_id = 'YOUR APPLICATION ID HERE'
    ##    ebay.cert = 'YOUR CERTIFICATE HERE'
    ##
    ##  # The default environment is the production environment
    ##  # Override by setting use_sandbox to true
    ##    ebay.use_sandbox = true
    ##  end
    #def self.configure
    #  yield self if block_given?
    #end

    # The schema version the API client is currently using
    def schema_version
      EbayAPI::ConfigAPI.instance.schema_version
    end

    def auth_token
      @auth_token
    end

    def init_params
      configFile  = YAML.load_file('config/ebay.yml')
      @dev_id     = get_dev_id(configFile)
      @app_id     = get_app_id(configFile)
      @cert       = get_cert(configFile)
      @username   = get_username(configFile)
      @password   = get_password(configFile)
      @auth_token = (Rails.env != 'production') ? get_auth_token(configFile) : nil
      @request_uri = URI.parse(get_request_uri(configFile))
    end

    # With no options, the default is to use the default site_id and the default
    # auth_token configured on the Api class.
    #   ebay = Ebay::Api.new
    #
    # However, another user's auth_token can be used and the site can be selected
    # at the time of creation. Ex: Canada(Site 2) with another user's auth token.
    #   ebay = Ebay::Api.new(:site_id => 2, :auth_token => 'TEST')
    def initialize(options = {})
      init_params
      @format = options[:format] || :object
      @site_id = options[:site_id] || 0
    end

    private

    def get_dev_id(configFile)
      configFile[Rails.env]["dev_id"]
    end

    def get_app_id(configFile)
      configFile[Rails.env]["app_id"]
    end

    def get_cert(configFile)
      configFile[Rails.env]["cert"]
    end

    def get_username(configFile)
      configFile[Rails.env]["username"]
    end

    def get_password(configFile)
      configFile[Rails.env]["password"]
    end

    def get_request_uri(configFile)
      configFile[Rails.env]["request_url"]
    end

    def get_auth_token(configFile)
      configFile[Rails.env]["auth_token"]
    end

    def get_client_alerts_api_url(configFile)
      configFile[Rails.env]["client_alerts_api_url"]
    end

    def commit(method, request_class, params, options = nil)
      format = params.delete(:format) || @format

      params[:username] = @username
      params[:password] = @password
      #params[:auth_token] = @auth_token

      request = request_class.new(params)
      yield request if block_given?
      invoke(method, request, format, options)
    end

    def invoke(method, request, format, options)
      case method
      when :POST
        response = connection(@request_uri, true).post( @request_uri.path,
                                  build_body(request),
                                  build_headers(request.call_name))
      when :GET
        uri = URI.parse(options[:url])
        response = connection(uri, true).get(uri.path + "?" + request.build_param_str.to_s,
                                    nil,
                                    build_get_headers)
      end

      descompressed_response = decompress(response)
      puts "decompressed response == " + descompressed_response
      if method == :POST || options[:force_json] == true
        parse descompressed_response, format
      elsif method == :GET
        JSON.parse(descompressed_response)
      end

    end

    def convert_xml_to_json(xml_str)
      Hash.from_xml(xml_str).to_json
    end

    def build_headers(call_name)
      {
          'X-EBAY-API-COMPATIBILITY-LEVEL' => schema_version.to_s,
          #'X-EBAY-API-SESSION-CERTIFICATE' => "#{dev_id};#{app_id};#{cert}",
          'X-EBAY-API-DEV-NAME' => @dev_id.to_s,
          'X-EBAY-API-APP-NAME' => @app_id.to_s,
          'X-EBAY-API-CERT-NAME' => @cert.to_s,
          'X-EBAY-API-SITEID' => @site_id.to_s,
          'X-EBAY-API-CALL-NAME' => call_name.to_s,
          #'Content-Type' => 'text/xml',
          'Accept-Encoding' => 'gzip'
      }
    end

    def build_get_headers
      {
          'X-EBAY-API-RESPONSE-ENCODING' => 'JSON'
      }
    end

    def build_body(request)
      result = REXML::Document.new
      result << REXML::XMLDecl.new('1.0', 'UTF-8')
      result << request.save_to_xml
      result.root.add_namespace XmlNs
      result.to_s
    end

    def connection(uri, refresh = false)
      @connection = Connection.new(uri) if refresh || @connection.nil?
      @connection
    end

    def decompress(response)
      content = case response['Content-Encoding']
                  when 'gzip'
                    gzr = Zlib::GzipReader.new(StringIO.new(response.body))
                    decoded = gzr.read
                    gzr.close
                    decoded
                  else
                    response.body
                end
    end

    def parse(content, format)
      case format
        when :object
          xml = REXML::Document.new(content)
          # Fixes the wrong case of API returned by eBay
          fix_root_element_name(xml)
          result = XML::Mapping.load_object_from_xml(xml.root)
          case result.ack
            when 'Failure', 'PartialFailure'
              raise RequestError.new(result.errors)
          end
        when :raw
          result = content
        else
          raise ArgumentError, "Unknown response format '#{format}' requested"
      end
      result
    end

    def fix_root_element_name(xml)
      # Fix upper cased API in response
      xml.root.name = xml.root.name.gsub(/API/, 'Api')

      # Fix lowercased Xsl in response document
      xml.root.name = xml.root.name.gsub(/XslResponse$/, 'XSLResponse')
    end
  end
end