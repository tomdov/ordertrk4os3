
module EbayAPI
  module Requests

    class BaseGetRequest
      def initialize (options = {})
        configFile = YAML.load_file('config/ebay.yml')
        @version = configFile[Rails.env]["schema_version"]
        @app_id = configFile[Rails.env]["app_id"]
        @call_name = call_name
      end

      def build_param_str
        "version=" + @version.to_s + "&appid=" + @app_id.to_s + "&callname=" + @call_name.to_s
      end


      def call_name
        self.class.to_s.split('::').last.gsub(/Request$/, '')
      end

    end
  end
end