require 'ebay/api/requests/base_get_request'
require 'ebay/api/config_api'

module EbayAPI
  module Requests

    class BaseShoppingGetRequest < BaseGetRequest
      def initialize (options = {})
        super
      end

      def build_param_str
        super
      end
    end
  end
end