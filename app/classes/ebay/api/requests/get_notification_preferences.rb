require 'ebay/api/classes/requester_credentials'
require 'ebay/api/requests/abstract'

module EbayAPI # :nodoc:
  module Requests # :nodoc:
                  # == Attributes
                  #  object_node :requester_credentials, 'RequesterCredentials', :class => RequserterCredentials, :optional => true
                  #  text_node :preference_level, 'PreferenceLevel', :optional => true
    class GetNotificationPreferences < Abstract
      include XML::Mapping
      include Initializer
      root_element_name 'GetNotificationPreferencesRequest'
      object_node :requester_credentials, 'RequesterCredentials', :class => RequesterCredentials, :optional => true
      text_node :preference_level, 'PreferenceLevel', :optional => true
    end
  end
end


