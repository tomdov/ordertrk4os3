require 'ebay/api/classes/requester_credentials'
require 'ebay/api/requests/abstract'
require 'ebay/api/initializer'

module EbayAPI # :nodoc:
  module Requests # :nodoc:
                  # == Attributes
                  #  object_node :requester_credentials, 'RequesterCredentials', :class => RequserterCredentials, :optional => true
                  #  text_node :preference_level, 'PreferenceLevel', :optional => true
    class GetClientAlertsAuthToken < Abstract
      include XML::Mapping
      include Initializer
      root_element_name 'GetClientAlertsAuthTokenRequest'
      object_node :requester_credentials, 'RequesterCredentials', :class => RequesterCredentials, :optional => true
    end
  end
end
