require 'ebay/api/classes/requester_credentials'
require 'ebay/api/classes/user_delivery_preference_array'
require 'ebay/api/classes/notification_event_property'
require 'ebay/api/requests/abstract'
require 'ebay/api/classes/application_delivery_preferences'

module EbayAPI # :nodoc:
  module Requests # :nodoc:
                  # == Attributes
                  #  object_node :application_delivery_preferences, 'ApplicationDeliveryPreferences', :class => ApplicationDeliveryPreferences, :optional => true
                  #  array_node :user_delivery_preferences, 'UserDeliveryPreferenceArray', 'NotificationEnable', :class => NotificationEnable, :default_value => []
                  #  object_node :user_data, 'UserData', :class => NotificationUserData, :optional => true
                  #  array_node :event_properties, 'EventProperty', :class => NotificationEventProperty, :default_value => []
                  #  text_node :delivery_url_name, 'DeliveryURLName', :optional => true
    class SetNotificationPreferences < Abstract
      include XML::Mapping
      include Initializer
      root_element_name 'SetNotificationPreferencesRequest'
      object_node :requester_credentials, 'RequesterCredentials', :class => RequesterCredentials
      object_node :application_delivery_preferences, 'ApplicationDeliveryPreferences', :class => ApplicationDeliveryPreferences, :optional => true
      object_node :user_delivery_preferences, 'UserDeliveryPreferenceArray', :class => UserDeliveryPreferenceArray, :default_value => []
      #object_node :user_data, 'UserData', :class => NotificationUserData, :optional => true
      #array_node :event_properties, 'EventProperty', :class => NotificationEventProperty, :default_value => []
      text_node :delivery_url_name, 'DeliveryURLName', :optional => true
    end
  end
end


