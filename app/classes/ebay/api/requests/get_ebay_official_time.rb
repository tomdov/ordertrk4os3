require 'ebay/api/responses/abstract'
require 'ebay/api/initializer'

module EbayAPI # :nodoc:
  module Requests # :nodoc:
    # == Attributes
    class GeteBayOfficialTime < Abstract
      include XML::Mapping
      include Initializer
      root_element_name 'GeteBayOfficialTimeRequest'
      object_node :requester_credentials, 'RequesterCredentials', :class => RequesterCredentials
    end
  end
end


