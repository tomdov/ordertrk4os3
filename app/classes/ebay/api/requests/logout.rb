require 'ebay/api/requests/base_client_alerts_get_request'

module EbayAPI
  module Requests

    class Logout < BaseClientAlertsGetRequest
      def initialize (options = {})
        super
        @session_id = options[:session_id]
        @session_data = options[:session_data]
      end

      def build_param_str
        super + "&SessionID=" + @session_id + "&SessionData=" + @session_data
      end

    end
  end
end
