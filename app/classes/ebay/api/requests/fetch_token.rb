require 'ebay/api/classes/requester_credentials'
require 'ebay/api/requests/abstract'
require 'ebay/api/initializer'

module EbayAPI # :nodoc:
  module Requests # :nodoc:
    # == Attributes
    #  text_node :secret_id, 'SecretID', :optional => true
    #  text_node :session_id, 'SessionID', :optional => true
    class FetchToken < Abstract
      include XML::Mapping
      include Initializer
      root_element_name 'FetchTokenRequest'
      text_node :secret_id, 'SecretID', :optional => true
      text_node :session_id, 'SessionID', :optional => true
      #object_node :requester_credentials, 'RequesterCredentials', :class => RequesterCredentials, :optional => true
    end
  end
end


