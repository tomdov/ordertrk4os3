require 'ebay/api/classes/requester_credentials'

module EbayAPI
  module Requests
    class Base
      include EbayAPI::Classes
      attr_accessor :auth_token, :username, :password

      def call_name
        self.class.to_s.split('::').last.gsub(/Request$/, '')
      end

      def requester_credentials
        RequesterCredentials.new(:ebay_auth_token => auth_token)
      end
    end
  end
end