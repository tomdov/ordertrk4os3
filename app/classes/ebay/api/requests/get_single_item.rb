require 'ebay/api/requests/base_shopping_get_request'
require 'ebay/api/initializer'
#require 'ebay/types/name_value_list'

module EbayAPI # :nodoc:
  module Requests # :nodoc:
    class GetSingleItem < BaseShoppingGetRequest
      def initialize (options = {})
        super
        @item_id = options[:item_id]
      end

      def build_param_str
        super + "&ItemID=" + @item_id
      end

    end
  end
end


