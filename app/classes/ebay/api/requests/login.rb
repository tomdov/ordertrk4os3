require 'ebay/api/requests/base_client_alerts_get_request'

module EbayAPI
  module Requests

    class Login < BaseClientAlertsGetRequest
      def initialize (options = {})
        super
        @client_alerts_auth_token = options[:client_alerts_auth_token]
      end

      def build_param_str
        super + "&clientAlertsAuthToken=" + @client_alerts_auth_token
      end

    end
  end
end
