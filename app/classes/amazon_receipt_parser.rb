require 'mail_parsing_common'

class AmazonReceiptParser

  def initialize(htmlMsg)
    @htmlMsg = htmlMsg
    @firstMerchantStarted = false
    @currentMerchantOrder = ""
    @requestNewLink = true
    @lastMarchantLink = ""
  end

  def parse_one_order
    if (nextOrder = get_next_order).nil?
      return nil
    end
    @firstMerchantStarted = true
    parse_one_order_ex(nextOrder)
  end

  def get_next_order
    begin
      if @firstMerchantStarted && get_next_order_in_same_merchant != nil
        @requestNewLink = false
        @currentMerchantOrder
      else
        @requestNewLink = true
        get_next_merchant_order
      end
    rescue Exception => e
      Rails.logger.error "Exception thrown: " + e.message
      nil
    end
  end

  def parse_one_order_ex(tempDetails)
    tempDetailsBackup = tempDetails
    @tempDetails = tempDetails
    retHash = {}

    begin
      if @requestNewLink
        tempLink        = @lastMarchantLink = getLinkFromOrder
      else
        tempLink        = @lastMarchantLink
      end

      tempQty         = getQtyFromOrder
      tempDesc        =	getDescFromOrder
      tempPayment     = getPriceFromOrder

    #    #TODO: get the mail date
    rescue
      Rails.logger.error "tempDetailsBackup = #{tempDetailsBackup.inspect}"
    end

    retHash[:description] = tempDesc
    retHash[:link]        = tempLink
    retHash[:qty]         = tempQty
    retHash[:amount]      = tempPayment

    if (tempDesc.blank? || tempLink.blank?)
      retHash[:error] = "Some details in the order were missing. Therefore the order hasn't created in your account. Please check that the receipt is a legal Amazon receipt in english. if you still think it is an issue in the site, please contact us"
    end


    @currentMerchantOrder = @tempDetails

    retHash

  end

  private

  def getLinkFromOrder
    begin
      @tempDetails      = MailParsingCommon.getStringAfterSubstr(@tempDetails, "a href=")
      @tempDetails      = MailParsingCommon.getStringAfterSubstr(@tempDetails, "\"")
      MailParsingCommon.getStringUntilSubstr(@tempDetails, "\">") #fix for target="_blank" issue - instead of "\">"
    rescue
      return nil
    end

  end

  def getQtyFromOrder
    if @requestNewLink
      @tempDetails    = MailParsingCommon.getStringAfterSubstr(@tempDetails, "Delivery estimate")
    end
    @tempDetails    = MailParsingCommon.getStringAfterSubstr(@tempDetails, "<b>")
    MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/b>")
  end

  def	getDescFromOrder

    @tempDetails = MailParsingCommon.getStringAfterSubstr(@tempDetails, "<b>")
    MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/b>")
  end

  def getPriceFromOrder
    @tempDetails    = MailParsingCommon.getStringAfterSubstr(@tempDetails, "\"price\">")
    MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/span>")
  end


  def get_next_order_in_same_merchant

    begin
      @currentMerchantOrder = MailParsingCommon.getStringAfterSubstr(@currentMerchantOrder, "<tr")
    rescue
      nil
    end
  end

  def get_next_merchant_order

    begin
      @htmlMsg = MailParsingCommon.getStringAfterSubstr(@htmlMsg, "Order #:")
    rescue
      return nil
    end

    begin
      tempOrder = MailParsingCommon.getStringUntilSubstr(@htmlMsg, "Order #:")
      @currentMerchantOrder = MailParsingCommon.getStringUntilSubstr(tempOrder, "<\\/table>")
    rescue
      @currentMerchantOrder = MailParsingCommon.getStringUntilSubstr(@htmlMsg, "<\\/table>")
    end
  end

end
