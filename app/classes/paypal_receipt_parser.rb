require 'mail_parsing_common'

class PaypalReceiptParser


  def initialize(htmlMsg)
    @htmlMsg = htmlMsg
    @firstMerchantStarted = false
    @currentMerchantOrder = ""
  end

  def parse_one_order
    if (nextOrder = get_next_order).nil?
      return nil
    end
    @firstMerchantStarted = true
    parse_one_order_ex(nextOrder)
  end

  def get_next_order
    begin
      if @firstMerchantStarted && get_next_order_in_same_merchant != nil
        @currentMerchantOrder
      else
        get_next_merchant_order
      end
    rescue
      nil
    end
  end

  def parse_one_order_ex(tempDetails)
    tempDetailsBackup = tempDetails
    @tempDetails = tempDetails
    retHash = {}

    begin
      tempLink        = getLinkFromOrder
      tempDesc        =	getDescFromOrder(tempLink)
      @tempDetails    =	MailParsingCommon.getStringAfterSubstr(@tempDetails, "<td") # getting to the price per unit td
      @tempDetails    =	MailParsingCommon.getStringAfterSubstr(@tempDetails, "<td") # getting to the Qty td
      tempQty         = MailParsingCommon.getStringAfterSubstr(@tempDetails, ">")
      tempQty         =	MailParsingCommon.getStringUntilSubstr(tempQty, "<\\/td>")

      @tempDetails    =	MailParsingCommon.getStringAfterSubstr(@tempDetails, "<td") # getting to the Payment td
      tempPayment     =	MailParsingCommon.getStringAfterSubstr(@tempDetails, ">")
      tempPayment     = MailParsingCommon.getStringUntilSubstr(tempPayment, "<\\/td>")
      if !tempLink.nil?
        tempLink      = CGI.unescapeHTML(tempLink).html_safe
      else
        tempLink      = "Unknown site"
      end

        #TODO: get the mail date
    rescue
      Rails.logger.error "tempDetailsBackup = #{tempDetailsBackup.inspect}"
    end

    retHash[:description] = tempDesc
    retHash[:link]        = tempLink
    retHash[:qty]         = tempQty
    retHash[:amount]      = tempPayment

    if (tempDesc.blank? || tempLink.blank?)
      retHash[:error] = "Some details in the order were missing. Therefore the order hasn't created in your account. Please check that the receipt is a legal Payapl receipt in english. if you still think it is an issue in the site, please contact us"
    end

    retHash

  end

private

  def getLinkFromOrder
    @tempDetails      = MailParsingCommon.getStringAfterSubstr(@tempDetails, "<td")
    tempDetails       = MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/td>")

    begin
      tempDetails     = MailParsingCommon.getStringAfterSubstr(tempDetails, "href=\"")
    rescue
      return nil
    end
    @tempDetails      = MailParsingCommon.getStringAfterSubstr(@tempDetails, "href=\"")
    MailParsingCommon.getStringUntilSubstr(tempDetails, "\"") #fix for target="_blank" issue - instead of "\">"

  end

  def	getDescFromOrder(link)

    @tempDetails = MailParsingCommon.getStringAfterSubstr(@tempDetails, ">")

    if link.nil?
      MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/td>")
    else
      MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/a>")
    end

  end


  def get_next_order_in_same_merchant

    begin
      @currentMerchantOrder = MailParsingCommon.getStringAfterSubstr(@currentMerchantOrder, "<tr>")
    rescue
      nil
    end
  end

  def get_next_merchant_order
    @htmlMsg    = MailParsingCommon.getStringAfterSubstrCaseSensitive(@htmlMsg, "Amount<")
    @htmlMsg    = MailParsingCommon.getStringAfterSubstr(@htmlMsg, "<tr>")
    @currentMerchantOrder = MailParsingCommon.getStringUntilSubstr(@htmlMsg, "<\\/table>")
  end

end