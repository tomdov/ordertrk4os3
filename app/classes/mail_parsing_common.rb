class MailParsingCommon


  def self.getStringAfterSubstr(str, substr)
    str.from((str =~ eval("/" + substr + "/i")) + substr.length)
  end

  def self.getStringFromSubstr(str, substr)
    str.from(str =~ eval("/" + substr + "/i"))
  end

  def self.getStringAfterSubstrCaseSensitive(str, substr)
    str.from((str =~ eval("/" + substr + "/")) + substr.length)
  end

  def self.getStringUntilSubstr(str, substr)
    pos = (str =~ eval("/" + substr + "/i")) - 1
    if pos == -1
      ""
    else
      str.to(pos)
    end
  end

end