require 'mail_parsing_common'

class AmazonReceiptParserMarch14

  def initialize(htmlMsg)
    @htmlMsg = htmlMsg
    @firstMerchantStarted = false
    @currentMerchantOrder = ""
  end

  def parse_one_order
    if (nextOrder = get_next_order).nil?
      return nil
    end
    @firstMerchantStarted = true
    parse_one_order_ex(nextOrder)
  end

  def get_next_order
    begin
      if @firstMerchantStarted && get_next_order_in_same_merchant != nil
        @currentMerchantOrder
      else
        get_next_merchant_order
      end
    rescue Exception => e
      Rails.logger.error "Exception thrown: " + e.message
      nil
    end
  end

  def parse_one_order_ex(tempDetails)
    tempDetailsBackup = tempDetails
    @tempDetails = tempDetails
    retHash = {}

    begin

      tempQty         = getQtyFromOrder
      tempImgUrl      = getImgUrlFromOrder
      tempLink        = getLinkFromOrder
      tempDesc        =	getDescFromOrder
      tempPayment     = getPriceFromOrder

        #    #TODO: get the mail date
    rescue
      Rails.logger.error "tempDetailsBackup = #{tempDetailsBackup.inspect}"
    end

    retHash[:description] = tempDesc
    retHash[:link]        = CGI.unescapeHTML(tempLink).html_safe
    retHash[:qty]         = tempQty
    retHash[:amount]      = tempPayment
    retHash[:image_url]   = CGI.unescapeHTML(tempImgUrl).html_safe

    if (tempDesc.blank? || tempLink.blank?)
      retHash[:error] = "Some details in the order were missing. Therefore the order hasn't created in your account. Please check that the receipt is a legal Amazon receipt in english. if you still think it is an issue in the site, please contact us"
    end


    @currentMerchantOrder = @tempDetails

    retHash

  end

  private

  def getQtyFromOrder
    ""
  end

  def getImgUrlFromOrder
    @tempDetails = MailParsingCommon.getStringAfterSubstr(@tempDetails, "img src=\"")
    MailParsingCommon.getStringUntilSubstr(@tempDetails, "\"")
  end


  def getLinkFromOrder
    @tempDetails      = MailParsingCommon.getStringAfterSubstr(@tempDetails, "a href=\"")
    MailParsingCommon.getStringUntilSubstr(@tempDetails, "\"") #fix for target="_blank" issue - instead of "\">"
  end

  def	getDescFromOrder

    @tempDetails = MailParsingCommon.getStringAfterSubstr(@tempDetails, ">")
    MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/a")
  end

  def getPriceFromOrder
    @tempDetails    = MailParsingCommon.getStringAfterSubstr(@tempDetails, "<strong>")
    MailParsingCommon.getStringUntilSubstr(@tempDetails, "<\\/strong")
  end


  def get_next_order_in_same_merchant

    begin
      @currentMerchantOrder = MailParsingCommon.getStringAfterSubstr(@currentMerchantOrder, "<table")
      @currentMerchantOrder = MailParsingCommon.getStringFromSubstr(@currentMerchantOrder, "<img src")
    rescue
      nil
    end
  end

  def get_next_merchant_order

    begin
      @htmlMsg = MailParsingCommon.getStringAfterSubstr(@htmlMsg, "Order Details")
      @htmlMsg = MailParsingCommon.getStringAfterSubstr(@htmlMsg, "Placed on")
      @htmlMsg = MailParsingCommon.getStringUntilSubstr(@htmlMsg, "Order Total")
    rescue
      return nil
    end
  end

end
