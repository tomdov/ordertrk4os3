class PaypalReceiptHandler

  def initialize(htmlMsg, user)
    @htmlMsg    = htmlMsg
    @user       = user
    @firstSeen  = false
  end

  def parse_html_save_and_send
    @parser = PaypalReceiptParser.new(@htmlMsg)
    while (resultHash = @parser.parse_one_order) != nil

      @firstSeen = true
      if !error_happened?(resultHash)
        add_order_to_db(resultHash)
      end
      send_mail(resultHash)
    end

    if @firstSeen == false
      send_no_orders_found_mail
    end
  end

private

  def send_no_orders_found_mail
    send_non_complient_mail({ :error => "No orders found in the mail you've sent. please check requirements for emailing orders" }).deliver
  end

  def send_non_complient_mail(resultHash)
    OrderMailer.new_order_failed_not_complient(@user, resultHash)
  end

  def add_order_to_db(resultHash)

    desc    = resultHash[:description]
    link    = resultHash[:link]
    qty     = resultHash[:qty]
    amount  = resultHash[:amount]

    if qty == "1"
      new_desc = desc
    else
      new_desc = qty + " " + desc
    end

    attr = {:description => new_desc,
            :site => link,
            :purchase_date => Date.current,
            :price => amount}
    order = @user.orders.build(attr.merge(:status => "Ordered"))
    #TODO: get the mail date
    order.status_date = Date.current
    order.save
  end

  def send_mail(orderHash)
    if error_happened?(orderHash)
      send_error_email(orderHash)
    else
      send_success_email(orderHash)
    end
  end

  def error_happened?(orderHash)
    !orderHash[:error].nil? && !orderHash[:error].blank?
  end

  def send_error_email(orderHash)
    OrderMailer.new_order_failed(@user, orderHash).deliver
  end

  def send_success_email(orderHash)
    OrderMailer.new_order_success(@user, orderHash).deliver
  end


end