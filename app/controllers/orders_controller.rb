include ApplicationHelper
require 'rubygems'
require 'aftership'

class OrdersController < ApplicationController

  layout 'lightbox', :only => [:save_order, :order_saved_msg, :order_saved_msg_chrome_ext]
  before_filter :authenticate, :except => [:save_order]
  before_filter :authenticate_lightbox, :only => [:save_order]

  def new
    @title = "New order"
    @order = Order.new
    @order.purchase_date = Date.current.strftime('%F')
    @order.package_tracking = PackageTracking.new
    @user = current_user

    set_default_notif_days_for_order
  end

  def create

    if signed_in?
      @user = current_user

      user_orders = current_user.orders
      #checking if we come from lightbox or regular
      source = params[:order][:source]
      save_order_params = params[:order][:save_order_params]
      params[:order].delete(:lightbox)
      params[:order].delete(:save_order_params)
      params[:order].delete(:source)

      hash = {}

      #check the tracking number
      if params[:order][:package_tracking_attributes] != nil &&
         check_and_create_tracking_number(params[:order][:package_tracking_attributes][:tracking_number], hash) == false
        flash[:error] = "Non valid tracking number"
        render 'new'
        return
      end

      if params[:order][:package_tracking_attributes] != nil &&
        params[:order][:package_tracking_attributes].merge!(:id_aftership => hash['id_aftership'])
      end
      @order = user_orders.build(params[:order].merge(:status => "Ordered"))
      @order.status_date = @order.purchase_date


      if @order.save
        flash[:success] = "Order saved!"
        if source == "bookmarklet"
          redirect_to orders_order_saved_path
        elsif source == "chrome_ext"
          redirect_to orders_order_saved_chrome_ext_path
        else
          redirect_to user_path(current_user)
        end

      else
        @feed_items = []
        @title = "New order"
        @source = source

        if source == "bookmarklet"
          render 'new', :layout => 'lightbox'
        elsif source == "chrome_ext"
          render 'new', :layout => 'lightbox'
        else
          render 'new'
        end
      end
    else
      redirect_to root_path
    end
  end

  def complete

    if signed_in?
      @order = Order.find(params[:id])
      if (@order.user != current_user)
        flash[:error] = "Access denied"
        redirect_to current_user
        return
      end

      @order.status = "Picked"
      @order.status_date = Date.current
      @user = current_user

      if @order.save
        flash[:success] = "Order marked as picked. You can see it in the Archive"
        redirect_to user_path(current_user)
      else
        flash[:error] = "Error in order saving"
      end

    end
  end

  def destroy
    if signed_in?
      @order = Order.find(params[:id])
      @order.destroy
      redirect_to user_path(current_user)
    else
      redirect_to root_path
    end
  end

  def edit
    if signed_in?
      @user = current_user
      @title = "Edit order"
      @order = Order.find(params[:id])
      if (@order.user != current_user)
        flash[:error] = "Access denied"
        redirect_to current_user
      end
      if @order.package_tracking.nil?    
        @order.package_tracking = PackageTracking.new
      end

    end
  end

  def update
    if signed_in? == false
      redirect_to root_path
      return
    end

    # user is signed in
    @order = Order.find(params[:id])
    logger.debug "@order.package_tracking = " + @order.package_tracking.inspect
    hash = {}
    #check the tracking number
    if is_tracking_number_changed(params, @order)

      if params[:order][:package_tracking_attributes] != nil &&
         check_and_create_tracking_number(params[:order][:package_tracking_attributes][:tracking_number], hash) == false
        flash[:error] = "Non valid tracking number"
        @user = current_user
        render 'edit'
        return
      else
        # tracking number was changed and the new one created successfully
        remove_tracking_number_from_aftership(@order.package_tracking)
      end

    end

    # adding Aftership id to the order
    if (hash['id_aftership']) != nil
      params[:order][:package_tracking_attributes].merge!(:id_aftership => hash['id_aftership'])
    end

    user_orders = current_user.orders
    params2 = renew_notification_if_days_changed(params, user_orders)
    params = params2

    if @order.update_attributes(params[:order])
      flash[:success] = "Order saved successfuly"
      redirect_to user_path(current_user)
    else
      flash[:error] = "Error in order saving"
      @title = "Edit order"
      @user = current_user
      render 'edit'
    end

  end

  def save_order
    save_order_common(params)
    render 'new', :layout => 'lightbox'
  end

  def order_saved_msg_chrome_ext
    @title = "Order saved"
    @user = current_user
    @source = "chrome_ext"
    render 'order_saved_msg'
  end

  def remove_non_DB_attr_from_params(params)
    params.delete(:days_notification_enable)
  end


  def order_saved_msg
    @title = "Order saved"
    @user = current_user
  end

private

  def save_order_common(params)
    get_params_hash = params

    @title = "New order"
    @order = create_order_with_params(get_params_hash)
    @source = get_params_hash[:source]
    @user = current_user
    @save_order_params = get_params_hash
    set_default_notif_days_for_order
  end

  def set_default_notif_days_for_order
    user_default_days_notification = @user.default_days_notification
    if ( user_default_days_notification != nil && user_default_days_notification != 0 )
      @order.days_notification_enable = true
      @order.days_notification        = user_default_days_notification
    end
  end

  def renew_notification_if_days_changed(params, user_orders)
    order = user_orders.find_by_id(params[:id])
    if ((params[:order][:days_notification_enabled] && params[:order][:days_notification].to_i != order.days_notification) ||
        !params[:order][:days_notification_enabled])
      params[:order].merge!(:days_notification_sent => false)
    end
    return params
  end

  def create_order_with_params(req_params)
    order = Order.new
    order.description     = req_params[:description]
    order.site             = req_params[:url]
    order.price           = req_params[:price]
    order.purchase_date   = Date.today
    order
  end

  def check_and_create_tracking_number(tracking_number, hash_params)
    if !(tracking_number.blank?)
      AfterShip.api_key = YAML.load_file('config/aftership.yml')[Rails.env]["api_key"]
      res = AfterShip::V4::Tracking.create(tracking_number)
      
      if ((res['meta']['code'].to_i / 100) != 2) #succcess codes
        #only if tracking number isn't recognized
        puts 'got error when trying to create new tracking: ' + res['data'].inspect
        return false
      end

      hash_params['id_aftership'] = res['data']['tracking']['id']

    end
    true
  end

  def is_tracking_number_changed(params, orig_order)
    params[:order][:package_tracking_attributes] != nil && 
    params[:order][:package_tracking_attributes][:tracking_number] != orig_order.package_tracking.tracking_number
  end

  def remove_tracking_number_from_aftership(package_tracking)
    id_aftership = package_tracking.id_aftership
    if !(id_aftership.blank?)
      AfterShip.api_key = YAML.load_file('config/aftership.yml')[Rails.env]["api_key"]
      res = AfterShip::V4::Tracking.delete_by_id(id_aftership)
      logger.debug "delete RES = " + res.inspect
    end

  end

end
