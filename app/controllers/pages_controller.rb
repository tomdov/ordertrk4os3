class PagesController < ApplicationController

  include ApplicationHelper
  before_filter :collect_referer

  def home
    @title = "Home"
    if signed_in?
      redirect_to user_path(current_user)
    end
  end

  def contact
	@title = "Contact"
  @user = current_user
  end

  def about
	@title = "About"
  @user = current_user
  end

  def help
  @title = "Help"
  @user = current_user
  end

  def forgot
  @title = "Forgot password"
  @user = current_user
  end

  def terms
  @title = "Terms of use"
  @user = current_user
  end

  def news
  @title = "News"
  @user = current_user
  end

  def bookmarklet_explain
  @title = "Browser Plugin"
  @user = current_user
  end

  def android_app
  redirect_to android_app_short
  end

  def ios_app
  redirect_to ios_app_short
  end

  def chrome_ext
  redirect_to chrome_ext_short
  end

  def letsencrypt_check
  render :layout => false, :formats => [:text]
  end 
end
