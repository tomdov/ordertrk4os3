class ForgotController < ApplicationController
  def show
    #@user = User.new
    @key   = params[:id]
    @title = "Forgot password"
    @user  = current_user
  end

  def new
    @title = "Forgot password"
    @user  = current_user
  end


  def create
    email = params[:user][:email]
    if email == "" || !(email =~ User.email_regexp)
      flash.now[:error] = "Invalid E-mail address"
      @title = "Forgot password"
      render 'new'
      return
    end

    #check that user exists
    @user = User.find_by_email(email)
    flash[:success] = "mail with instructions will be sent to #{email} in case the E-mail address found in the system"
    if (!@user.nil?)
      string_len = 64
      #create string
      random_key = SecureRandom.hex(string_len).insert(string_len/4, "#{@user.id}")
      #remove previous key (if any)
      @user.forgot_key.destroy unless @user.forgot_key.nil?
      #save it in the user DB
      ForgotKey.create(:key => random_key, :user_id => @user.id)
      @user.reload
      #send email
      UserMailer.forgot_password_mail(@user).deliver
    end
    redirect_to root_path
  end


  def update

    @user = User.find_by_email(params[:user][:email])

    if (@user.nil? || @user.forgot_key.nil? || (@user.forgot_key.key != params[:id]))
      flash[:error] = "Mismatch between link and email. \nthe link is limited to one use. please click the \"Forgot password\" button"
      redirect_to signin_path
      @user.forgot_key.destroy if !@user.nil? && !@user.forgot_key.nil?
      return
    end


    if @user.update_attributes(params[:user])
        flash[:success] = "Password changed successfully"
    else
        flash[:error] = "Errors occurred and the link is limited to one use. \nPlease click the \"Forgot password\" button"
    end

    @title = "Sign in"
    @user.forgot_key.destroy
    redirect_to signin_path
  end
end
