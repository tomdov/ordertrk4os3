class TrackUpdatesController < ApplicationController

  def create
    event_payload = params['track_update']
    if check_security_token(extract_security_token_from_params(params)) && event_payload != nil
      handle_event(get_hook_event_from_params(event_payload), get_hook_msg_from_params(event_payload))
    end

    render :nothing => true
  end

  def show
     render :nothing => true
  end

  private

  def handle_event(event, msg)
    if event == 'tracking_update'
      handle_tracking_update_event(msg)
    end
  end

  def handle_tracking_update_event(msg)
  #  get tracking number
    tracking_number = get_tracking_number_from_msg(msg)

  #  find the tracking number in DB
    package_tracking_from_DB = PackageTracking.find_by_tracking_number(tracking_number)

  #  check checkpoints: if we don't have all the info, add all the info to the order
  #  if we just need the last update, take just the last one
    update_checkpoints(package_tracking_from_DB , get_checkpoints_arr_from_msg(msg))

  end

  def update_checkpoints(package_tracking_from_DB, checkpoints_arr)
    # checkpoints can be null or empty
    if checkpoints_arr.nil? || checkpoints_arr.count == 0
      return
    end
    
    checkpoints_num = checkpoints_arr.count

  #  optimization - if we have the same number of checkpoints, do nothing
    if package_tracking_from_DB.aftership_checkpoints == checkpoints_num
      return
    end

  #  check how many new checkpoints we have in DB
    update_order_with_checkpoints(package_tracking_from_DB, checkpoints_arr)

  #save the new number of checkpoints
    package_tracking_from_DB.aftership_checkpoints = checkpoints_num
    package_tracking_from_DB.save
  end

  def update_order_with_checkpoints(package_tracking_from_DB, checkpoints_arr)
    order = package_tracking_from_DB.order
    old_order = Order.new(:status => order.status)

    if !order.nil?
      checkpoints_diff = checkpoints_arr.count - package_tracking_from_DB.aftership_checkpoints
      relevant_checkpoints_arr = get_relevant_checkpoint_to_update_order(checkpoints_arr, checkpoints_diff)
      if relevant_checkpoints_arr.nil?
        return
      end

      updated_order = update_order_with_last_checkpoints(order, relevant_checkpoints_arr, checkpoints_diff)
      updated_order = update_order_status_with_last_checkpoint(updated_order, relevant_checkpoints_arr)
    
      if updated_order.save
        send_status_change_mail(old_order, updated_order)
      else
        Rollbar.report_message("Failed to save updated order!!!")  
      end

    else
      Rollbar.report_message("order of package tracking was nil!!!")
    end

  end

  def send_status_change_mail(order, updated_order)
    order_status = order.status
    updated_order_status = updated_order.status

    if order_status != updated_order_status
      orderHash = {}
      orderHash[:previous_state]  = order_status
      orderHash[:new_state]       = updated_order_status
      send_status_change_mail_do(updated_order, orderHash)
    end
  end

  def update_order_status_with_last_checkpoint(order, relevant_checkpoints_arr)
    status = get_aftership_status_from_checkpoints(relevant_checkpoints_arr)

    if status != nil
      if order.status != status
        order.status                = status
        order.status_date           = Date.today        
      end
    end
    order
  end

  def get_aftership_status_from_checkpoints(relevant_checkpoints_arr)
    relevant_checkpoints_arr.reverse.each do |checkpoint|
      package_tracking_status = get_tag_from_checkpoint(checkpoint)
      converted_status_from_tag = convert_aftership_tag_to_status(package_tracking_status)
      if converted_status_from_tag != nil
        return converted_status_from_tag
      end
    end
    return nil
  end

  def send_status_change_mail_do(order, orderHash)
    orderHash[:description] = order.description
    orderHash[:link]        = order.site
    orderHash[:amount]      = order.price
    OrderMailer.order_status_changed(order.user, orderHash).deliver
  end

  def convert_aftership_tag_to_status(package_tracking_status)
    case package_tracking_status
      when 'InTransit'
        'Shipped'
      when 'Delivered'
        'Delivered'
      when 'OutForDelivery'
        'Delivered'
      else
        nil
      end
  end

  def get_relevant_checkpoint_to_update_order(checkpoints_arr, checkpoints_diff)
    checkpoints_arr_count = checkpoints_arr.count
    relevant_checkpoints_begin_index = checkpoints_arr_count - checkpoints_diff
    relevant_checkpoints_end_index = checkpoints_arr_count - 1
    checkpoints_arr[relevant_checkpoints_begin_index..relevant_checkpoints_end_index]
  end

  def update_order_with_last_checkpoints(order, relevant_checkpoints_arr, checkpoints_diff)
    updated_order = order
    relevant_checkpoints_arr.each do |checkpoint|
      updated_order = update_order_with_checkpoint(updated_order, checkpoint)
    end

    updated_order
  end

  def update_order_with_checkpoint(order, checkpoint)
    add_automated_checkpoint_text_to_order(order, checkpoint)
  end

  def add_automated_checkpoint_text_to_order(order, checkpoint)
    order.notes += "\n"
    order.notes += "** #{Date.parse(get_checkpoint_time_from_checkpoint(checkpoint))} - Automatic tracking update: #{get_tag_from_checkpoint(checkpoint)} #{get_msg_from_checkpoint(checkpoint)} Country: #{get_country_name_from_checkpoint(checkpoint)}"
    if get_state_from_checkpoint(checkpoint) != nil
      order.notes += " State: #{get_state_from_checkpoint(checkpoint)}"
    end
    if get_city_name_from_checkpoint(checkpoint) != nil
      order.notes += " City: #{get_city_name_from_checkpoint(checkpoint)}"
    end
    order.notes += " **"
    order
  end


  def get_hook_event_from_params(params)
    params['event']
  end

  def get_hook_time_from_params(params)
    params['ts']
  end

  def get_hook_msg_from_params(params)
    params['msg']
  end

  def get_tracking_number_from_msg(msg)
    msg['tracking_number']
  end

  def get_checkpoints_arr_from_msg(msg)
    msg['checkpoints']
  end

  def get_msg_from_checkpoint(checkpoint)
    checkpoint['message']
  end

  def get_tag_from_checkpoint(checkpoint)
    checkpoint['tag']
  end

  def get_checkpoint_time_from_checkpoint(checkpoint)
    checkpoint['checkpoint_time']
  end

  def get_country_name_from_checkpoint(checkpoint)
    checkpoint['country_name']
  end

  def get_city_name_from_checkpoint(checkpoint)
    checkpoint['city']
  end

  def get_state_from_checkpoint(checkpoint)
    checkpoint['state']
  end

  def extract_security_token_from_params(params)
    params['sec_tok']
  end

  def check_security_token(security_token)
    security_token.to_s == YAML.load_file('config/aftership.yml')[Rails.env]["security_token"].to_s
  end

end
