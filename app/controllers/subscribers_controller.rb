class SubscribersController < ApplicationController
  def create
    @subscriber = Subscriber.new(params[:subscriber])
    @subscriber.add_unsubscribe_key

    if @subscriber.save
      UserMailer.beta_subscriber_mail(@subscriber.email).deliver
      redirect_to beta_thank_you_path
    else
      flash[:error] = "The E-Mail you've enter is illegal, please enter valid mail address"
      redirect_to root_path
    end

  end

  def unsubscribe
    subscriber = Subscriber.find_by_id(params[:id])
    if subscriber != nil && subscriber.key == params[:un]
      flash[:success] = 'You were successfully removed from subscribers mailing list'
      subscriber.subscribed = false
      subscriber.save
    end
    redirect_to root_path
  end

end
