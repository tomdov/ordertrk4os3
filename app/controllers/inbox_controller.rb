class InboxController < ApplicationController
  # include Mandrill::Rails::WebHookProcessor
  # authenticate_with_mandrill_keys! 'KSo6TIdXZGC6gzQSqVTwAg', '2w9-KB__pGkZtrtAEyLiyg'
  # require 'mandrill'

  def user_verified?(payload)
    @user = User.find_by_email(payload.sender_email)
    !@user.nil?
  end

  def handle_inbound(event_payload)

    logger.info "New incoming order mail. info: #{event_payload.inspect}"

    if event_payload.recipient_emails.include?("info@#{UserMailer.site_url}") ||
        event_payload.recipient_emails.include?("webmaster@#{UserMailer.site_url}")
      forward_info_mail(event_payload)
      return
    end

    if !user_verified?(event_payload)
      #error
      return
    end

    if event_payload.recipient_emails.any?{ |s| s.casecmp("neworder@#{OrderMailer.site_url}") == 0 }

      if ((event_payload.subject =~ /paypal/i) != nil) && !event_payload.message_body(:html).nil?
        paypalHandler = MailReceiptHandler.new(event_payload.message_body(:html), @user, PaypalReceiptParser)
        paypalHandler.parse_html_save_and_send

      elsif ((event_payload.subject =~ /amazon/i) != nil) && !event_payload.message_body(:html).nil?
        amazonHandler = MailReceiptHandler.new(event_payload.message_body(:html), @user, AmazonReceiptParserMarch14)
        amazonHandler.parse_html_save_and_send

      else
        orderHash = { :error => "The subject of your mail you've sent isn't recognized by the site. Please forward Paypal or Amazon receipt (which contains 'Paypal' or 'Amazon' in the subject)." }
        OrderMailer.new_order_failed_not_complient(@user, orderHash).deliver
      end

    end


  end

  def forward_info_mail(event_payload)
    begin
      forward_mail = "ordertrk@gmail.com"
      event_payload['msg']['to'] = [ { :email => forward_mail } ]
      event_payload['msg']['headers']['To'] = forward_mail
      mandrill = Mandrill::API.new(ActionMailer::Base.smtp_settings[:password])
      result = mandrill.messages.send(event_payload['msg'])
      logger.info "mandrill send result = #{result.inspect}"
    rescue Mandrill::Error => e
      logger.error "Mandrill send error #{e.class} - #{e.message}"
    end

  end

end
