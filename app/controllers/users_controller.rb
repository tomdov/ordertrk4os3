require 'ebay/api/ebay_api_main'

class UsersController < ApplicationController
  include ApplicationHelper

  before_filter :authenticate, :except => [:new, :create]
  before_filter :correct_user, :only => [:edit, :update, :general_settings, :ebay_settings, :subscribe_settings]
  before_filter :admin_only,   :only => :destroy

  def index
    if !current_user.admin?
      @user  = current_user
      render 'public/404'
    else
      @users = User.paginate(:page => params[:page])
      @user  = current_user
      @title = "All users"
    end

  end
  def show

    #if signed_in?
      @user = current_user
      @title = @user.name
      @feed_items = @user.feed.paginate(:page => params[:page])
      if current_user != User.find(params[:id])
          flash.now[:error] = "Access denied"
          render 'show'
      end
    #else
    #  redirect_to root_path
    #end

  end


  def new
    if signed_in?
      @user = current_user
      @title = @user.name
      flash[:error] = "You are already signed up...."
      redirect_to @user

    else
      collect_referer
      @user = User.new
      @title = "Sign Up"
    end
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      UserMailer.welcome_email(@user).deliver
      flash[:success] = "Welcome to the Order Tracking app"
      sign_in_first_time(@user)
      redirect_to @user # possible user_path(@user) also
    else
      @title = "Sign Up"
      render 'new'
    end
  end

  def edit
      @title = "Edit user"
      @user  = current_user
  end

  def update

    #check old password and remove old_password param
    if !@user.has_password?(params[:user][:old_password])
      flash.now[:error] = "Wrong old password"
      @title = "Edit user"
      if subscribe_settings_context(params)
        render 'subscribe_settings'
      else
      #  general settings context
        render 'general_settings'
      end
      return
    end

    params[:user].delete(:update_user_context)

    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user][:password]              = params[:user][:old_password]
      params[:user][:password_confirmation] = params[:user][:old_password]
    end

    params[:user].delete(:old_password)

    if @user.update_attributes(params[:user])
      flash[:success] = "User saved successfuly"
      redirect_to @user # possible user_path(@user) also
    else
      flash[:error] = "Error in user saving"
      @title = "Edit user"
      render 'edit'
    end

  end

  def destroy
      user = User.find(params[:id])
      if !currect_user?(user)
        user.destroy
        redirect_to users_path, :flash => { :success => "User successfully deleted" }
      else
        redirect_to users_path, :flash => { :error => "An admin can't delete itself" }
      end
  end

  def archive
    @archive_mode = true
    @user = current_user
    @title = @user.name
    @feed_items = @user.archive_feed.paginate(:page => params[:page])
    if current_user != User.find(params[:id])
      flash.now[:error] = "Access denied"
    end

    render 'show'
  end

  def ebay_settings
    if signed_in?
      @user = current_user
      @title = "eBay settings"
      @ebay_accounts = @user.ebay_users
    end
  end

  def general_settings
    @user = current_user
    @title = "General settings"
  end

  def subscribe_settings
    @unsubscribe_mail = params[:mail]
    @user = current_user
    @title = "Subscribe settings"
  end

  private

    def correct_user
      @user = User.find(params[:id])

      if current_user != nil && !currect_user?(@user)
        flash[:error] = "Access denied"
        redirect_to(root_path)
      end
    end

    def admin_only
      redirect_to(root_path) unless current_user.admin?
    end

    def subscribe_settings_context(params)
      context = params[:user][:update_user_context]
      context != nil && context == 'subscribe'
    end
end
