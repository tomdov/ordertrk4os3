require 'ebay/api/ebay_api_main'
require 'ebay/api/config_api'

class EbayUsersController < ApplicationController

  def new
    @title = "Set new eBay account"
    @ebay_user = EbayUser.new
    @user = current_user
    ru_name = CGI.escape(EbayAPI::ConfigAPI.instance.ru_name)

    @api = EbayAPI::Api.new

    response = @api.get_session_id( :ru_name => ru_name )
    #TODO: handle failure

    @session_id = response.session_id
  end

  def create

    if signed_in? == false
      redirect_to root_path
      return
    end

    sid   = params[:ebay_user][:session_id]
    name  = params[:ebay_user][:name]
    @user = current_user

    # shouldn't happen - we're validating the name on client side
    if params[:name] == ""
      flash[:error] = "Cannot save without nickname"
      redirect_to new_ebay_user_path(@user)
      return
    end

    if duplicate_nick_exist?(@user, name)
      flash[:error] = "Nickname already exist"
      redirect_to new_ebay_user_path(@user)
      return
    end

    # validations finished
    ru_name = CGI.escape(EbayAPI::ConfigAPI.instance.ru_name)

    encoded_session_id = CGI.escape(sid)

    #params to be returned by eBay
    encoded_return_params = CGI.escape("session_id=#{sid}&name=#{name}")

    redirect_to "#{EbayAPI::ConfigAPI.instance.signin_address}SignIn&RuName=#{ru_name}&SessID=#{encoded_session_id}&ruparams=#{encoded_return_params}"
  end

# called after successful auth redirect from eBay
  def complete

    if signed_in? == false
      redirect_to root_path
      return
    end

    @user = current_user

    @api = EbayAPI::Api.new

    begin
      @ebay_user = fetch_token(params)
    rescue
      flash[:error] = "Error in creating eBay account, please try again"
      redirect_to new_ebay_user_path(@user)
      return
    end

    if check_auth_token?(@ebay_user.token) && @ebay_user.save
      api = EbayAPI::Api.new
      @ebay_user.set_notifications(api)
      flash[:success] = "eBay account saved! We will start to get notifications from eBay in the next hour"
      redirect_to ebay_settings_user_path(@user)
    else
      @title = "New eBay account"
      redirect_to new_ebay_user_path(@user)
    end

  end

  def destroy
    if signed_in?
      @ebay_user = EbayUser.find_by_id(params[:id])
      api = EbayAPI::Api.new
      @ebay_user.unset_notifications(api)
      @ebay_user.destroy
      flash[:success] = "eBay account successfully removed"
      redirect_to ebay_settings_user_path(current_user)
    end
  end

  private

  def fetch_token(params)
    response = @api.fetch_token( :session_id => params[:session_id] )
    params.delete(:session_id)
    ebay_users = current_user.ebay_users
    ebay_user = ebay_users.build(params)
    ebay_user.token = response.ebay_auth_token
    ebay_user.expiration = response.hard_expiration_time
    ebay_user
  end

  def check_auth_token?(auth_token)

    req_cred = EbayAPI::Classes::RequesterCredentials.new(
        :ebay_auth_token => auth_token
    )

    response = @api.get_ebay_official_time( :requester_credentials => req_cred )
    response.success?
  end

  def duplicate_nick_exist?(user, nick)
    user.ebay_users.each do | ebay_user |
      if ebay_user.name == nick
        return true
      end
    end

    false
  end

end


