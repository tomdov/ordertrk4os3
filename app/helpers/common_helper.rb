module CommonHelper

  def site_name
    "Order Tracking"
  end

  def site_url
    "ordertrk.com"
  end

  def full_site_url
    "http://www.#{site_url}"
  end

  def signin_url
    "#{full_site_url}/signin"
  end

end