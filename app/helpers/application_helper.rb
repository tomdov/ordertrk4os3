module ApplicationHelper

  extend CommonHelper

  # Return a title on a per-page basis.
  def title
    base_title = site_name
    if @title.nil?
      base_title
    else
      "#{base_title} | #{@title}"
    end
  end

  def logo
    image_tag("OrderTrackingLogo.png", :alt => "Order Tracking", :class => "round")
  end

  # mobile logos
  # Android
  def android_app_logo
    image_tag("AndroidAppLogo.png", :alt => "Order Tracking Android App", :class => "round")
  end

  def android_app_logo_small
    image_tag("AndroidAppLogo_small.png", :alt => "Order Tracking Android App", :class => "round")
  end

  def android_app_short
    "https://play.google.com/store/apps/details?id=com.ionicframework.ordertrackingtestapp817585"
  end

  # IOS
  def ios_app_logo
    image_tag("appStoreLogo.png", :alt => "Order Tracking IOS App", :class => "round")
  end

  def ios_app_logo_small
    image_tag("appStoreLogo_small.png", :alt => "Order Tracking IOS App", :class => "round")
  end

  def ios_app_short
    "https://itunes.apple.com/us/app/order-trk/id969489409?mt=8"
  end

  #Chrome extension
  def chrome_ext_short
    "https://chrome.google.com/webstore/detail/order-tracking-beta-order/jjdpihikjegjdnmcfageameidobeanmk"
  end


  def logo_small
    image_tag("orderTrackingLogoOnlySmallBlack.jpg", :alt => "Order Tracking", :class => "round img-top")
  end

  def logo_and_title_small
    image_tag("orderTrackingTitleLogo.jpg", :alt => "Order Tracking", :class => "round auto-height", :height => '45px')
  end

  #return the amount of days from the first date to the second date
  def ApplicationHelper.days_diff(first_date, second_date)
    (second_date - first_date).to_i
  end

  def days_diff(first_date, second_date)
    ApplicationHelper.days_diff(first_date, second_date)
  end

  def ApplicationHelper.is_link?(link)
    #http:// or https://
    (link.at(0..6) == "http://") or (link.at(0..7) == "https://")
  end

  def is_link?(link)
    ApplicationHelper.is_link?(link)
  end

  def ApplicationHelper.seconds_between_timestamps(first, second)
    (Time.parse(second.to_s) - Time.parse(first.to_s))
  end

  def hours_between_timestamps(first, second)
    ApplicationHelper.seconds_between_timestamps(first, second) / 3600
  end

  def collect_referer
    if (cookies.signed[:referer].nil?)
      cookies.permanent.signed[:referer] = request.env["HTTP_REFERER"] || 'direct'
    end
  end

  def cookie_fetch_referer
    cookies.signed[:referer]
  end
end
