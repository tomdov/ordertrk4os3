module OrdersHelper

  #return the absolute amount of days between 2 dates
  def days_diff_abs(first_date, second_date)
    abs_days = days_diff(first_date, second_date)
    abs_days = abs_days * (-1) if abs_days < 0
    return abs_days
  end

  #return "ago" if the first date is before the second date or the same date, and "ahead" if the first date is "bigger" the second
  def days_diff_ago_or_ahead(first_date, second_date)
    if days_diff(first_date, second_date) < 0
      "ahead"
    else
      "ago"
    end
  end

  def manipulate_site_if_neccesery(site)
    max_site_length_without_truncating = 30
    str = site
    str = str.truncate(max_site_length_without_truncating) if is_link?(site)
    return str
  end

  def manipulate_notes_if_neccesery(notes)
    max_notes_length_without_truncating = 100
    str = notes
    str = str.truncate(max_notes_length_without_truncating)
    return str
  end

  def create_site_for_view(site, bmanipulate_site = true)
    str = ""
    if site.nil?
       return ""
    end

    str += "\<a href=#{site} target=\"_blank\"\>" if is_link?(site)
    if bmanipulate_site
      str += manipulate_site_if_neccesery(site)
    else
      str += site
    end
    str += "</a>" if is_link?(site)
    str.html_safe()
  end

  def create_notes_for_view(notes, bmanipulate_str = true)
    str = ""

    if notes.nil?
      return ""
    end

    if bmanipulate_str
      str += manipulate_notes_if_neccesery(notes)
    else
      str += notes
    end
    str.html_safe()
  end
end
