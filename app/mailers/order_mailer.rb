class OrderMailer < ActionMailer::Base

  include ApplicationHelper
  extend CommonHelper

  default from: "#{site_name} <do-not-replay@#{site_url}>"
  layout 'mailer_layout/order_mailer'

  def days_notification_mail(order)
    @user               =   order.user
    if mail_pre_checks?(prepare_pre_checks_hash_params(@user)) == false
      return
    end

    @url                =   OrderMailer.signin_url
    @site_url           =   OrderMailer.full_site_url
    @order_desc         =   order.description
    @link               =   order.site
    @days_since_ordered =   ApplicationHelper.days_diff(order.purchase_date, Date.current)
    @addr_is_link       =   ApplicationHelper.is_link?(@link)
    @order_link         =   OrderMailer.full_site_url + edit_order_path(order)
    email_with_name = "#{@user.name} <#{@user.email}>"
    mail(to: email_with_name, subject: 'Order Tracking - Notification')
    order.days_notification_sent = true
    order.save
  end

  def new_order_failed_not_complient(user, orderHash)
    if mail_pre_checks?(prepare_pre_checks_hash_params(user)) == false
      return
    end
    assign_order_attr_from_hash(orderHash)
    @full_details_failure_mail = false
    create_new_order_mail(user, 'Order Tracking - New order receive failed', 'order_mailer', 'new_order_failure')
  end

  def new_order_success(user, orderHash)
    if mail_pre_checks?(prepare_pre_checks_hash_params(user)) == false
      return
    end
    assign_order_attr_from_hash(orderHash)
    @full_details_failure_mail = true
    create_new_order_mail(user, 'Order Tracking - New order received', 'order_mailer', 'new_order_success')
  end

  def new_order_failed(user, orderHash)
    if mail_pre_checks?(prepare_pre_checks_hash_params(user)) == false
      return
    end
    assign_order_attr_from_hash(orderHash)
    @full_details_failure_mail = true
    create_new_order_mail(user, 'Order Tracking - New order receive failed', 'order_mailer', 'new_order_failure')
  end

  def order_status_changed(user, orderHash)
    if mail_pre_checks?(prepare_pre_checks_hash_params(user)) == false
      return
    end
    assign_status_changed_attr_from_hash(orderHash)
    status_changed_mail(user, 'Order Tracking - Order status changed notification')
  end

  private

  def assign_common_instance_attr(user)
    @user                       = user
    @url                        = OrderMailer.signin_url
    @site_url                   = OrderMailer.full_site_url
    if !@tempLink.nil?
      @addr_is_link             = ApplicationHelper.is_link?(@tempLink)
    end
    @email_with_name = "#{@user.name} <#{@user.email}>"
  end

  def create_new_order_mail(user, subject, template_path, template_name)
    assign_common_instance_attr(user)
    mail(to: @email_with_name, subject: subject,
         template_path: template_path, template_name: template_name)
  end

  def status_changed_mail(user, subject)
    assign_common_instance_attr(user)
    mail(to: @email_with_name, subject: subject,
         template_path: 'order_mailer', template_name: 'status_changed_mail')
  end

  def assign_status_changed_attr_from_hash(orderHash)
    @tempDesc           = orderHash[:description]
    @tempLink           = orderHash[:link]
    @tempPreviousState  = orderHash[:previous_state]
    @tempNewState       = orderHash[:new_state]
    @tempPayment        = orderHash[:amount]
  end

  def assign_order_attr_from_hash(orderHash)
    @tempDesc     = orderHash[:description]
    @tempLink     = orderHash[:link]
    @tempQty      = orderHash[:qty]
    @tempPayment  = orderHash[:amount]
    @error        = orderHash[:error]
  end

  def prepare_pre_checks_hash_params(user)
    paramsHash = {}
    paramsHash[:user] = user
    paramsHash
  end

  def mail_pre_checks?(checks_hash)
    mail_pre_checks_on_user?(checks_hash[:user])
  end

  def mail_pre_checks_on_user?(user)
    user.orders_notifications == true
  end


end
