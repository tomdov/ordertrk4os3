class EbayUserMailer < ActionMailer::Base
  
  include ApplicationHelper
  extend CommonHelper

  default from: "#{site_name} <do-not-replay@#{site_url}>"
  layout 'mailer_layout/order_mailer'

  def end_subscription_mail(ebayUser)
    @user                       = User.find_by_id(ebayUser.user_id)
    @days_left_for_subscription = ApplicationHelper.days_diff(Date.current, Date.parse(ebayUser.expiration.to_s))
    @ebay_settings_url          = EbayUserMailer.full_site_url + ebay_settings_user_path(@user)
  	attachments['logo.jpg']     = File.read(Rails.root.to_s + "/public" + 
                                  ActionController::Base.helpers.asset_path("orderTrackingTitleLogo.jpg"))
    email_with_name             = "#{@user.name} <#{@user.email}>"
    
    mail(to: email_with_name, subject: 'Order Tracking - Ebay user end of subsription Notification')
  end

end
