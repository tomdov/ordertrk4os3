class UserMailer < ActionMailer::Base

  include ApplicationHelper
  extend CommonHelper

  default from: "#{site_name} <do-not-replay@#{site_url}>"

  def welcome_email(user)
    if pre_checks_user_mailer(prepare_pre_checks_hash(user)) == false
      return
    end

    @user = user
    @url  = UserMailer.signin_url
    email_with_name = "#{@user.name} <#{@user.email}>"
    attachments['logo.jpg'] = File.read(Rails.root.to_s + "/public" + ActionController::Base.helpers.asset_path("orderTrackingTitleLogo.jpg"))
    mail(to: email_with_name, subject: 'Welcome to Order Tracking Site') do |format|
      format.html { render layout: 'mailer_layout/user_mailer' }
      format.text { render layout: 'mailer_layout/user_mailer' }
    end
  end

  def beta_subscriber_mail(email)
    attachments['logo.jpg'] = File.read(Rails.root.to_s + "/public" + ActionController::Base.helpers.asset_path("orderTrackingTitleLogo.jpg"))
    mail(to: email, subject: 'Thank you for subscribing Order Tracking Site')
  end

  def beta_subscriber_welcome_mail(email)
    attachments['logo.jpg'] = File.read(Rails.root.to_s + "/public" + ActionController::Base.helpers.asset_path("orderTrackingTitleLogo.jpg"))
    mail(to: email, subject: 'Invitation to Order Tracking')
  end

  def beta_subscriber_reminder(subscriber)
    if subscriber.subscribed == false
      return
    end

    email = subscriber.email
    @unsubscribe_url = subscriber_unsubscribe_url(subscriber)
    attachments['logo.jpg'] = File.read(Rails.root.to_s + "/public" + ActionController::Base.helpers.asset_path("orderTrackingTitleLogo.jpg"))
    mail(to: email, subject: 'Invitation to Order Tracking - Reminder')
  end

  def forgot_password_mail(user)
    @user               =   user
    @site_url           =   UserMailer.full_site_url
    @reset_password_url =   "#{UserMailer.full_site_url}/forgot/#{@user.forgot_key.key}"

    email_with_name = "#{@user.name} <#{@user.email}>"
    mail(to: email_with_name, subject: 'Order Tracking - Reset password instructions')
  end

  def feedback_mail(user)
    if pre_checks_user_mailer(prepare_pre_checks_hash(user)) == false
      return
    end

    @user               =   user
    @site_url           =   UserMailer.full_site_url
    @form_url           =   'https://docs.google.com/forms/d/1EMJSMuqcWxFbyrJZcI-sAQCyX52rfFTTlfw6OnrVvm0/viewform?usp=send_form'
    email_with_name = "#{@user.name} <#{@user.email}>"

    mail(to: email_with_name, subject: 'Order Tracking Feedback') do |format|
      format.html { render layout: 'mailer_layout/user_mailer' }
      format.text { render layout: 'mailer_layout/user_mailer' }
    end
  end

  private

  def pre_checks_user_mailer(checks_hash)
    pre_check_user_mailer_on_user(checks_hash[:user])
  end

  def pre_check_user_mailer_on_user(user)
    user.messages_notifications?
  end

  def prepare_pre_checks_hash(user)
    checks_hash = {}
    checks_hash[:user] = user
    checks_hash
  end

  def subscriber_unsubscribe_url(subscriber)
    UserMailer.full_site_url + unsubscribe_subscriber_path(subscriber) + "?un=#{subscriber.key}"
  end

end
