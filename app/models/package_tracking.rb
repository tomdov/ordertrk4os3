# == Schema Information
#
# Table name: package_trackings
#
#  id                    :integer          not null, primary key
#  order_id              :integer
#  tracking_number       :string(255)
#  tracking_carrier      :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  aftership_checkpoints :integer          default(0)
#

class PackageTracking < ActiveRecord::Base

  attr_accessible :order_id, :tracking_carrier, :tracking_number, :id_aftership

  belongs_to :order

end
