# == Schema Information
#
# Table name: ebay_users
#
#  id                               :integer          not null, primary key
#  name                             :string(255)
#  token                            :text
#  expiration                       :datetime
#  user_id                          :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  item_paid_notification_set       :boolean          default(FALSE)
#  item_shipped_notification_set    :boolean          default(FALSE)
#  last_session_id                  :text
#  last_session_data                :text
#  last_login_time                  :datetime
#  last_get_orders_time             :datetime
#  end_subsc_two_weeks_notification :boolean          default(FALSE)
#  end_subsc_one_week_notification  :boolean          default(FALSE)
#

require 'ebay/api/ebay_api_main'

class EbayUser < ActiveRecord::Base

  include ApplicationHelper

  attr_accessible :expiration, :name, :token, :user_id,
                  :end_subsc_two_weeks_notification, :end_subsc_one_week_notification

  validates :name,                 :presence => true
  validates :token,                :presence => true
  validates :expiration,           :presence => true

  belongs_to :user

  def add_new_order(order_hash)
    desc      = order_hash[:description]
    date      = order_hash[:date]
    price     = order_hash[:price]
    link      = order_hash[:link]
    image_url = order_hash[:image_url]
    qty       = order_hash[:qty]
    if qty > 1 
      description = "#{qty} #{desc}"
    else
      description = "#{desc}"
    end

    attr = {:description => description, :site => link, :price => price, :purchase_date => date, :status => "Ordered",
             :status_date => date, :image_remote_url => image_url, :ebay_order_id => order_hash[:order_id],
             :ebay_transaction_id => order_hash[:transaction_id], :ebay_item_id => order_hash[:item_id],
             :ebay_item_n_transaction => create_item_and_transaction_str(order_hash[:item_id], order_hash[:transaction_id])}

    user = User.find_by_id(self.user_id)

    if ( user.get_default_days_notification_value != nil)
      attr.merge!(:days_notification        => user.get_default_days_notification_value,
                  :days_notification_enable => true)
    end

    order = user.orders.create!(attr)

    if order.save
      send_success_email(user, attr.merge(:link => attr[:site], :amount => attr[:price]))
    end
  end

  def get_item_price(item_hash)
    item_hash['Item']['ConvertedCurrentPrice']['Value'].to_s + " " +
        item_hash['Item']['ConvertedCurrentPrice']['CurrencyID'].to_s
  end

  def get_item_link(item_hash)
    item_hash['Item']['ViewItemURLForNaturalSearch']
  end

  def get_image_url(item_hash)
    pic_url_array = item_hash['Item']['PictureURL']
    if pic_url_array.nil?
      ""
    elsif pic_url_array.empty?
      ""
    else
      pic_url_array.first
    end
  end

  def get_item_description(item_hash)
    item_hash['Item']['Title']
  end

  def get_additional_info_for_item(item_id)
    response = @api.get_single_item(:item_id => item_id.to_s)
    ret_hash = {}
    #ret_hash[:ack] = response['Ack']
    #if ret_hash[:ack] != 'Success'
    #  return ret_hash
    #end

    ret_hash[:price] = get_item_price(response)
    ret_hash[:link] = get_item_link(response)
    ret_hash[:image_url] = get_image_url(response)
    ret_hash[:description] = get_item_description(response)
    ret_hash
  end

  def parse_event_item_marked_paid(event_details)
    order_hash = {}
    order_hash[:description] = event_details['Title']
    order_hash[:item_id] = event_details['ItemID']
    order_hash[:order_id] = event_details['OrderID']
    order_hash[:transaction_id] = event_details['TransactionID']
    order_hash[:date] = convert_ebay_timestamp_to_date(event_details['Timestamp'])
    order_hash
  end

  def parse_event_item_marked_shipped(event_details)
    order_hash = {}
    order_hash[:order_id] = event_details['OrderID']
    order_hash[:item_id] = event_details['ItemID']
    order_hash[:transaction_id] = event_details['TransactionID']
    order_hash
  end

  def duplicate_order(order_hash)
    !get_order_by_ebay_unique_number(create_item_and_transaction_str(order_hash[:item_id], order_hash[:transaction_id])).nil?
  end

  def handle_event_item_marked_paid(event)
    order_hash = parse_event_item_marked_paid(event)
    if duplicate_order(order_hash)
      return
    end
    item_id = order_hash[:item_id]
    order_hash.merge!(get_additional_info_for_item(item_id))
    add_new_order(order_hash)
  end

  def handle_event_item_marked_shipped(event)
    order_hash = parse_event_item_marked_shipped(event)
    #  find order in DB
    order_from_db = get_order_by_ebay_unique_number(create_item_and_transaction_str(order_hash[:item_id], order_hash[:transaction_id]))

    if order_from_db.nil?
      puts "ERROR: Order that not in the list is marked as shipped"
    #  add new order to DB
      handle_event_item_marked_paid(event)
    else
      #  update order & mark as shipped
      #  shipment_tracking_number = event['Shipment']['ShipmentTrackingNumber']
      #  shipping_carrier = event['Shipment']['ShippingCarrierUsed']
      shipped_marked_order = mark_order_as_shipped(order_from_db)
      if save_marked_order_to_db(shipped_marked_order)
        order_hash = prepare_order_hash_from_order(order_from_db)
        send_order_shipped_mail(order_from_db.user, order_hash)
      end
    end

  end

  def set_notifications_ex(bEnabled)
    sNewState = bEnabled ? "Enable" : "Disable"

    notification2 = EbayAPI::Classes::NotificationEnable.new(
        :event_type => "ItemMarkedShipped", :event_enable => sNewState
    )

    notifications = []
    notifications << notification2

    user_delivery_preferences = EbayAPI::Classes::UserDeliveryPreferenceArray.new(
        :notification_enable => notifications
    )


    req_cred = EbayAPI::Classes::RequesterCredentials.new(
        :ebay_auth_token => self.token
    )

    app_delivery_opt = EbayAPI::Classes::ApplicationDeliveryPreferences.new( :application_enable => 'Enable', :alert_enable => 'Enable' )

    response = @api.set_notification_preferences( :requester_credentials => req_cred,
                                                    :user_delivery_preferences => user_delivery_preferences,
                                                    :application_delivery_preferences =>  app_delivery_opt )

  end

  def set_notifications(api = nil)

    if (!api.nil?)
      @api = api
    end

    set_notifications_needed = !(self.item_shipped_notification_set)
    puts "set notifications needed = " + set_notifications_needed.to_s

    if !set_notifications_needed
      return
    end

    begin

      set_notifications_ex(true)
      self.item_shipped_notification_set = true
      self.save
    rescue EbayAPI::RequestError, EbayAPI::ServerError
      puts "Error occured on set_notification_preferences"
    end
  end

  def unset_notifications(api = nil)
    if (!api.nil?)
      @api = api
    end

    if self.item_shipped_notification_set
      begin
        set_notifications_ex(false)
        self.item_shipped_notification_set = false
        self.save
      rescue EbayAPI::RequestError, EbayAPI::ServerError
        puts "Error occured on unset_notification_preferences"
      end
    end
  end

  def get_client_alerts_auth_token
    req_cred = EbayAPI::Classes::RequesterCredentials.new(
        :ebay_auth_token => self.token
    )

    response = @api.get_client_alerts_auth_token( :requester_credentials => req_cred )
    response.client_alerts_auth_token
  end

  def login_user(token)
    response = @api.login( :client_alerts_auth_token => CGI.escape(token))
    get_and_save_login_data(response)
    response
  end

  def get_user_alerts(login_hash)
    encoded_session_id = CGI.escape(login_hash['SessionID'])
    encoded_session_data = CGI.escape(login_hash['SessionData'])

    response = @api.get_user_alerts(:session_id => encoded_session_id,
                                    :session_data => encoded_session_data)
    save_last_session_data(response['SessionData'])
    #logout_response = @api.logout( :session_id => encoded_session_id, :session_data => CGI.escape(response['SessionData']) )

    response
  end

  def get_notifications_updates
    if check_if_login_required
      token = get_client_alerts_auth_token
      login_user(token)
    end

    login_hash = get_login_hash
    get_user_alerts(login_hash)
  end

  def parse_notifications(notifications)
    alerts_array = get_alerts_array_from_response(notifications)
    handle_alerts(alerts_array)

  end

  def update_account(api_instance)
    @api = api_instance
    set_notifications
    new_orders = get_new_orders
    parse_get_new_orders(new_orders)
    notifications = get_notifications_updates
    parse_notifications(notifications)
  end

  def EbayUser.update

    api = EbayAPI::Api.new

    EbayUser.all.each do |ebay_user|
      begin
        ebay_user.update_account(api)
      rescue Exception => e
        puts "Error while trying to update the user from eBay. message = " + e.message
        puts e.backtrace.inspect
      end
    end

  end


  def handle_event(event, api = nil)
    if !api.nil?
      @api = api
    end

    case event_type(event)
      when "ItemMarkedPaid"
        event_details = event['ItemMarkedPaid']
        handle_event_item_marked_paid(event_details)
      when "ItemMarkedShipped"
        event_details = event['ItemMarkedShipped']
        handle_event_item_marked_shipped(event_details)
    end
  end


  def parse_get_new_orders(response, api = nil)
    if !api.nil?
      @api = api
    end
    response.orders.each do |order|
      order.transactions.each do |transaction|
        transaction_details = create_transaction_details_hash(transaction.transaction_id, transaction.item.item_id, order.order_id, transaction.quantity_purchased)
        handle_transaction(transaction_details)
      end
    end
  end

  def handle_alerts(alerts_array, api = nil)

    if alerts_array.nil?
      return
    end

    if !api.nil?
      @api = api
    end

    alerts_array.each do |event|
      handle_event(event)
    end
  end

  def EbayUser.send_end_subsription_mail
    EbayUser.all.each do |ebayUser|
        ebayUser.send_end_subsription_mail
    end
  end

  def send_end_subsription_mail
      if need_to_send_end_subsription_mail
        send_end_subsription_mail_do
      end
  end


  private

  def need_to_send_end_subsription_mail
    # check if need to send two weeks notice one week notice
   
    days_for_first_notif  = 14
    days_for_second_notif  = 7
    days_for_expiration = ApplicationHelper.days_diff(Date.current, Date.parse(self.expiration.to_s))
    return  ((days_for_expiration <= days_for_second_notif) &&
              self.end_subsc_one_week_notification == false) ||
              ((days_for_expiration <= days_for_first_notif) &&
              self.end_subsc_two_weeks_notification == false)                    
  end

  def send_end_subsription_mail_do

    days_for_first_notif  = 14
    days_for_second_notif  = 7
    send_notif = false
    days_for_expiration = ApplicationHelper.days_diff(Date.current, Date.parse(self.expiration.to_s))
    if days_for_expiration <= days_for_first_notif    
      self.update_attribute(:end_subsc_two_weeks_notification, true)
      send_notif = true
    end

    if days_for_expiration <= days_for_second_notif
      self.update_attribute(:end_subsc_one_week_notification, true)
      send_notif = true
    end

    if send_notif == true
      EbayUserMailer.end_subscription_mail(self).deliver
    end

  end

  def save_last_get_last_order_time(time)
    self.last_get_orders_time = time
    self.save
  end

  def get_last_get_order_time
    if self.last_get_orders_time.nil?
      get_current_time
    else
      self.last_get_orders_time
    end
  end

  def get_new_orders
    req_cred = EbayAPI::Classes::RequesterCredentials.new(
        :ebay_auth_token => self.token
    )

    output_selector = "OrderID,ItemID,TransactionID,QuantityPurchased"

    last_get_orders_fetch_time = get_last_get_order_time
    response = @api.get_orders(:requester_credentials => req_cred, :output_selector => output_selector, :create_time_from => last_get_orders_fetch_time, :create_time_to => get_current_time, :order_role => "Buyer", :order_status => "Completed")
    save_last_get_last_order_time(get_current_time)
    response
  end

  def create_transaction_details_hash(transaction_id, item_id, order_id, qty)
    transaction_details = {}
    transaction_details[:transaction_id]  = transaction_id
    transaction_details[:item_id]         = item_id
    transaction_details[:order_id]        = order_id
    transaction_details[:qty]             = qty
    transaction_details
  end

  def duplicate_order_by_orderline(transaction_details)
    item_transaction_str = create_item_and_transaction_str(transaction_details[:item_id], transaction_details[:transaction_id].to_s)
    !get_order_by_ebay_unique_number(item_transaction_str).nil?
  end

  def handle_transaction(transaction_details)
    order_hash = {}
    if duplicate_order_by_orderline(transaction_details)
      return
    end

    item_id = transaction_details[:item_id]
    order_hash.merge!(get_additional_info_for_item(item_id))
    order_hash.merge!(transaction_details)
    order_hash[:date] = Date.today
    add_new_order(order_hash)
  end

  def create_item_and_transaction_str(item_id, transaction_id)
    "#{item_id.to_s}-#{transaction_id.to_s}"
  end

  def get_current_time
    Time.now.utc
  end

  def get_and_save_login_data(login_response)
    self.last_session_id = login_response['SessionID']
    self.last_session_data = login_response['SessionData']
    set_last_login_time(get_current_time)
    self.save
  end

  def set_last_login_time(time)
    self.last_login_time = time
  end

  def get_login_hash
    login_hash = {}
    login_hash['SessionID'] = self.last_session_id
    login_hash['SessionData'] = self.last_session_data
    login_hash
  end

  def get_hours_from_last_login
    hours = hours_between_timestamps(self.last_login_time, get_current_time)
    puts "hours from last login = " + hours.to_s
    hours
  end

  def check_if_login_required
    self.last_login_time.nil? || (get_hours_from_last_login > 24.5)
  end

  def prepare_order_hash_from_order(order_from_db)
    order_hash = {}
    order_hash[:description] = order_from_db.description
    order_hash[:link] = order_from_db.site
    order_hash[:amount] = order_from_db.price
    order_hash
  end

  def send_success_email(user, orderHash)
    OrderMailer.new_order_success(user, orderHash).deliver
  end

  def mark_order_as_shipped(order)
    order.status = "Shipped"
    order.status_date = Date.today
    order
  end

  def save_marked_order_to_db(order)
    order.save
  end

  def get_order_by_ebay_unique_number(item_n_transacation_id)
    order = Order.find_by_ebay_item_n_transaction(item_n_transacation_id.to_s)
    order
  end

  def get_last_session_data
    self.last_session_data
  end

  def save_last_session_data(session_data)
    if session_data.nil?
      puts "save_last_session_data - received nil session_data"
      return
    end
    self.last_session_data = session_data
    self.save
  end

  def get_alerts_array_from_response(response)
    if response['ClientAlerts'] != nil
      response['ClientAlerts']['ClientAlertEvent']
    else
      nil
    end
  end

  def event_type(event)
    event['EventType']
  end

  def convert_ebay_timestamp_to_date(timestamp)
    Date.parse(timestamp)
  end

  def send_order_shipped_mail(user, order_hash)
    order_hash[:previous_state] = "Ordered"
    order_hash[:new_state]      = "Shipped"
    send_status_changed_mail(user, order_hash)
  end

  def send_status_changed_mail(user, hash)
    OrderMailer.order_status_changed(user, hash).deliver
  end
end


