# == Schema Information
#
# Table name: subscribers
#
#  id                    :integer          not null, primary key
#  email                 :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  signup_mail_sent      :boolean          default(FALSE)
#  key                   :string(255)
#  subscribed            :boolean          default(TRUE)
#  welcome_reminder_sent :boolean          default(FALSE)
#

class Subscriber < ActiveRecord::Base
  attr_accessible :email

  @email_regex = /\A[\w\+\-.]+@([\da-zA-Z\-.])*[\.]([a-zA-Z\..])+\z/i

  validates :email, :presence => true,
            :format   => { :with => @email_regex},
            :uniqueness => { :case_sensitive => false }

  def Subscriber.send_welcome_mails
    Subscriber.all.each do | subscriber |
      if !(subscriber.signup_mail_sent?)
        UserMailer.beta_subscriber_welcome_mail(subscriber.email).deliver
        subscriber.signup_mail_sent = true
        subscriber.save
      end
    end
  end

  def add_unsubscribe_key
    self.set_unsubscribe_key(SecureRandom.hex(64))
  end

  def set_unsubscribe_key(key)
    self.key = key
  end

  def Subscriber.add_unsubscribe_keys
    Subscriber.all.each do |sub|
      if sub.key.nil?
        sub.add_unsubscribe_key
        sub.save
      end
    end
  end

  def welcome_sent_long_time_ago?
    ApplicationHelper.days_diff(Date.parse(self.created_at.to_s), Date.today) >= 14
  end

  def send_welcome_reminder
    if welcome_sent_long_time_ago? && welcome_reminder_sent == false &&
        User.find_by_email(self.email).nil?
      UserMailer.beta_subscriber_reminder(self).deliver
      self.welcome_reminder_sent = true
      self.save
    end

  end

  def Subscriber.send_welcome_reminders
    Subscriber.all.each do |sub|
      sub.send_welcome_reminder
    end
  end

end
