# == Schema Information
#
# Table name: user_mails
#
#  id         :integer          not null, primary key
#  name       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserMail < ActiveRecord::Base
  attr_accessible :name
  validates :name,  :presence => true

  has_and_belongs_to_many :users

  def UserMail.send_mail_to_users(mail_name, limit_num_of_users = 0 )
    @counter = 0
    user_mail = UserMail.find_by_name(mail_name)
    if user_mail.nil?
      logger.error "#{__method__}: mail_id is nil!!!"
      return
    end

    User.all.each do |user|
      sent = UserMail.send_mail_to_user(user, user_mail)
      if sent
        @counter += 1
        if limit_num_of_users > 0 && @counter >= limit_num_of_users
          break
        end
      end
    end
  end

  def UserMail.send_mail_to_user(user, user_mail)
    user_mailer_function_name = "#{user_mail.name}_mail"
    unless user.user_mail_ids.include?(user_mail.id)
      mail = UserMailer.send(user_mailer_function_name, user).deliver if UserMailer.respond_to?(user_mailer_function_name)
      unless mail.nil?
        user.user_mails << user_mail
        user.save
        return true
      end
    end

    false
  end



end
