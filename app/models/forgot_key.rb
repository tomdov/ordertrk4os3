# == Schema Information
#
# Table name: forgot_keys
#
#  id         :integer          not null, primary key
#  key        :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ForgotKey < ActiveRecord::Base
  attr_accessible :key, :user_id

  belongs_to :user
end
