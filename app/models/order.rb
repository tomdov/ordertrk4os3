# == Schema Information
#
# Table name: orders
#
#  id                       :integer          not null, primary key
#  description              :text
#  site                     :text
#  purchase_date            :date
#  status                   :string(255)
#  status_date              :date
#  notes                    :text
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  price                    :string(255)
#  user_id                  :integer
#  days_notification        :integer          default(0)
#  days_notification_sent   :boolean          default(FALSE)
#  days_notification_enable :boolean          default(FALSE)
#  image_file_name          :string(255)
#  image_content_type       :string(255)
#  image_file_size          :integer
#  image_updated_at         :datetime
#  image_remote_url         :text
#  shipment_tracking_number :string(255)
#  shipping_carrier         :string(255)
#  ebay_order_id            :string(255)      default("0")
#  ebay_transaction_id      :string(255)
#  ebay_item_id             :string(255)
#  ebay_item_n_transaction  :string(255)
#

require 'open-uri'
class Order < ActiveRecord::Base


  has_attached_file :image,
                    :styles => { :medium => '300x300>', :thumb => '150x150>' },
                    :default_url => ActionController::Base.helpers.asset_path("fallback/" + ["order_image", "default.jpg"].compact.join('_')),
                    :storage => :s3,
                    :s3_protocol => 'https',
                    #:s3_permissions => 'private',
                    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')


  attr_accessible :description, :notes, :purchase_date, :site, :status, :status_date, :price, :days_notification,
                  :days_notification_sent, :days_notification_enable, :image, :image_remote_url, :ebay_order_id,
                  :ebay_transaction_id, :ebay_item_id, :ebay_item_n_transaction, :package_tracking_attributes


  validates :description,                 :presence => true
  validates :site,                        :presence => true
  validates :status,                      :presence => true
  validates :days_notification, numericality: { only_integer: true }

  validate :check_validity_days_notification
  validate :check_valid_dates
  validate :image_from_url

  #validates_presence_of :image_remote_url
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/gif", "image/png", "image/jpeg"], :message => "The URL doesn't contain valid content type"
  validates_attachment_size :image, :in => 0..2.megabytes, :message => 'exceeding the size limit'
  validate :remove_redundency_from_errors

  belongs_to :user
  has_one  :package_tracking, :dependent => :destroy
  accepts_nested_attributes_for :package_tracking
  default_scope :order => 'orders.purchase_date'

  def check_validity_days_notification
    if days_notification_enable && (days_notification.nil? || 
      (days_notification <= 0 || days_notification > 365))
      errors.add(:days_notification, ': You should specifiy days (>0) and (<366) for notification')
    end
  end

  def days_notification_needed_to_be_send?
    order = self
    (order.status == "Ordered" || order.status == "Shipped") &&
    order.days_notification_enable && !order.days_notification_sent &&
        (ApplicationHelper.days_diff(order.purchase_date, Date.current) >= order.days_notification)
  end

  def send_one_days_notification
    if self.days_notification_needed_to_be_send?
      OrderMailer.days_notification_mail(self).deliver
    end
  end

  def Order.send_days_notifications
    Order.all.each do |order|
      order.send_one_days_notification
    end
  end

  def self.live_orders(user)
    where("user_id = :user_id AND (status = 'Ordered' OR status = 'Shipped' OR status = 'Delivered')", :user_id => user)

  end

  def self.archive_orders(user)
    orders = where("user_id = :user_id AND (status = 'Picked' OR status = 'Cancelled')", :user_id => user)
  end

  def get_image_remote_url_from_db
    order = Order.find_by_id(self.id)
    order ? order.image_remote_url : nil
  end

  def image_url_changed
    new_record? || (self.image_remote_url != get_image_remote_url_from_db)
  end

  def image_from_url
    if !self.image_remote_url.blank?
      begin
        if image_url_changed
          self.image = open(URI.parse(self.image_remote_url))
        end
      rescue
        self.image = nil
        errors.add(:image_remote_url, ": URL doesn't contain valid image")
      end

    else
      self.image = nil
    end
  end

  def remove_redundency_from_errors
    errors.delete(:image) if !errors[:image].nil?
    errorsTempHash = {}
    errors.messages.each do |key, val|
      errorsTempHash[key] = val.uniq
      errors.messages.delete(key)
    end

    errors.messages.merge!(errorsTempHash)
  end

  def check_valid_dates
    begin
      Date.parse(self.purchase_date.to_s)
    rescue
      errors.add(:purchase_date, " isn't valid date")
    end

    if :purchase_date != :status_date
      begin
        Date.parse(self.status_date.to_s)
      rescue
        errors.add(:status_date, " isn't valid date, date format should be YYYY-MM-DD")
      end
    end
  end
end
