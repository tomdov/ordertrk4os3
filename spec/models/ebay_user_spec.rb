# == Schema Information
#
# Table name: ebay_users
#
#  id                               :integer          not null, primary key
#  name                             :string(255)
#  token                            :text
#  expiration                       :datetime
#  user_id                          :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  item_paid_notification_set       :boolean          default(FALSE)
#  item_shipped_notification_set    :boolean          default(FALSE)
#  last_session_id                  :text
#  last_session_data                :text
#  last_login_time                  :datetime
#  last_get_orders_time             :datetime
#  end_subsc_two_weeks_notification :boolean          default(FALSE)
#  end_subsc_one_week_notification  :boolean          default(FALSE)
#

require 'spec_helper'

describe EbayUser do

  describe "validations" do
    before (:each) do
      @attr = { :name => "user1", :token => "some token", :expiration => Time.now }
    end

    it "should create a new instance given a valid attr" do
      EbayUser.create!(@attr)
    end

    it "should require name" do
      res = EbayUser.new(@attr.merge(:name => ""))
      res.should_not be_valid
    end

    it "should require token" do
      res = EbayUser.new(@attr.merge(:token => ""))
      res.should_not be_valid
    end

    it "should require expiration" do
      res = EbayUser.new(@attr.merge(:expiration => nil))
      res.should_not be_valid
    end

    describe "interaction with user" do
      before(:each) do
        @user = FactoryGirl.create(:user)
        @ebay_user = @user.ebay_users.create!(@attr)
      end

      it "should associate the ebay user to the right user" do
        @user.ebay_users.should include(@ebay_user)
        @ebay_user.user_id.should == @user.id
        @ebay_user.user.should == @user
      end

    end

    def ebay_api_path
      Pathname.new(File.dirname(__FILE__)).join('..','fixtures','ebay')
    end

    describe "subscription end mails" do

        before(:each) do
          @user = FactoryGirl.create(:user)

          @ebay_user_attr = { :name => "Nick",
                              :token => "thisistoken",
                              :expiration => DateTime.tomorrow + 1.day
          }
          @ebay_user = @user.ebay_users.create!(@ebay_user_attr)
        end

        it "should send mail to the user" do
          lambda do
            EbayUser.send_end_subsription_mail
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
          mail = ActionMailer::Base.deliveries.last
          mail.to.size.should == 1
          mail.to.should include(@user.email)
          mail.subject.should == 'Order Tracking - Ebay user end of subsription Notification'
          mail.body.encoded.should include("1 day")
        end


        it "should not send mail twice" do
          EbayUser.send_end_subsription_mail
          lambda do
            EbayUser.send_end_subsription_mail
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end

        it "should not send the same notification twice" do
          @ebay_user.update_attribute(:expiration, DateTime.now + 10.days)
          lambda do
            EbayUser.send_end_subsription_mail
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
          
          @ebay_user.update_attribute(:expiration, DateTime.now + 9.days)
          lambda do
            EbayUser.send_end_subsription_mail
          end.should_not change(ActionMailer::Base.deliveries, :size)

          #change the time for second notification to be sent
          @ebay_user.update_attribute(:expiration, DateTime.now + 6.days)
          lambda do
            EbayUser.send_end_subsription_mail
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
          
          @ebay_user.update_attribute(:expiration, DateTime.now + 5.days)
          lambda do
            EbayUser.send_end_subsription_mail
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end

        it "should send 2 mails notification" do
          @ebay_user.update_attribute(:expiration, DateTime.now + 9.days)
          lambda do
            EbayUser.send_end_subsription_mail
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
          mail = ActionMailer::Base.deliveries.last
          mail.to.size.should == 1
          mail.to.should include(@user.email)
          mail.subject.should == 'Order Tracking - Ebay user end of subsription Notification'
          mail.body.encoded.should include("9 days")
          @ebay_user.reload
          @ebay_user.end_subsc_two_weeks_notification.should == true
          @ebay_user.end_subsc_one_week_notification.should == false

          @ebay_user.update_attribute(:expiration, DateTime.now + 1.day)
          lambda do
            EbayUser.send_end_subsription_mail
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
          mail = ActionMailer::Base.deliveries.last
          mail.to.size.should == 1
          mail.to.should include(@user.email)
          mail.subject.should == 'Order Tracking - Ebay user end of subsription Notification'
          mail.body.encoded.should include("1 day")
          @ebay_user.reload
          @ebay_user.end_subsc_two_weeks_notification.should == true
          @ebay_user.end_subsc_one_week_notification.should == true
        end

        it "should contain correct redirect url" do
          lambda do
            EbayUser.send_end_subsription_mail
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
          mail = ActionMailer::Base.deliveries.last
          raise mail.body.encoded.inspect
          mail.body.encoded.should include("ordertrk.com/users/#{@user.id}/ebay_settings")
        end

    end

    describe "notification handling" do

      before(:each) do
        @user = FactoryGirl.create(:user)

        @ebay_user_attr = { :name => "Nick",
                            :token => "thisistoken",
                            :expiration => DateTime.tomorrow
        }
        @ebay_user = @user.ebay_users.create!(@ebay_user_attr)
        @api = EbayAPI::Api.new
      end
      describe "get orders" do
        before(:each) do
          get_orders_xml = ebay_api_path.join("get_orders_with_output_selector.xml")

          xml = REXML::Document.new(get_orders_xml.read)
          # Fixes the wrong case of API returned by eBay
          xml.root.name = xml.root.name.gsub(/API/, 'Api')
          # Fix lowercased Xsl in response document
          xml.root.name = xml.root.name.gsub(/XslResponse$/, 'XSLResponse')
          @orders = XML::Mapping.load_object_from_xml(xml.root)

        end

        describe "add orders with default days for notification" do
          it "should add the order without default days for the notification" do
            @ebay_user.parse_get_new_orders(@orders, @api)
            Order.last.days_notification.should == 0
          end
         
          it "should add the order with default days for the notification" do
            @user.update_attribute(:default_days_notification, 1)            
            @ebay_user.parse_get_new_orders(@orders, @api)
            Order.last.days_notification.should == 1
          end

        end

        it "should add an order" do
          lambda do
            @ebay_user.parse_get_new_orders(@orders, @api)
          end.should change(Order, :count).by(4)
        end

        it "should not accept duplicates" do
          @ebay_user.parse_get_new_orders(@orders, @api)
          lambda do
            @ebay_user.parse_get_new_orders(@orders, @api)
          end.should_not change(Order, :count)
        end

        it "should send mail to the user" do

          lambda do
            @ebay_user.parse_get_new_orders(@orders, @api)
          end.should change(ActionMailer::Base.deliveries, :size).by(4)
          mail = ActionMailer::Base.deliveries.last
          mail.to.size.should == 1
          mail.to.should include(@user.email)
          mail.subject.should == 'Order Tracking - New order received'
        end

        describe "two users" do
          before(:each) do
            @user2 = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
            @ebay_user_attr2 = { :name => "Nick2",
                                :token => "thisistoken2",
                                :expiration => DateTime.tomorrow
            }

            @ebay_user2 = @user2.ebay_users.create!(@ebay_user_attr2)
          end

          it "should add the order to the right user" do
            lambda do
              @ebay_user.parse_get_new_orders(@orders, @api)
            end.should change(@ebay_user.user.orders, :count).by(4)
          end

          it "should add the order to the right user (second check)" do
            lambda do
              @ebay_user2.parse_get_new_orders(@orders, @api)
            end.should change(@ebay_user2.user.orders, :count).by(4)
          end

          it "should send mail to the user" do

            lambda do
              @ebay_user.parse_get_new_orders(@orders, @api)
            end.should change(ActionMailer::Base.deliveries, :size).by(4)
            mail = ActionMailer::Base.deliveries.last
            mail.to.size.should == 1
            mail.to.should include(@user.email)
            mail.subject.should == 'Order Tracking - New order received'
          end


          it "should send mail to the user (second check)" do

            lambda do
              @ebay_user2.parse_get_new_orders(@orders, @api)
            end.should change(ActionMailer::Base.deliveries, :size).by(4)
            mail = ActionMailer::Base.deliveries.last
            mail.to.size.should == 1
            mail.to.should include(@user2.email)
            mail.subject.should == 'Order Tracking - New order received'
          end

        end

      end

      describe "ItemMarkedShipped" do
        before(:each) do
          get_orders_xml = ebay_api_path.join("get_orders_one_order_with_output-selector.xml")

          xml = REXML::Document.new(get_orders_xml.read)
          # Fixes the wrong case of API returned by eBay
          xml.root.name = xml.root.name.gsub(/API/, 'Api')
          # Fix lowercased Xsl in response document
          xml.root.name = xml.root.name.gsub(/XslResponse$/, 'XSLResponse')
          @orders = XML::Mapping.load_object_from_xml(xml.root)

          item_marked_shipped = ebay_api_path.join("item_shipped.json")
          @shipped_notification = JSON.parse(item_marked_shipped.read)
        end

        it "should turn the order to shipped state" do

            @ebay_user.parse_get_new_orders(@orders, @api)
            @ebay_user.handle_event(@shipped_notification['ClientAlerts']['ClientAlertEvent'].first, @api)
            order = Order.find_by_ebay_order_id("110125372390-919293865003")
            order2 = Order.find_by_ebay_item_n_transaction("110125372390-919293865003")
            order.should_not be_nil
            order.should == order2
            order.status.should == "Shipped"
            order.status_date.should == Date.today
        end

        it "should send mail to the user" do
          @ebay_user.parse_get_new_orders(@orders, @api)
          lambda do
            @ebay_user.handle_event(@shipped_notification['ClientAlerts']['ClientAlertEvent'].first, @api)
          end.should change(ActionMailer::Base.deliveries, :size).by(1)

          mail = ActionMailer::Base.deliveries.last
          mail.to.size.should == 1
          mail.to.should include(@user.email)
          mail.subject.should == 'Order Tracking - Order status changed notification'
          mail.body.encoded.should include("Previous status: Ordered")
          mail.body.encoded.should include("New status: Shipped")
        end

        it "should add a new order when order not exists" do
          lambda do
            @ebay_user.handle_event(@shipped_notification['ClientAlerts']['ClientAlertEvent'].first, @api)
          end.should change(Order, :count).by(1)
        end

      end
    end

    describe "settings notifications" do
      before(:each) do
        @api = EbayAPI::Api.new
        @user = FactoryGirl.create(:user)

        @ebay_user_attr = { :name => "Nick",
                            :token => EbayAPI::ConfigAPI.instance.auth_token,
                            :expiration => DateTime.tomorrow
        }
        @ebay_user = @user.ebay_users.create!(@ebay_user_attr)

      end

      it "should enable the notifications" do
        @ebay_user.set_notifications(@api)

        req_cred = EbayAPI::Classes::RequesterCredentials.new(
            :ebay_auth_token => @api.auth_token
        )

        response = @api.get_notification_preferences( :requester_credentials => req_cred, :preference_level => 'User' )
        response.user_delivery_preferences.each do |event|
          if event.event_type == "ItemMarkedShipped"
            event.event_enable.should == "Enable"
          end
        end
      end

      it "should disable the notifications" do
        @ebay_user.item_shipped_notification_set = true
        @ebay_user.unset_notifications(@api)
        req_cred = EbayAPI::Classes::RequesterCredentials.new(
            :ebay_auth_token => @api.auth_token
        )

        response = @api.get_notification_preferences( :requester_credentials => req_cred, :preference_level => 'User' )
        response.user_delivery_preferences.each do |event|
          if event.event_type == "ItemMarkedShipped"
            event.event_enable.should == "Disable"
          end
        end

      end

    end


  end
end
