# == Schema Information
#
# Table name: subscribers
#
#  id                    :integer          not null, primary key
#  email                 :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  signup_mail_sent      :boolean          default(FALSE)
#  key                   :string(255)
#  subscribed            :boolean          default(TRUE)
#  welcome_reminder_sent :boolean          default(FALSE)
#

require 'spec_helper'

describe Subscriber do
  before(:each) do
    @attr = {:email=>"t.t@t.net"}
  end

  it "should create a new instance given a valid attr" do
    subs = Subscriber.create!(@attr)
    subs.should be_valid
    subs.should_not be_nil
  end

  it "should require a email" do
    Subscriber.new(@attr.merge(:email => "")).should_not be_valid
  end

  it " should accept valid email address" do
    addresses = %w[user@foo.com THIS_IS_FULL_CAPS@GMAIL.COM hey.now@g.net]
    addresses.each do |address|
      valid_email = Subscriber.new(:email => address)
      valid_email.should be_valid
    end

  end

  it "should not accept invalid email addresses" do
    addresses = %w[us@er@foo.com THIS_IS_FULL_CAPS@GMAIL,COM hey.now@g.]
    addresses.each do |address|
      invalid_email = Subscriber.new(:email => address)
      invalid_email.should_not be_valid
    end
  end

  it "should reject duplicate email addresses" do
    Subscriber.create!(@attr)
    dup_email = Subscriber.new(@attr)
    dup_email.should_not be_valid
  end

  it "should reject emails identical up to case" do
    mail = "user@expl.com"
    Subscriber.create!(@attr.merge(:email => mail))
    same_email = Subscriber.new(@attr.merge(:email => mail.upcase))
    same_email.should_not be_valid
  end

  describe "welcome mail" do
    before(:each) do
      Subscriber.create!(@attr)
    end
    it "should send a mail" do
      Subscriber.send_welcome_mails
      ActionMailer::Base.deliveries.should_not be_empty
    end

    it "should send exctly on email per user" do
      lambda do
        Subscriber.send_welcome_mails
      end.should change(ActionMailer::Base.deliveries, :size).by(1)
    end

    it "should send the right subject" do
      Subscriber.send_welcome_mails
      welcome_mail = ActionMailer::Base.deliveries.last
      welcome_mail.subject.should == 'Invitation to Order Tracking'
    end

    it "should send the right 'from field'" do

      Subscriber.send_welcome_mails
      site_url = UserMailer.site_url
      welcome_mail = ActionMailer::Base.deliveries.last
      welcome_mail.from.should include("do-not-replay@#{site_url}")
    end

    it "should send the email to the right user" do
      Subscriber.send_welcome_mails
      welcome_mail = ActionMailer::Base.deliveries.last
      welcome_mail.to.size.should == 1
      welcome_mail.to.should include(@attr[:email])
    end

    it "should contain welcome message" do
      Subscriber.send_welcome_mails
      welcome_mail = ActionMailer::Base.deliveries.last
      welcome_mail.body.encoded.should =~ /We hope that you'll like the site and make an efficient use of it./
    end

    it "should turn the mail flag to 'sent'" do
      Subscriber.send_welcome_mails
      Subscriber.find_by_email("t.t@t.net").signup_mail_sent.should == true
    end

    it "should have link to signup" do
      Subscriber.send_welcome_mails
      welcome_mail = ActionMailer::Base.deliveries.last
      welcome_mail.body.encoded.should =~ /www.ordertrk.com\/signup/i
    end


  end

  describe "welcome mail reminder" do
    before(:each) do
      @subscriber = FactoryGirl.create(:subscriber)
    end

    it "should send it after enough time from welcome mail sent" do
      @subscriber.created_at = Date.today - 15
      @subscriber.save
      lambda do
        @subscriber.send_welcome_reminder
      end.should change(ActionMailer::Base.deliveries, :size).by(1)
    end

    it "should not send it before time" do
      @subscriber.created_at = Date.today - 12
      @subscriber.save
      lambda do
        @subscriber.send_welcome_reminder
      end.should_not change(ActionMailer::Base.deliveries, :size)
    end

    it "should send it only once" do
      @subscriber.created_at = Date.today - 15
      @subscriber.save
      @subscriber.send_welcome_reminder
      lambda do
        @subscriber.send_welcome_reminder
      end.should_not change(ActionMailer::Base.deliveries, :size)
    end

    it "should send it only to non-users" do
      FactoryGirl.create(:user, :email => @subscriber.email)
      lambda do
        @subscriber.send_welcome_reminder
      end.should_not change(ActionMailer::Base.deliveries, :size)
    end

    it "should send it only to subscribed users" do
      @subscriber.created_at = Date.today - 15
      @subscriber.save
      @subscriber.subscribed.should be_true
      lambda do
        @subscriber.send_welcome_reminder
      end.should change(ActionMailer::Base.deliveries, :size).by(1)
    end

    it "should not send to unsubscribed user" do
      @subscriber.subscribed = false
      lambda do
        @subscriber.send_welcome_reminder
      end.should_not change(ActionMailer::Base.deliveries, :size)
    end
  end
end
