# == Schema Information
#
# Table name: orders
#
#  id                       :integer          not null, primary key
#  description              :text
#  site                     :text
#  purchase_date            :date
#  status                   :string(255)
#  status_date              :date
#  notes                    :text
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  price                    :string(255)
#  user_id                  :integer
#  days_notification        :integer          default(0)
#  days_notification_sent   :boolean          default(FALSE)
#  days_notification_enable :boolean          default(FALSE)
#  image_file_name          :string(255)
#  image_content_type       :string(255)
#  image_file_size          :integer
#  image_updated_at         :datetime
#  image_remote_url         :text
#  shipment_tracking_number :string(255)
#  shipping_carrier         :string(255)
#  ebay_order_id            :string(255)      default("0")
#  ebay_transaction_id      :string(255)
#  ebay_item_id             :string(255)
#  ebay_item_n_transaction  :string(255)
#

require 'spec_helper'

describe Order do
  before(:each) do
    @attr = {:description => "example desc", :site => "Ebay", :purchase_date => Date.current.to_s, :status => "Ordered",
             :status_date => Date.current.to_s, :notes => "Bla bla bla"}
  end

  it "should create a new instance given a valid attr" do
    Order.create!(@attr)
  end

  it "should require a description" do
    order_wo_description = Order.new(@attr.merge(:description => ""))
    order_wo_description.should_not be_valid
  end

  it "should require a site" do
    order_wo_site = Order.new(@attr.merge(:site => ""))
    order_wo_site.should_not be_valid
  end

  it "should require a status" do
    order_wo_status = Order.new(@attr.merge(:status => ""))
    order_wo_status.should_not be_valid
  end

  it "should require a >0 days for notification" do
    order = Order.new(@attr.merge(:days_notification_enable => true))
    order.should_not be_valid
  end

  it "should not allow string in the days field" do
    order = Order.new(@attr.merge(:days_notification_enable => true, :days_notification => "not valid"))
    order.should_not be_valid
  end

  it "should accept valid date format" do
    order = Order.new(@attr.merge(:purchase_date => "2000-01-31", :status_date => "2000-01-31"))
    order.should be_valid
  end

  it "should not accept invalid date" do
    order = Order.new(@attr.merge(:purchase_date => "2000-01-32", :status_date => "2000-01-32"))
    order.should_not be_valid
  end

  it "should allow order without image" do
    order = Order.new(@attr)
    order.should be_valid
  end

  it "should not allow order with invalid image url" do
    order = Order.new(@attr.merge(:image_remote_url => 'http://fake-site.com/pic.jpeg'))
    order.should_not be_valid
  end

  it "should respond to live_orders method" do
    Order.should respond_to(:live_orders)
  end

  it "should respond to archive_orders method" do
    Order.should respond_to(:archive_orders)
  end

  describe "interaction with user" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @order = @user.orders.create!(@attr)
    end

    it "should associate the order to the right user" do
      @user.orders.should include(@order)
      @order.user_id.should == @user.id
      @order.user.should == @user
    end

    it "should fetch the orders in the right order" do
      order2 = @user.orders.create!(@attr.merge(:purchase_date => Date.tomorrow))
      @user.orders.should == [@order, order2]
    end
  end

  describe "enforce defaults" do
    it "should create new order with the right days_notification value" do
      order = Order.new(@attr)
      order.days_notification.should == 0
    end

    it "should create new order with the right days_notification_sent value" do
      order = Order.new(@attr)
      order.days_notification_sent.should be_false
    end

    it "should create new order with the right days_notification_enable value" do
      order = Order.new(@attr)
      order.days_notification_enable.should be_false
    end
  end

  describe "send notification mails" do
    before(:each) do
      @new_attr = {:description => "example desc", :site => "Ebay", :purchase_date => Date.current - 5, :status => "Ordered",
               :status_date => Date.current, :notes => "Bla bla bla", :days_notification_enable => true, :days_notification => 5 }
      @user = FactoryGirl.create(:user)
      @order = @user.orders.create!(@new_attr)
    end

    describe "success" do
      it "should send email notification" do
        lambda do
          @order.send_one_days_notification
        end.should change(ActionMailer::Base.deliveries, :size).by(1)
      end

      it "should send to the right user" do
        @order.send_one_days_notification
        ActionMailer::Base.deliveries.last.to.should include(@order.user.email)
      end

      it "should mark the notification as sent" do
        @order.send_one_days_notification
        @order.reload
        @order.days_notification_sent.should be_true
      end

      it "should not send email if an email already sent (and days hasn't changed)" do
        @order.send_one_days_notification
        lambda do
          @order.send_one_days_notification
        end.should_not change(ActionMailer::Base.deliveries, :size)
      end
    end

    describe "failure" do
      it "should not send email if it don't need to" do
        not_to_send_order = @user.orders.create!(@new_attr.merge(:days_notification => 6))
        lambda do
          not_to_send_order.send_one_days_notification
        end.should_not change(ActionMailer::Base.deliveries, :size)
      end

      it "should not send email if it on state delivered" do
        not_to_send_order = @user.orders.create!(@new_attr.merge(:status => "Delivered"))
        lambda do
          not_to_send_order.send_one_days_notification
        end.should_not change(ActionMailer::Base.deliveries, :size)
      end

      it "should not send email if it on state picked" do
        not_to_send_order = @user.orders.create!(@new_attr.merge(:status => "Picked"))
        lambda do
          not_to_send_order.send_one_days_notification
        end.should_not change(ActionMailer::Base.deliveries, :size)
      end

      it "should not send email if it on state cancelled" do
        not_to_send_order = @user.orders.create!(@new_attr.merge(:status => "Cancelled"))
        lambda do
          not_to_send_order.send_one_days_notification
        end.should_not change(ActionMailer::Base.deliveries, :size)
      end
    end
  end

  describe "package_tracking" do
    before(:each) do
      @new_attr = {:description => "example desc", :site => "Ebay", :purchase_date => Date.current - 5, :status => "Ordered",
                   :status_date => Date.current, :notes => "Bla bla bla", :days_notification_enable => true, :days_notification => 5 }

      @user = FactoryGirl.create(:user)
      @order = @user.orders.create!(@new_attr)
      @package_tracking = PackageTracking.create(:tracking_number => "1234", :tracking_carrier => "YYYY", :order_id => @order.id)
    end

    it "should belong to the order" do
      @package_tracking.order.should == @order
    end

    it "should be in the order" do
      @order.package_tracking.should == @package_tracking
    end

    it "should get the right values" do
      @order.package_tracking.tracking_number.should == "1234"
      @order.package_tracking.tracking_carrier.should == "YYYY"
    end

  end

end
