# == Schema Information
#
# Table name: user_mails
#
#  id         :integer          not null, primary key
#  name       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe UserMail do
  describe "send user mail" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @user_mail = UserMail.create(:name => 'feedback')
    end

    describe "user already received mail" do
      before (:each) do
        @user.user_mails << @user_mail
      end
      it "should not send mail to user" do
        lambda do
          UserMail.send_mail_to_user(@user, @user_mail)
        end.should_not change(ActionMailer::Base.deliveries, :size)
      end
    end

    describe "user hasn't received mail" do
      it "should send mail to user" do
        lambda do
          UserMail.send_mail_to_user(@user, @user_mail)
        end.should change(ActionMailer::Base.deliveries, :size).by(1)
      end
    end
  end

  describe "send user mails" do
    before(:each) do
      @num_of_users = 10
      i = 0
      @num_of_users.times do
        FactoryGirl.create(:user, :email => "user#{i}@email.com")
        i += 1
      end
      @user_mail = UserMail.create(:name => 'feedback')
    end

    it "should not send more mails then it should" do
      lambda do
        UserMail.send_mail_to_users(@user_mail.name, @num_of_users)
      end.should change(ActionMailer::Base.deliveries, :size).by(@num_of_users)
    end

  end

end
