# == Schema Information
#
# Table name: package_trackings
#
#  id                    :integer          not null, primary key
#  order_id              :integer
#  tracking_number       :string(255)
#  tracking_carrier      :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  aftership_checkpoints :integer          default(0)
#

require 'spec_helper'

describe PackageTracking do
  describe "sanity" do

    before(:each) do
      @attr = {:tracking_number => "1234", :tracking_carrier => "YYYY"}
    end

    describe "success" do
      it "should create a new instance given a valid attr" do
        PackageTracking.create!(@attr)
      end

      it "should create valid record" do
        trk = PackageTracking.new(@attr)
        trk.should be_valid
      end
    end

    describe "failure" do

      it "should not create new record without tracking number" do
        trk = PackageTracking.new(@attr.merge(:tracking_number => ""))
        trk.should_not be_valid
      end

      it "should not create new record without tracking carrier" do
        trk = PackageTracking.new(@attr.merge(:tracking_carrier => ""))
        trk.should_not be_valid
      end
    end
  end
end
