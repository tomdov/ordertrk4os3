require 'spec_helper'
include OrdersHelper
include ActionView::Helpers::TextHelper #for pluralize
describe UsersController do
  render_views

  describe "Get 'index'" do

    describe "for non-signed-in users" do
      it 'should deny access' do
        get :index
        response.should redirect_to(signin_path)
      end
    end

    describe "for signed-in users" do

      before(:each) do
        @user = test_sign_in(FactoryGirl.create(:user))

        FactoryGirl.create(:user, :email => "user@example.com")
        FactoryGirl.create(:user, :email => "user@example.net")

      30.times do
        FactoryGirl.create(:user,:email =>  FactoryGirl.generate(:email))

        end
      end

      describe "for admin users" do
        before(:each) do
          @user.toggle!(:admin)
        end

        it 'should be successful' do
          get :index
          response.should be_success
        end

        it 'should have the right title' do
          get :index
          response.should have_selector('title', :content => "All users")
        end

        it 'should have an element for each user' do
          get :index
          User.paginate(:page => 1).each do |user|
            response.should have_selector('li', :content => @user.name)
          end
        end

        it 'should paginate users' do
          get :index
          response.should have_selector('ul.pagination')
          response.should have_selector('li.previous_page.unavailable')
          response.should have_selector('a', :href => "/users?page=2",
                                             :content => "2")
        end

        it 'should have a delete link for admins' do

          other_user = User.all.second
          get :index
          response.should have_selector('a', :href => user_path(other_user),
                                        :content => "Delete")
        end

      end

      describe "for non-admin users" do

        it 'should not be able to access index' do
          get :index
          response.should have_selector('div.page_not_found', :content => "The page you tried to reach isn't exist.")
        end
      end

    end

  end

  describe "Get 'show'" do

    describe "signed user" do
      before(:each) do
        @user = FactoryGirl.create(:user)
        test_sign_in(@user)
        @base_title = "Order Tracking"
      end

      it "should be successful" do
        get :show,  :id => @user
        response.should be_success
      end

      it "should find the right user" do
        get :show, :id => @user
        assigns(:user).should == @user    # assigns :user the @user from the users_controller
      end

      it "should have the right title" do
        get :show, :id => @user
        response.should have_selector('title', :content => @user.name)
      end

      it "should have the user's name" do
        get :show, :id => @user
        response.should have_selector('a', :content => @user.name)
      end

      it "should have a profile image" do
        get :show, :id => @user
        response.should have_selector('img', :class => "gravatar")
      end

      it "should paginate" do
        35.times { FactoryGirl.create(:order, :user => @user) }
        get :show, :id => @user
        response.should have_selector('ul.pagination')
      end

      describe "when singed in as another user" do
        it " should be successful" do
          test_sign_in(FactoryGirl.create(:user, :email => FactoryGirl.generate(:email)))
        end

        it "should not be able to see other user's orders" do
          other_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
          get :show, :id => other_user
          flash[:error].should =~ /access denied/i
        end
      end

      describe "User orders" do

        before(:each) do
          test_sign_in(@user)
          @attr = {:description           => "example desc",
                   :site                  => "Ebay",
                   :purchase_date         => Date.current,
                   :status                => "Ordered",
                   :status_date           => Date.current,
                   :notes                 => "Bla bla bla"}

          @archive_attr = {
                   :description           => "example archive desc",
                   :site                  => "Ebay",
                   :purchase_date         => Date.current,
                   :status                => "Cancelled",
                   :status_date           => Date.current,
                   :notes                 => "Bla bla bla"}

        end

        it "should contain link to new order" do
          get :show, :id => @user
          response.should have_selector('a.button', :content => 'Make a New Order',
                                                    :href => new_order_path)
        end

        it "should contain link to ebay add account" do
          get :show, :id => @user
          response.should have_selector('a.button', :content => 'Attach eBay account',
                                        :href => ebay_settings_user_path)
        end

        it "should contain link to bookmarklet" do
          get :show, :id => @user
          response.should have_selector('a.button', :content => 'Meet our Bookmarklet',
                                        :href => bookmarklet_explain_path)
        end

        it "should contain link to new order if the user has orders" do
          @user.orders.create!(@attr)
          get :show, :id => @user
          response.should have_selector('a.button', :content => 'New Order',
                                                    :href => new_order_path)
        end

        it "should contain the order" do
          @user.orders.create!(@attr)
          get :show, :id => @user
          response.should have_selector('span', :content => @attr[:description])
        end
        #
        #it "should contain image column" do
        #  @user.orders.create!(@attr)
        #  get :show, :id => @user
        #  response.should have_selector('td', :content => "Image")
        #end

        it "should contain a default order image" do
          @user.orders.create!(@attr)
          get :show, :id => @user
          response.should have_selector('img.order_image', :src => '/assets/fallback/order_image_default.jpg')
        end

        it "should contain link on the image" do
          @user.orders.create!(@attr.merge(:site => "http://ebay.com"))
          get :show, :id => @user
          response.should have_selector('span.order_image_span>a', :href => "http://ebay.com")
        end

        it "should not contain cancelled order in the feed" do
          @user.orders.create!(@archive_attr)
          get :show, :id => @user
          response.should_not have_selector('span', :content => @archive_attr[:description])
        end

        it "should not contain picked order in the feed" do
          @user.orders.create!(@archive_attr.merge(:status => "Picked"))
          get :show, :id => @user
          response.should_not have_selector('span', :content => @archive_attr[:description])
        end

        it "should show delete link" do
          order1 = @user.orders.create!(@attr)
          get :show, :id => @user
          response.should have_selector('a', :href => order_path(order1), :title => "Delete")
        end

        it "should show edit link" do
          order1 = @user.orders.create!(@attr)
          get :show, :id => @user
          response.should have_selector('a', :href => edit_order_path(order1), :title => "Edit")
        end

        it "should show 'picked' link" do
          order1 = @user.orders.create!(@attr)
          get :show, :id => @user
          response.should have_selector('a', :href => complete_order_path(order1), :title => "Mark as Picked", :class => 'checkmark foundicon-checkmark brown')
        end

        describe "days display" do

          describe "purchase date" do

            it "should display the correct days ago" do
              @user.orders.create!(@attr.merge(:purchase_date => 1.days.ago))
              get :show, :id => @user
              response.should have_selector('span.purchase_date', :content => "1 day ago")
            end

            it "should display the correct days ahead" do
              @user.orders.create!(@attr.merge(:purchase_date => Date.tomorrow))
              get :show, :id => @user
              response.should have_selector('span.purchase_date', :content => "1 day ahead")
            end

          end

          describe "status date" do

            it "should display the correct days ago" do
              @user.orders.create!(@attr.merge(:status_date => 1.days.ago))
              get :show, :id => @user
              response.should have_selector('span.status_date', :content => "1 day ago")
            end

            it "should display the correct days ahead" do

              @user.orders.create!(@attr.merge(:status_date => Date.tomorrow))
              get :show, :id => @user
              response.should have_selector('span.status_date', :content => "1 day ahead")
            end

          end
        end

        describe "link/site display" do

          before(:each) do
            @max_site_without_truncate = 30
          end

          it "should display link with http or with link" do
            @user.orders.create!(@attr.merge(:site => "http://www.site4example.com"))
            get :show, :id => @user
            response.should have_selector('span.site>a', :href => "http://www.site4example.com",
                                                         :content => "http://www.site4example.com")
          end

          it "should display link with https or with link" do
            @user.orders.create!(@attr.merge(:site => "https://www.site4example.com"))
            get :show, :id => @user
            response.should have_selector('span.site>a', :href => "https://www.site4example.com",
                                          :content => "https://www.site4example.com")
          end

          it "should not display regular site with link" do
            @user.orders.create!(@attr.merge(:site => "Ebay"))
            get :show, :id => @user
            response.should_not have_selector('span.site>a', :href => "Ebay",
                                          :content => "Ebay")
          end

          it "should truncate the link if it's too long" do
            @user.orders.create!(@attr.merge(:site => "http://#{"a" * (@max_site_without_truncate + 1 - "http://".length)}"))
            get :show,  :id => @user
            response.should have_selector('span.site>a', :content => "http://#{"a" * (@max_site_without_truncate - 3 - "http://".length)}...")
          end

          it "should not truncate the link if it's below limit" do
            @user.orders.create!(@attr.merge(:site => "http://#{"a" * (@max_site_without_truncate - "http://".length)}"))
            get :show,  :id => @user
            response.should_not have_selector('span.site>a', :content => "http://#{"a" * (@max_site_without_truncate - 1 - "http://".length)}...")
            response.should     have_selector('span.site>a', :content => "http://#{"a" * (@max_site_without_truncate - "http://".length)}")
          end

          it "should not truncate regular site" do
            @user.orders.create!(@attr.merge(:site => "a" * (@max_site_without_truncate + 1)))
            get :show,  :id => @user
            response.should     have_selector('span.site', :content => "a" * (@max_site_without_truncate + 1))
          end
        end

      end
    end

    describe "when not signed in" do
      before(:each) do
        @user = FactoryGirl.create(:user)
      end

      it "should be redirect to root if try to access wall" do
        get :show,  :id => @user
        response.should redirect_to(signin_path)
      end
    end

  end
  describe "GET 'new'" do
    it "returns http success" do
      get :new
      response.should be_success
    end

    it "should have the right title" do
      get :new
      response.should have_selector("title",
                                    :content => "Sign Up")
    end

    it "should have link to the terms" do
      get :new
      response.should have_selector('a',
                                      :href => "/terms",
                                      :content => 'Terms of use')
    end
  end

  describe "POST 'new'" do

    describe "failure" do

      before(:each) do
        @attr = { :name => "", :email => "", :password => "", :password_confirmation => "" }
      end

      it "should have the right title" do
        post :create, :user => @attr
        response.should have_selector('title', :content => "Sign Up")
      end

      it "should render the 'new' page" do
        post :create, :user => @attr
        response.should render_template('new')
      end

      it "should not create a user" do
        lambda do
          post :create, :user => @attr
        end.should_not change(User, :count)

      end
    end

    describe 'success' do
      before(:each) do
        @attr = { :name => 'new user', :email => "user@example.com",
                  :password => "1234", :password_confirmation => "1234" }
      end

      it "should create a user" do
        lambda do
          post :create, :user => @attr
        end.should change(User, :count).by(1)
      end

      it 'should redirect to the user show page' do
        post :create, :user => @attr
        response.should redirect_to(user_path(assigns(:user)))
      end

      it 'should have a welcome message' do
        post :create, :user => @attr
        flash[:success].should =~ /Welcome to the Order Tracking app/i
      end

      it 'should sign the user in' do
        post :create, :user => @attr
        controller.should be_signed_in
      end

      describe "send welcome email" do

        before(:each) do
          @welcome_mail = ActionMailer::Base.deliveries.last
        end
        it "should send a mail" do
          post :create, :user => @attr
          ActionMailer::Base.deliveries.should_not be_empty
        end

        it "should send exctly on email per user" do
          lambda do
            post :create, :user => @attr
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send the right subject" do
          post :create, :user => @attr
          @welcome_mail.subject.should == 'Welcome to Order Tracking Site'
        end

        it "should send the right 'from field'" do
          post :create, :user => @attr
          site_url = UserMailer.site_url
          @welcome_mail.from.should include("do-not-replay@#{site_url}")
        end

        it "should send the email to the right user" do
          post :create, :user => @attr
          @welcome_mail.to.size.should == 1
          @welcome_mail.to.should include(@attr[:email])
        end

        it "should contain welcome message" do
          post :create, :user => @attr
          @welcome_mail.body.encoded.should =~ /You have successfully signed up to #{UserMailer.site_url}, Congratulations!/
        end


      end
    end
  end

  describe 'GET "Edit"' do

    before(:each) do
      @user = FactoryGirl.create(:user)
      test_sign_in(@user)
    end

    it 'should be successful' do
      get :edit, :id => @user
      response.should be_success
    end

    it 'should have the right title' do
      get :edit, :id => @user
      response.should have_selector('title', :content => "Edit user")
    end

    it 'should have a link to change the Gravatar' do
      get :general_settings, :id => @user
      response.should have_selector('a', :href => 'http://gravatar.com/emails',
                                          :content => 'change')
    end

    it 'should not allow access to other user settings' do
      @other_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
      get :edit, :id => @other_user
      response.should redirect_to(root_path)
      flash[:error].should =~ /Access denied/i
    end

    describe 'ebay settings' do
      it 'should not allow access to other user ebay settings' do
        @other_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
        get :ebay_settings, :id => @other_user
        response.should redirect_to(root_path)
        flash[:error].should =~ /Access denied/i
      end

    end

    describe 'general settings' do
      it 'should not allow access to other user general settings' do
        @other_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
        get :general_settings, :id => @other_user
        response.should redirect_to(root_path)
        flash[:error].should =~ /Access denied/i
      end

      it 'should not allow unsigned user access' do
        sign_out
        unsigned_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
        get :general_settings, :id => unsigned_user
        response.should redirect_to(signin_path)
      end

    end

    describe 'subscribe_settings' do

      it "should display subscribe_settings in the edit menu" do
        get :edit, :id => @user
        response.should have_selector('a', :href => subscribe_settings_user_path, :content => "Subscribe Settings")
      end

      describe "get" do

        it "should be success" do
          get :subscribe_settings, :id => @user
          response.should be_success
        end

        it "should not allow other user to see the settings" do
          @other_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
          get :subscribe_settings, :id => @other_user
          response.should redirect_to root_path
        end
      end

    end

  end

  describe 'PUT "Update"' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      test_sign_in(@user)
      @new_password = "12345"
    end

    describe 'success' do

      before(:each) do
        @attr = {:name => 'new-user',
                 :email => 'new@example.com',
                 :old_password => 'foobar',
                 :password => '12345',
                 :password_confirmation => '12345'}
      end

      it 'should display success message' do
        put :update, :user => @attr, :id => @user
        flash[:success].should =~ /User saved successfuly/i
      end

      it 'should render the "show" template' do
        put :update, :user => @attr, :id => @user
        @user.reload
        response.should redirect_to(@user)
      end

      it 'should update the user' do
        put :update, :user => @attr, :id => @user
        user = assigns(:user)
        @user.reload
        user.name.should == @user.name
        user.email.should == @user.email
        @user.has_password?(user.password)
      end

      describe 'enforce old password' do
        before(:each) do
          @partial_attr = {:name => 'new-user',
                           :email => 'new@example.com',
                           :old_password => 'foobar'}
        end

        it 'should check for match on the old password' do
          @user.has_password?(@attr[:old_password]).should be_true
          put :update, :user => @attr, :id => @user
          flash[:success].should =~ /User saved successfuly/i
        end

        it 'should allow change the deatils' do
          put :update, :user => @partial_attr, :id => @user
          flash[:success].should =~ /User saved successfuly/i
        end
      end


      describe "subscribe_settings" do
        before(:each) do
          @attr = {
              :old_password => 'foobar',
              :messages_notifications => "0",
              :orders_notifications => "1"
          }
        end

        it "should fail for wrong password" do
          put :update, :user => @attr.merge(:old_password => 'wrong', :update_user_context => 'subscribe'), :id => @user
          flash[:error].should == "Wrong old password"
          response.should render_template('subscribe_settings')
        end
        it "should show flash in case of success" do
          put :update, :user => @attr, :id => @user
          flash[:success].should =~ /User saved successfuly/i
        end

        it "should save the user's choises" do
          @user.messages_notifications.should == true
          @user.orders_notifications.should == true
          put :update, :user => @attr, :id => @user
          user = @user.reload
          user.messages_notifications.should == false
          user.orders_notifications.should == true
        end

      end

    end

    describe 'failure' do

      before(:each) do
        @attr = {:name => '',
                 :email => '',
                 :old_password => 'foobar',
                 :password => '',
                 :password_confirmation => ''}
      end
      it 'should display error message' do
        put :update, :user => @attr, :id => @user
        flash[:error].should =~ /Error in user saving/i
      end

      it 'should match the old password' do
        put :update, :user => @attr.merge(:old_password => 'wrongPass'), :id => @user
        flash[:error].should =~ /Wrong old password/i
        response.should render_template('general_settings')
      end

      it 'should not change details if old password isn\'t matched' do
        user = @user
        put :update, :user => @attr.merge(:old_password => 'wrongPass'), :id => @user
        @user.reload
        user.name.should == @user.name
        user.email.should == @user.email
        @user.has_password?(user.password)

      end

      it 'should render the \'edit\' page' do
        put :update, :user => @attr, :id => @user
        response.should render_template('edit')
      end

      it 'should have the right title' do
        put :update, :user => @attr, :id => @user
        response.should have_selector('title', content: "Edit user")
      end
    end
  end

  describe 'authentication of edit/update actions' do

    before(:each) do
      @user = FactoryGirl.create(:user)
    end

    describe 'for non-sign-in users' do

      it 'should not allow access to \'edit\'' do
        get :edit, :id => @user
        response.should redirect_to(signin_path)
        flash[:notice].should =~ /sign in/i
      end

      it 'should deny access to \'update\'' do
        put :update, :id => @user, :user => {}
        response.should redirect_to(signin_path)
      end

    end

    describe 'for signed-in users' do

      before(:each) do
        wrong_user = FactoryGirl.create(:user, :email => "wrongUser@example.com")
        test_sign_in(wrong_user)
      end

      it 'should require matching users for \'edit\'' do
        get :edit, :id => @user
        response.should redirect_to(root_path)
      end

      it 'should require matching users for \'update\'' do
        get :update, :id => @user, :user => {}
        response.should redirect_to(root_path)
      end
    end

  end

  describe 'DELETE "destroy"' do

    before(:each) do
      @user = FactoryGirl.create(:user)
    end

    describe 'as a non-signed-in user' do
      it 'should deny access' do
        delete :destroy, :id => @user
        response.should redirect_to(signin_path)
      end
    end

    describe 'as a non-admin user' do
      it 'should protect the action' do
        test_sign_in(@user)
        delete :destroy, :id => @user
        response.should redirect_to(root_path)
      end
    end

    describe 'as an admin user' do

      before(:each) do
        @admin = FactoryGirl.create(:user, :email => "admin@example.com", :admin => true)
        test_sign_in(@admin)
      end

      it 'should destroy the user' do
        lambda do
          delete :destroy, :id => @user
        end.should change(User, :count).by(-1)

      end

      it 'should redirect to the users page' do
        delete :destroy, :id => @user
        flash[:success].should =~ /User successfully deleted/i
        response.should redirect_to(users_path)
      end

      it 'should not allow an admin to destroy itself' do
        lambda do
          delete :destroy, :id => @admin
        end.should_not change(User, :count)
        #flash[:error].should =~ /An admin can't delete itself/i
      end

    end
  end

  describe "get 'archive'" do

    before(:each) do
      @user = FactoryGirl.create(:user)
      @other_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
      test_sign_in(@user)
      @attr = {:description           => "example desc",
               :site                  => "Ebay",
               :purchase_date         => Date.current,
               :status                => "Ordered",
               :status_date           => Date.current,
               :notes                 => "Bla bla bla"}

      @archive_attr = {
          :description           => "example archive desc",
          :site                  => "Ebay",
          :purchase_date         => Date.current,
          :status                => "Cancelled",
          :status_date           => Date.current,
          :notes                 => "Bla bla bla"}

    end

    it "should be successful" do
      get :archive, :id => @user
      response.should be_success
    end

    it "should display a message that there are no orders" do
      get :archive, :id => @user
      response.should have_selector('td', :content => "No orders in archive..")
    end

    it "should contain the archived order" do
      @user.orders.create!(@archive_attr)
      get :archive, :id => @user
      response.should have_selector('span', :content => @archive_attr[:description])
    end

    it "should not contain regular order in the feed" do
      @user.orders.create!(@attr)
      get :archive, :id => @user
      response.should_not have_selector('span', :content => @attr[:description])
    end

    it "should forbid access to other user archive" do
      get :archive, :id => @other_user
      flash.now[:error].should =~ /access denied/i
    end

    it "should not display days ago or ahead" do
      @user.orders.create!(@archive_attr)
      get :archive, :id => @user
      response.should_not have_selector('span', :content => "(#{pluralize(days_diff_abs(@attr[:status_date], Date.current), "day")} #{days_diff_ago_or_ahead(@attr[:status_date], Date.current)})")
    end
  end
end
