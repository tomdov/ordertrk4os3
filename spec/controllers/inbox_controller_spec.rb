require 'spec_helper'

require 'pathname'
require 'json'

describe InboxController do
  render_views



  describe "handle_inbound" do

    describe "New Order" do

      before(:each) do
        @user = FactoryGirl.create(:user)
      end
      describe "paypal" do
        before(:each) do
          inbound_payload =
              {
              'inbound' => {
                  :event_type => 'inbound',
                  :subject => 'paypal',
                  :message_id => '<CAGBx7GhULS7d6ZsdLREHnKQ68V6w2fbGmD85dPn63s6RtpsZeQ@mail.gmail.com>',
                  :message_version => nil,
                  :in_reply_to => nil,
                  :references => [],
                  :headers => {
                      "Content-Type" => "multipart/alternative; boundary=e0cb4efe2faee9b97304c6d0647b",
                      "Date" => "Thu, 9 Aug 2012 15:44:15 +0800",
                      "Dkim-Signature" => "v=1; a=rsa-sha256; c=relaxed/relaxed; d=gmail.com; s=20120113; h=mime-version:date:message-id:subject:from:to:content-type; bh=9/e4o7vsyI5eIM2MUze13ZWWdxyhC7cxjHwrHXPSEJQ=; b=HOb83u8i6ai3HuT61C+NfQcUHqATH+/ivAjZ2pD/MXcCFboOyN9LGeMHm+RhwnL+Ap mC0R9+eqlWaoxqd6ugrvtNOQ1Kvb9LunPnnMwY06KZKpoXCVwFrzT3e2f8JgLwyAxpUv G0srziHwpuCh/y42dJ83tKhctHc6w6GKC79H1WBAcexnjvk0LgrkOnNJ/iBCOznjs35o V4jfjlJBeZLvxcnEJ5Xxade2kWbLZ9TWiuVfTS6xUyVb/gfn5x9D1KjCUb1Gwq9wYJ4m UxH6oC5f3mkM+NZ6oDBmJFDdVxg23rRaMrF4YBpVGj+6+pjF36N8CrmtaDOJNVqCS5FN koSw==",
                      "From" => "From Name <from@example.com>",
                      "Message-Id" => "<CAGBx7GhULS7d6ZsdLREHnKQ68V6w2fbGmD85dPn63s6RtpsZeQ@mail.gmail.com>",
                      "Mime-Version" => "1.0",
                      "Received" => [ "from mail-lpp01m010-f51.google.com (mail-lpp01m010-f51.google.com [209.85.215.51]) by app01.transact (Postfix) with ESMTPS id F01841E0010A for <to@example.com>; Thu, 9 Aug 2012 03:44:16 -0400 (EDT)", "by lahe6 with SMTP id e6so79326lah.24 for <to@example.com>; Thu, 09 Aug 2012 00:44:15 -0700 (PDT)", "by 10.112.43.67 with SMTP id u3mr382471lbl.16.1344498255378; Thu, 09 Aug 2012 00:44:15 -0700 (PDT)", "by 10.114.69.44 with HTTP; Thu, 9 Aug 2012 00:44:15 -0700 (PDT)" ],
                      "Subject" => "[inbound] Sample Subject",
                      "To" => "to@example.com"
                  },
                  :sender_email => 'from@example.com',
                  :user_email => 'from@example.com',
                  :recipients => [["to@example.com", nil]],
                  :recipient_emails => ["to@example.com"],
                  :message_body => '"<table align="center" border="0" cellpadding="0" cellspacing="0" style="clear:both;color: #333 !important;font-size: 12px;font-family: arial,helvetica,sans-serif;" width="598px">
                                      <tr>
                                        <td style="border:1px solid #ccc;border-right:none;border-left:none;padding:5px 10px 5px 10px !important;color: #333333;" width="348" align="left">Description</td>
                                        <td style="border:1px solid #ccc;border-right:none;border-left:none;padding:5px 10px 5px 10px !important;color: #333333;" width="100" align="right">Unit price</td>
                                        <td style="border:1px solid #ccc;border-right:none;border-left:none;padding:5px 10px 5px 10px !important;color: #333333;" width="50" align="right">Qty</td>
                                        <td style="border:1px solid #ccc;border-right:none;border-left:none;padding:5px 10px 5px 10px !important;color: #333333;" width="80" align="right">Amount</td>
                                      </tr>
                                      <tr>
	                                      <td valign="top" align="left" style="border-bottom:none;padding: 10px;"><a target="new" href="http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&amp;item=261180983693&amp;var=560189745574">Luxury Flip Leather Book Case Phone Cover for Samsung Galaxy S 2 II i9100 #FG  [Purple,ONLY for GT-i9100]</a><br/>Item# 261180983693</td>
	                                      <td valign="top" align="right" style="border-bottom:none;padding: 10px;">$5.99 USD</td>
	                                      <td valign="top" align="right" style="border-bottom:none;padding: 10px;">1</td><td valign="top" align="right" style="border-bottom:none;padding: 10px;">$5.99 USD</td>
	                                    </tr>
                                    </table>"',
                  :click => nil,
                  :all_clicks => [],
                  :all_clicked_links => []
              }
          }

          inbound_mail = webhook_example_event('inbound_paypal')
          @event_payload = Mandrill::WebHook::EventDecorator[inbound_mail]
        end

        describe "incoming mail" do

          it "should include the correct message body" do
            @event_payload.message_body(:html).should include('<td valign="top" align="left" style="border-bottom:none;padding: 10px;"><a target="new" href="http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&amp;item=261180983693&amp;var=560189745574">Luxury Flip Leather Book Case Phone Cover for Samsung Galaxy S 2 II i9100 #FG  [Purple,ONLY for GT-i9100]</a><br/>Item# 261180983693</td>')
            @event_payload.message_body(:html).should include('<td valign="top" align="right" style="border-bottom:none;padding: 10px;">$5.99 USD</td>')
            @event_payload.message_body(:html).should include('<td valign="top" align="right" style="border-bottom:none;padding: 10px;">1</td><td valign="top" align="right" style="border-bottom:none;padding: 10px;">$5.99 USD</td>')
          end

          it "should include the right subject" do
            @event_payload.subject.should include("paypal")
          end

          it "should have a valid user" do
            User.find_by_email(@event_payload.sender_email).should_not be_nil
          end

          it "should be sent to the right email" do
            @event_payload.recipient_emails.any?{ |s| s.casecmp("neworder@#{OrderMailer.site_url}") == 0 }.should be_true
          end

        end

        describe "mail to the user" do
          describe "success" do
            before(:each) do
              @ic = InboxController.new
            end

            describe "multi mail" do
              before(:each) do
                inbound_paypal_multi_products = webhook_example_event('inbound_paypal_multi_products')
                @event_payload_multi_products = Mandrill::WebHook::EventDecorator[inbound_paypal_multi_products]
              end

              it "should send 4 mails" do
                lambda do
                  @ic.handle_inbound(@event_payload_multi_products)
                end.should change(ActionMailer::Base.deliveries, :size).by(4)
              end

              it "should add order" do
                lambda do
                  @ic.handle_inbound(@event_payload_multi_products)
                end.should change(Order, :count).by(4)

              end
            end

            it "should send a mail" do
              @ic.handle_inbound(@event_payload)
              ActionMailer::Base.deliveries.should_not be_empty
            end

            it "should add order" do
              ic = InboxController.new
              lambda do
                ic.handle_inbound(@event_payload)
              end.should change(Order, :count).by(1)

            end

            it "should send exctly on email per user" do
              lambda do
                @ic.handle_inbound(@event_payload)
              end.should change(ActionMailer::Base.deliveries, :size).by(1)
            end

            it "should send the right subject" do
              @ic.handle_inbound(@event_payload)
              ActionMailer::Base.deliveries.last.subject.should == 'Order Tracking - New order received'
            end

            it "should send the right 'from field'" do
              @ic.handle_inbound(@event_payload)
              @mail = ActionMailer::Base.deliveries.last
              @mail.from.should include("do-not-replay@#{OrderMailer.site_url}")
            end

            it "should send the email to the right user" do
              @ic.handle_inbound(@event_payload)
              @mail = ActionMailer::Base.deliveries.last
              @mail.to.size.should == 1
              @mail.to.should include(@user.email)
            end

            it "should add new order to the right user" do
              @ic.handle_inbound(@event_payload)
              @user.reload
              Order.find_by_description("Luxury Flip Leather Book Case Phone Cover for Samsung Galaxy S 2 II i9100 #FG  [Purple,ONLY for GT-i9100]").user.should == @user
            end

            describe "mail content" do
              before(:each) do
                @ic.handle_inbound(@event_payload)
                @mail = ActionMailer::Base.deliveries.last
              end

              it "should send the right subject" do
                @mail.subject.should == 'Order Tracking - New order received'
              end

              it "should send the right 'from field'" do
                site_url = OrderMailer.site_url
                @mail.from.should include("do-not-replay@#{site_url}")
              end

              it "should send the email to the right user" do
                @mail.to.size.should == 1
                @mail.to.should include(@user.email)
              end

              it "should contain relevant message" do
                @mail.body.encoded.should include("Price: $5.99 USD")
                @mail.body.encoded.should =~ /Description: Luxury Flip Leather Book Case Phone Cover for Samsung Galaxy S 2 II i9100 #FG  [Purple,ONLY for GT-i9100]/
                @mail.body.encoded.should include(CGI::unescapeHTML("http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&amp;item=261180983693&amp;var=560189745574"))
              end

            end

            describe "mail without link in description" do
              before(:each) do
                inbound_paypal_desc_wo_link = webhook_example_event('inbound_paypal_no_link_for_description')
                @event_payload_desc_wo_link = Mandrill::WebHook::EventDecorator[inbound_paypal_desc_wo_link]
              end

              it "should send mail" do
                lambda do
                  @ic.handle_inbound(@event_payload_desc_wo_link)
                end.should change(ActionMailer::Base.deliveries, :size).by(1)
              end

              it "should add order" do
                lambda do
                  @ic.handle_inbound(@event_payload_desc_wo_link)
                end.should change(Order, :count).by(1)

              end

              describe "mail content" do
                before(:each) do
                  @ic.handle_inbound(@event_payload_desc_wo_link)
                  @mail = ActionMailer::Base.deliveries.last
                end

                it "should send the right subject" do
                  @mail.subject.should == 'Order Tracking - New order received'
                end

                it "should contain relevant message" do
                  @mail.body.encoded.should include("Price: $5.99 USD")
                  @mail.body.encoded.should =~ /Description: Luxury Flip Leather Book Case Phone Cover for Samsung Galaxy S 2 II i9100 #FG  [Purple,ONLY for GT-i9100]/
                  @mail.body.encoded.should_not include(CGI::unescapeHTML("http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&amp;item=261180983693&amp;var=560189745574"))
                end
              end
            end

            describe "no order mail" do
              before(:each) do
                inbound_paypal_no_order = webhook_example_event('inbound_paypal_without_orders')
                @event_payload_no_orders = Mandrill::WebHook::EventDecorator[inbound_paypal_no_order]
              end

              it "should send mail" do
                lambda do
                  @ic.handle_inbound(@event_payload_no_orders)
                end.should change(ActionMailer::Base.deliveries, :size).by(1)
              end

              it "should contain non complient message" do
                @ic.handle_inbound(@event_payload_no_orders)
                ActionMailer::Base.deliveries.last.body.encoded.should =~ /No orders found in the mail you've sent/i
              end
            end
          end

          describe "failure" do
            before(:each) do
              @ic = InboxController.new
            end

            describe "invalid user" do
              before(:each) do
                inbound_mail_invalid_user = webhook_example_event('inbound_paypal_invalid_user')
                @event_payload_invalid_user = Mandrill::WebHook::EventDecorator[inbound_mail_invalid_user]
              end

              it "should not have a valid user" do
                User.find_by_email(@event_payload_invalid_user.sender_email).should be_nil
              end

              it "shouldn't send a mail for user that doesn't exist" do
                lambda do
                  @ic.handle_inbound(@event_payload_invalid_user)
                end.should_not change(ActionMailer::Base.deliveries, :count)
              end

              it "should not add order" do
                lambda do
                  @ic.handle_inbound(@event_payload_invalid_user)
                end.should_not change(Order, :count)
              end
            end

            describe "wrong subject" do
              before(:each) do
                inbound_mail_wrong_subject = webhook_example_event('inbound_paypal_wrong_subject')
                @event_payload_wrong_subject = Mandrill::WebHook::EventDecorator[inbound_mail_wrong_subject]
                @ic.handle_inbound(@event_payload_wrong_subject)
                @mail = ActionMailer::Base.deliveries.last
              end

              it "should send the right subject" do
                @mail.subject.should == 'Order Tracking - New order receive failed'
              end

              it "should send the right 'from field'" do
                site_url = OrderMailer.site_url
                @mail.from.should include("do-not-replay@#{site_url}")
              end

              it "should send the email to the right user" do
                @mail.to.size.should == 1
                @mail.to.should include(@user.email)
              end

              it "should contain relevant message" do
                @mail.body.encoded.should_not =~ /Description:/
                @mail.body.encoded.should =~ /The subject of your mail you've sent isn't recognized by the site/i
              end

            end

            describe "missing description" do
              before(:each) do
                inbound_mail_missing_description = webhook_example_event('inbound_paypal_missing_description')
                @event_payload_missing_description = Mandrill::WebHook::EventDecorator[inbound_mail_missing_description]
              end

              it "should not add order" do
                lambda do
                  @ic.handle_inbound(@event_payload_missing_description)
                end.should_not change(Order, :count)
              end

              it "should send an error mail for user that not all the details exist" do
                lambda do
                  @ic.handle_inbound(@event_payload_missing_description)
                end.should change(ActionMailer::Base.deliveries, :count).by(1)
              end

              describe "mail content" do

                before(:each) do
                  @ic.handle_inbound(@event_payload_missing_description)
                  @mail = ActionMailer::Base.deliveries.last
                end

                it "should send the right subject" do
                  @mail.subject.should == 'Order Tracking - New order receive failed'
                end

                it "should send the right 'from field'" do
                  site_url = OrderMailer.site_url
                  @mail.from.should include("do-not-replay@#{site_url}")
                end

                it "should send the email to the right user" do
                  @mail.to.size.should == 1
                  @mail.to.should include(@user.email)
                end

                it "should contain relevant message" do
                  @mail.body.encoded.should include("Price: $5.99 USD")
                  @mail.body.encoded.should_not =~ /Description: Luxury Flip Leather Book Case Phone Cover for Samsung Galaxy S 2 II i9100 #FG  [Purple,ONLY for GT-i9100]/
                  @mail.body.encoded.should include("http://cgi.ebay.com/ws/eBayISAPI.dll")
                  @mail.body.encoded.should =~ /Some details in the order were missing. Therefore the order hasn't created in your account/i
                end

              end

            end

            describe "mail without order" do
              before(:each) do
                inbound_paypal_without_orders = webhook_example_event('inbound_paypal_without_orders')
                @event_payload_without_orders = Mandrill::WebHook::EventDecorator[inbound_paypal_without_orders]
              end

              it "should send 1 mails" do
                lambda do
                  @ic.handle_inbound(@event_payload_without_orders)
                end.should change(ActionMailer::Base.deliveries, :size).by(1)
              end


              describe "mail content" do

                before(:each) do
                  @ic.handle_inbound(@event_payload_without_orders)
                  @mail = ActionMailer::Base.deliveries.last
                end

                it "should send the right subject" do
                  @mail.subject.should == 'Order Tracking - New order receive failed'
                end

                it "should send the right 'from field'" do
                  site_url = OrderMailer.site_url
                  @mail.from.should include("do-not-replay@#{site_url}")
                end

                it "should send the email to the right user" do
                  @mail.to.size.should == 1
                  @mail.to.should include(@user.email)
                end

                it "should contain relevant message" do
                  @mail.body.encoded.should include("No orders found in the mail you've sent")
                end

              end
            end

            describe "wrong 'to' address" do
              before(:each) do
                inbound_mail_wrong_to_address = webhook_example_event('inbound_paypal_wrong_to_address')
                @event_payload_wrong_to_address = Mandrill::WebHook::EventDecorator[inbound_mail_wrong_to_address]
              end

              it "should not be sent to the right email" do
                @event_payload_wrong_to_address.recipient_emails.should_not contain("neworder@#{OrderMailer.site_url}")
              end

              it "shouldn't send a mail for user that doesn't exist" do
                lambda do
                  @ic.handle_inbound(@event_payload_wrong_to_address)
                end.should_not change(ActionMailer::Base.deliveries, :count)
              end

              it "should not add order" do
                lambda do
                  @ic.handle_inbound(@event_payload_wrong_to_address)
                end.should_not change(Order, :count)
              end

            end

          end
        end
      end

      describe "amazon" do
        #
        #describe "2012 receipt" do
        #  before(:each) do
        #    inbound_mail = webhook_example_event('inbound_amazon')
        #    @event_payload = Mandrill::WebHook::EventDecorator[inbound_mail]
        #  end
        #
        #  describe "incoming mail" do
        #
        #    it "should include the correct message body" do
        #      @event_payload.message_body(:html).should include('4 GB DDR3 Laptop Memory CMSO4GX3M1A1333C9')
        #    end
        #
        #    it "should include the right subject" do
        #      @event_payload.subject.should include("amazon")
        #    end
        #
        #    it "should have a valid user" do
        #      User.find_by_email(@event_payload.sender_email).should_not be_nil
        #    end
        #
        #    it "should be sent to the right email" do
        #      @event_payload.recipient_emails.any?{ |s| s.casecmp("neworder@#{OrderMailer.site_url}") == 0 }.should be_true
        #    end
        #
        #  end
        #
        #  describe "mail to the user" do
        #    describe "success" do
        #      before(:each) do
        #        @ic = InboxController.new
        #      end
        #
        #      describe "multi mail" do
        #        before(:each) do
        #          inbound_multi_products = webhook_example_event('inbound_amazon_multi_products')
        #          @event_payload_multi_products = Mandrill::WebHook::EventDecorator[inbound_multi_products]
        #        end
        #
        #        it "should send 3 mails" do
        #          lambda do
        #            @ic.handle_inbound(@event_payload_multi_products)
        #          end.should change(ActionMailer::Base.deliveries, :size).by(3)
        #        end
        #
        #        it "should add order" do
        #          lambda do
        #            @ic.handle_inbound(@event_payload_multi_products)
        #          end.should change(Order, :count).by(3)
        #
        #        end
        #      end
        #
        #      it "should send a mail" do
        #        @ic.handle_inbound(@event_payload)
        #        ActionMailer::Base.deliveries.should_not be_empty
        #      end
        #
        #      it "should add order" do
        #        ic = InboxController.new
        #        lambda do
        #          ic.handle_inbound(@event_payload)
        #        end.should change(Order, :count).by(1)
        #
        #      end
        #
        #      it "should send exctly on email per user" do
        #        lambda do
        #          @ic.handle_inbound(@event_payload)
        #        end.should change(ActionMailer::Base.deliveries, :size).by(1)
        #      end
        #
        #      it "should add new order to the right user" do
        #        @ic.handle_inbound(@event_payload)
        #        @user.reload
        #        Order.find_by_description("\"Corsair 4 GB DDR3 Laptop Memory CMSO4GX3M1A1333C9\"").user.should == @user
        #      end
        #
        #      describe "mail content" do
        #        before(:each) do
        #          @ic.handle_inbound(@event_payload)
        #          @mail = ActionMailer::Base.deliveries.last
        #        end
        #
        #        it "should send the right subject" do
        #          @mail.subject.should == 'Order Tracking - New order received'
        #        end
        #
        #        it "should send the right 'from field'" do
        #          site_url = OrderMailer.site_url
        #          @mail.from.should include("do-not-replay@#{site_url}")
        #        end
        #
        #        it "should send the email to the right user" do
        #          @ic.handle_inbound(@event_payload)
        #          @mail = ActionMailer::Base.deliveries.last
        #          @mail.to.size.should == 1
        #          @mail.to.should include(@user.email)
        #        end
        #
        #        it "should contain relevant message" do
        #          @mail.body.encoded.should include("Price: $21.99")
        #          @mail.body.encoded.should =~ /Description: 1 \"Corsair 4 GB DDR3 Laptop Memory CMSO4GX3M1A1333C9\"/
        #          @mail.body.encoded.should include(CGI::unescapeHTML("https://www.amazon.com/gp/css/history/view.html/ref=ox_oce_order_history"))
        #        end
        #      end
        #    end
        #  end
        #end

        describe "march 2014 receipt" do
          before(:each) do
            inbound_mail = webhook_example_event('inbound_amazon_032014')
            @event_payload = Mandrill::WebHook::EventDecorator[inbound_mail]
          end

          describe "incoming mail" do

            it "should include the correct message body" do
              @event_payload.message_body(:html).should include('Hebrew: A Language Course Primer')
            end

            it "should include the right subject" do
              @event_payload.subject.should include("amazon")
            end

            it "should have a valid user" do
              User.find_by_email(@event_payload.sender_email).should_not be_nil
            end

            it "should be sent to the right email" do
              @event_payload.recipient_emails.any?{ |s| s.casecmp("neworder@#{OrderMailer.site_url}") == 0 }.should be_true
            end

          end

          describe "mail to the user" do
            describe "success" do
              before(:each) do
                @ic = InboxController.new
              end

              describe "multi mail" do
                before(:each) do
                  inbound_multi_products = webhook_example_event('inbound_amazon_multi_products_032014')
                  @event_payload_multi_products = Mandrill::WebHook::EventDecorator[inbound_multi_products]
                end

                it "should send several mails" do
                  lambda do
                    @ic.handle_inbound(@event_payload_multi_products)
                  end.should change(ActionMailer::Base.deliveries, :size).by(2)
                end

                it "should add order" do
                  lambda do
                    @ic.handle_inbound(@event_payload_multi_products)
                  end.should change(Order, :count).by(2)
                end
              end

              it "should send a mail" do
                @ic.handle_inbound(@event_payload)
                ActionMailer::Base.deliveries.should_not be_empty
              end

              it "should add order" do
                ic = InboxController.new
                lambda do
                  ic.handle_inbound(@event_payload)
                end.should change(Order, :count).by(1)

              end

              it "should send exctly on email per user" do
                lambda do
                  @ic.handle_inbound(@event_payload)
                end.should change(ActionMailer::Base.deliveries, :size).by(1)
              end

              it "should add new order with the right parameters" do
                @ic.handle_inbound(@event_payload)
                @user.reload
                order = Order.find_by_price("$12.64")
                order.should_not be_nil
                order.user.should == @user
                order.image_remote_url.should == "http://ecx.images-amazon.com/images/I/51YU+4P95RL._SCLZZZZZZZ__SY115_SX115_.jpg"
              end

              describe "mail content" do
                before(:each) do
                  @ic.handle_inbound(@event_payload)
                  @mail = ActionMailer::Base.deliveries.last
                end

                it "should send the right subject" do
                  @mail.subject.should == 'Order Tracking - New order received'
                end

                it "should send the right 'from field'" do
                  site_url = OrderMailer.site_url
                  @mail.from.should include("do-not-replay@#{site_url}")
                end

                it "should send the email to the right user" do
                  @ic.handle_inbound(@event_payload)
                  @mail = ActionMailer::Base.deliveries.last
                  @mail.to.size.should == 1
                  @mail.to.should include(@user.email)
                end

                it "should contain relevant message" do
                  @mail.body.encoded.should include("Price: $12.64")
                  @mail.body.encoded.should =~ /Description:  Hebrew: A Language Course Primer/
                  @mail.body.encoded.should include(CGI::unescapeHTML("http://www.amazon.com/gp/r.html?R=3VC8RMQ02WB49&C=3T5MUR8F9569O&H=XQ2WCXRRAXA9QEG4EXYARQIDFZ4A&T=C&U=http%3A%2F%2Fwww.amazon.com%2Fdp%2F0874414636%2Fref%3Dpe_385040_30332200_pe_309540_26725410_item"))
                end
              end
            end
          end
        end
      end
    end

    describe "mail forwarding" do
      before(:each) do
        @user = FactoryGirl.create(:user)
      end

      describe "info" do
        before(:each) do
          inbound_mail = webhook_example_event('inbound_info')
          @event_payload = Mandrill::WebHook::EventDecorator[inbound_mail]
        end

        it "should success in sending" do
          forward_mail = "ordertrk@gmail.com"
          @event_payload['msg']['to'] = [ { :email => forward_mail } ]
          @event_payload['msg']['headers']['To'] = forward_mail
          mandrill = Mandrill::API.new(ActionMailer::Base.smtp_settings[:password])
          result = mandrill.messages.send(@event_payload['msg'])
          result.first["status"].should == "sent"
        end
      end
    end
  end
end

