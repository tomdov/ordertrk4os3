require 'spec_helper'

describe TrackUpdatesController do

  def tracking_update_webhooks_examples_path
    Pathname.new(File.dirname(__FILE__)).join('..','fixtures','aftership_webhook_examples')
  end

  def get_aftership_webhook_example_as_json(example_file_name)
    JSON.parse((tracking_update_webhooks_examples_path.join(example_file_name)).read)
  end

  describe "POST create" do

    describe "order update with checkpoints" do
      before(:each) do
        attr = {:description   => "example desc",
                 :site          => "Ebay",
                 :purchase_date => Date.current,
                 :status        => "Ordered",
                 :status_date   => Date.current,
                 :notes         => "Bla bla bla",
                 :package_tracking_attributes => {:tracking_number => "906587618687",}}
        @params_attr = { :sec_tok => YAML.load_file('config/aftership.yml')[Rails.env]["security_token"] }
        @user = FactoryGirl.create(:user)
        @user.orders.build(attr)
        @user.save
        @tracking_update = get_aftership_webhook_example_as_json('tracking_update_example_2_delivery.json')
      end

      describe "check security key" do
        it "should disallow requests without the correct key" do
          post :create, :track_update => @tracking_update, :sec_tok => "wrong_token"
          tracking_number = @user.orders.first.package_tracking.tracking_number
          pkg_trk = PackageTracking.find_by_tracking_number(tracking_number)
          pkg_trk.aftership_checkpoints.should_not == 12
          pkg_trk.order.package_tracking.aftership_checkpoints.should_not == 12
        end

        it "should allow requests with correct key" do
          post :create, :track_update => @tracking_update, :sec_tok => @params_attr[:sec_tok]
          tracking_number = @user.orders.first.package_tracking.tracking_number
          pkg_trk = PackageTracking.find_by_tracking_number(tracking_number)
          pkg_trk.aftership_checkpoints.should == 12
          pkg_trk.order.package_tracking.aftership_checkpoints.should == 12
        end
      end

      it "should add the checkpoints to the order" do
        post :create, :track_update => @tracking_update, :sec_tok => @params_attr[:sec_tok]
        tracking_number = @user.orders.first.package_tracking.tracking_number
        pkg_trk = PackageTracking.find_by_tracking_number(tracking_number)
        order = pkg_trk.order
        order.notes.should contain("City: LA COURNEUVE, PARIS")
        order.notes.should contain("City: CHILLY MAZARIN, PARI")
        order.notes.should contain("Country: FR")
      end

      it "should change the number of checkpoints in the package tracking object" do
        post :create, :track_update => @tracking_update, :sec_tok => @params_attr[:sec_tok]
        tracking_number = @user.orders.first.package_tracking.tracking_number
        pkg_trk = PackageTracking.find_by_tracking_number(tracking_number)
        pkg_trk.aftership_checkpoints.should == 12
        pkg_trk.order.package_tracking.aftership_checkpoints.should == 12
      end
    end


    describe "order update with status" do
      before(:each) do
        attr = {:description   => "example desc",
                :site          => "Ebay",
                :purchase_date => Date.current,
                :status        => "Ordered",
                :status_date   => Date.current,
                :notes         => "Bla bla bla",
                :package_tracking_attributes => {:tracking_number => "906587618687",}}
        @params_attr = { :sec_tok => YAML.load_file('config/aftership.yml')[Rails.env]["security_token"] }
        @user = FactoryGirl.create(:user)
        @user.orders.build(attr)
        @user.save
      end

      it "should update the order status to delivered" do
        tracking_update = get_aftership_webhook_example_as_json('tracking_update_example_2_delivery.json')
        @user.orders.first.status.should == "Ordered"
        post :create, :track_update => tracking_update, :sec_tok => @params_attr[:sec_tok]
        @user.orders.first.status.should == "Delivered"
      end

      it "should update the order status to shipped" do
        tracking_update = get_aftership_webhook_example_as_json('tracking_update_example_1.json')
        @user.orders.first.status.should == "Ordered"
        post :create, :track_update => tracking_update, :sec_tok => @params_attr[:sec_tok]
        @user.orders.first.status.should == "Shipped"
      end

      describe "mails send" do
        before(:each) do
          @tracking_update = get_aftership_webhook_example_as_json('tracking_update_example_1.json')
        end
        it "should send mail" do
          lambda do
            post :create, :track_update => @tracking_update, :sec_tok => @params_attr[:sec_tok]
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send the email to the right user" do
          post :create, :track_update => @tracking_update, :sec_tok => @params_attr[:sec_tok]
          @mail = ActionMailer::Base.deliveries.last
          @mail.to.size.should == 1
          @mail.to.should include(@user.email)
        end

        describe "mail content" do
          before(:each) do
            post :create, :track_update => @tracking_update, :sec_tok => @params_attr[:sec_tok]
            @mail = ActionMailer::Base.deliveries.last
          end

          it "should send the right subject" do
            @mail.subject.should == 'Order Tracking - Order status changed notification'
          end

          it "should send the right 'from field'" do
            @mail.from.should include("do-not-replay@#{OrderMailer.site_url}")
          end

          it "should contain relevant message" do
            @mail.body.encoded.should include("Previous status: Ordered\r\nNew status: Shipped")
          end
        end
      end
    end
  end
end
