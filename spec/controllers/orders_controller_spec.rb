require 'spec_helper'

describe OrdersController do
  render_views

  describe "get 'new'" do

    it "should response to new, create and destroy" do
      @user = test_sign_in(FactoryGirl.create(:user))
      get :new
      response.should be_success
    end

    it "should have the right title" do
      @user = test_sign_in(FactoryGirl.create(:user))
      get :new
      response.should have_selector("title", :content => "New order")
    end

    describe "default notification days" do

      it "should NOT have default days for notifications" do
        @user = test_sign_in(FactoryGirl.create(:user))
        get :new
        response.should have_selector("input#order_days_notification",        :value => "0" )
        response.should_not have_selector("input#order_days_notification_enable", :checked => "checked" )
      end

      it "should have default days for notifications" do
        @user = test_sign_in(FactoryGirl.create(:user))
        @user.update_attribute(:default_days_notification, 1)
        get :new
        response.should have_selector("input#order_days_notification",        :value => "1" )
        response.should have_selector("input#order_days_notification_enable", :checked => "checked" )
      end

      it "should have default days for notifications from bookmarklet" do
        @user = test_sign_in(FactoryGirl.create(:user))
        @user.update_attribute(:default_days_notification, 1)
        get :save_order
        response.should have_selector("input#order_days_notification",        :value => "1" )
        response.should have_selector("input#order_days_notification_enable", :checked => "checked" )
      end

    end

    describe "tracking info" do

      describe "tracking disabled" do
        before (:each) do
          @user = test_sign_in(FactoryGirl.create(:user))
        end

        it "should hide tracking number option" do
          get :new
          response.should_not have_selector("label", :content => "Tracking Number")
        end

        it "should hide tracking carrier option" do
          get :new
          response.should_not have_selector("label", :content => "Shipping carrier")
        end
      end

      describe "tracking allowed" do
        before (:each) do
          @user = test_sign_in(FactoryGirl.create(:user))
          @user.update_attribute(:allow_tracking_info, true)
        end

        it "should show tracking number option" do
          get :new
          response.should have_selector("label", :content => "Tracking Number")
        end

        it "should show tracking carrier option" do
          get :new
          response.should have_selector("label", :content => "Shipping carrier")
        end
      end

    end

  end

  describe "post 'create'" do

    before(:each) do
      @attr = {:description   => "example desc",
               :site          => "Ebay",
               :purchase_date => Date.current,
               :status        => "Ordered",
               :status_date   => Date.current,
               :notes         => "Bla bla bla"}
      @user = FactoryGirl.create(:user)

    end

    it "should redirect to the user feed" do
      test_sign_in(@user)
      post :create, :order => @attr

      response.should redirect_to(@user)
    end

    it "should redirect to order saved message if came from bookmarklet" do
      test_sign_in(@user)
      post :create, :order => @attr.merge(:lightbox => true)

      response.should redirect_to(orders_order_saved_path)
    end

    describe "failure" do

      it "should not add order if user isn't logged on" do
        lambda do
          post :create, :order => @attr
        end.should_not change(Order, :count)
      end

      it "should not add order if site is missing" do

        test_sign_in(@user)

        lambda do
          post :create, :order => @attr.merge(:site => "")
        end.should_not change(Order, :count)
      end

      it "should not add order if description is missing" do

        test_sign_in(@user)

        lambda do
          post :create, :order => @attr.merge(:description => "")
        end.should_not change(Order, :count)

      end
    end

    describe "success" do


      it "should add order to the DB" do
        test_sign_in(@user)
        lambda do
          post :create, :order => @attr
        end.should change(Order, :count).by(1)
      end

      describe "package_tracking" do
        before(:each) do
          @attr_pkg_trk = @attr.merge(:package_tracking_attributes => {:tracking_number => "123",
                                                       :tracking_carrier => "UUPS"})
        end
        it "should add package_tracking to the DB" do
          test_sign_in(@user)
          lambda do
            post :create, :order => @attr_pkg_trk
          end.should change(PackageTracking, :count).by(1)
        end

        it "should store the correlated info of package_tracking" do
          test_sign_in(@user)
          post :create, :order => @attr_pkg_trk
          ord = Order.last.package_tracking
          ord.tracking_number.should == '123'
          ord.tracking_carrier.should == 'UUPS'
        end
      end

    end

  end

  describe "get save_order" do
    before(:each) do
      @attr = { :source => 'bookmarklet',
                :description => 'thisdescription',
                :url => 'www.somesite.com',
                :price => '4.99USD' }
      @user = FactoryGirl.create(:user)
    end

    describe "not authenticated user" do

      it "should redirect to sign in if the user isn't authenticated" do
        get :save_order,:params => @attr
        response.should redirect_to signin_lightbox_path
      end
    end

    describe "authenticated user" do
      it "should show the save_order screen" do
        test_sign_in(@user)
        get :save_order,:params => @attr
        response.should be_success
      end
    end

  end

  describe "delete 'destroy'" do
    before(:each) do
      @attr = {:description   => "example desc",
               :site          => "Ebay",
               :purchase_date => Date.current,
               :status        => "Ordered",
               :status_date   => Date.current,
               :notes         => "Bla bla bla"}
      @user = test_sign_in(FactoryGirl.create(:user))
      @order = @user.orders.create(@attr)
    end

    it "should delete the order" do
      lambda do
        delete :destroy, :id => @order
      end.should change(Order, :count).by(-1)
    end

  end

  describe "get 'edit'" do
    before(:each) do
      @attr = {:description   => "example desc",
               :site          => "Ebay",
               :purchase_date => Date.current,
               :status        => "Ordered",
               :status_date   => Date.current,
               :notes         => "Bla bla bla"}
      @user = test_sign_in(FactoryGirl.create(:user))
      @other_user = FactoryGirl.create(:user, :email => FactoryGirl.generate(:email))
      @order = @user.orders.create(@attr.merge(:site => "Alibaba"))
    end

    it "should be successful" do
      get :edit, :id => @order
      response.should be_success
    end

    it "should have the right title" do
      get :edit, :id => @order.id
      response.should have_selector('title', :content => "Edit order")
    end

    it "should not be available for user to edit another user's order" do
      other_order = @other_user.orders.create(@attr)
      get :edit, :id => other_order.id
      response.should redirect_to @user
      flash[:error].should =~ /access denied/i

    end
  end

  describe "put 'update'" do
    before(:each) do
      @attr = {:description                 => "example desc",
               :site                        => "Ebay",
               :purchase_date               => Date.current,
               :status                      => "Shipped",
               :status_date                 => Date.current,
               :notes                       => "Bla bla bla",
               :days_notification_enable    => true,
               :days_notification           => 20,
               :days_notification_sent => true}

      @user = test_sign_in(FactoryGirl.create(:user))
      @order = @user.orders.create(@attr)
      @new_attr = {:description   => "new desc",
                   :site          => "new_Ebay",
                   :purchase_date => Date.current + 2.days,
                   :status        => "Delivered",
                   :status_date   => Date.current + 2.days,
                   :notes         => "new",
                   :days_notification_enable    => true,
                   :days_notification           => 20,
                   }
    end

    it "should save successfully" do
      put :update, :order => @new_attr, :id => @order
      flash[:success].should =~ /Order saved successfuly/i
    end
    it "should redirect to the user" do
      put :update, :order => @new_attr, :id => @order
      response.should redirect_to @user
    end

    it "should update the order" do
      put :update, :order => @new_attr, :id => @order

      order = assigns(:order)
      @order.reload
      order.description.should      == @order.description
      order.site.should             == @order.site
      order.purchase_date.should    == @order.purchase_date
      order.status.should           == @order.status
      order.status_date.should      == @order.status_date
      order.notes.should.should     == @order.notes
    end

    it "should uncheck the notification sent if days has changed" do
      put :update, :order => @new_attr.merge(:days_notification => 21), :id => @order
      @order.reload
      @order.days_notification_sent.should be_false
    end

    it "should uncheck the notification sent if days_notification_enable checkbox has unchecked" do
      put :update, :order => @new_attr.merge(:days_notification_enable => false), :id => @order
      @order.reload
      @order.days_notification_sent.should be_false
    end

  end

  describe "GET complete" do

    before(:each) do
      @attr = {:description   => "example desc",
               :site          => "Ebay",
               :purchase_date => Date.current,
               :status        => "Ordered",
               :status_date   => Date.current,
               :notes         => "Bla bla bla"}
      @user = test_sign_in(FactoryGirl.create(:user))
      @order = @user.orders.create(@attr.merge(:site => "Alibaba"))
    end


    it "should mark order as picked" do
      get :complete, :id => @order
      @order.reload
      @order.status_date.should == Date.current
      @order.status.should == "Picked"
    end


  end

end
