require 'spec_helper'

describe SubscribersController do

  describe "POST create" do
    describe "success" do
      before(:each) do
        @attr = {:email => "valid@mail.com"}
      end

      it "should redirect to thank you page" do
        post :create, :subscriber => @attr
        response.should redirect_to beta_thank_you_path
      end

      it "should default of false in signup mail sent" do
        subscriber = Subscriber.create(@attr)
        subscriber.signup_mail_sent.should be_false
      end

      it "should save the email in the DB" do
        lambda do
          post :create, :subscriber => @attr
        end.should change(Subscriber, :count).by(1)
      end

      it "should have unsubscribe key" do
        post :create, :subscriber => @attr
        Subscriber.find_by_email(@attr[:email]).key.should_not be_nil
      end

      describe "send beta subscribe email" do

        before(:each) do
          @welcome_mail = ActionMailer::Base.deliveries.last
        end
        it "should send a mail" do
          post :create, :subscriber => @attr
          ActionMailer::Base.deliveries.should_not be_empty
        end

        it "should send exctly on email per user" do
          lambda do
            post :create, :subscriber => @attr
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send the right subject" do
          post :create, :subscriber => @attr
          @welcome_mail.subject.should == 'Thank you for subscribing Order Tracking Site'
        end

        it "should send the right 'from field'" do
          post :create, :subscriber => @attr
          site_url = UserMailer.site_url
          @welcome_mail.from.should include("do-not-replay@#{site_url}")
        end

        it "should send the email to the right email" do
          post :create, :subscriber => @attr
          @welcome_mail.to.size.should == 1
          @welcome_mail.to.should include(@attr[:email])
        end

        it "should contain subscribing message" do
          post :create, :subscriber => @attr
          @welcome_mail.body.encoded.should =~ /We're happy that you've signed up to the beta list of #{UserMailer.site_url}!/
        end


      end
    end

    describe "failure" do
      before(:each) do
        @attr = {:email => "invalid@@mail.com"}
      end

      it "should show flash error message" do
        post :create, :subscriber => @attr
        flash[:error].should =~ /The E-Mail you've enter is illegal/i
      end
    end

  end

  describe "GET 'unsubscribe'" do
    describe "success" do
      before(:each) do
        @subscriber = FactoryGirl.create(:subscriber)
      end
      it "should redirect to root path" do
        get :unsubscribe, :id => @subscriber, :un => @subscriber.key
        response.should redirect_to root_path
      end

      it "should show success message" do
        get :unsubscribe, :id => @subscriber, :un => @subscriber.key
        flash[:success].should =~ /You were successfully removed/i
      end

      it "should unsubscribe the subscriber" do
        get :unsubscribe, :id => @subscriber, :un => @subscriber.key
        Subscriber.find_by_id(@subscriber).subscribed.should be_false
      end

      it "should match the key" do
        Subscriber.find_by_id(@subscriber).key.should == @subscriber.key
      end
    end

    describe "failure" do
      before(:each) do
        @subscriber = FactoryGirl.create(:subscriber)
        @subscriber.key = 'wrong_key'
      end
      it "should redirect to root path" do
        get :unsubscribe, :id => @subscriber, :un => @subscriber.key
        response.should redirect_to root_path
      end

      it "should not show success message" do
        get :unsubscribe, :id => @subscriber, :un => @subscriber.key
        flash[:success].should_not =~ /You were successfully removed/i
      end

      it "should not match the key" do
        Subscriber.find_by_id(@subscriber).key.should_not == @subscriber.key
      end

      it "should not unsubscribe the subscriber" do
        get :unsubscribe, :id => @subscriber, :un => @subscriber.key
        Subscriber.find_by_id(@subscriber).subscribed.should be_true
      end

    end
  end
end
