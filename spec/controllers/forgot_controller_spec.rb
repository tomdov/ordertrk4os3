require 'spec_helper'

describe ForgotController do
  render_views

  describe "GET 'new'" do

    it "should be success" do
      get :new
      response.should be_success
    end
  end

  describe "GET 'show'" do

    before(:each) do
      @attr = {:id => '1'}
    end

    it "should be success" do
      get :show, :id => @attr
      response.should be_success
    end

    it "should display email, pass, and confirmation fields" do
      get :show, :id => @attr
      response.should have_selector('div.field.email')
      response.should have_selector('input', :placeholder => 'Email')
      response.should have_selector('div.field.password')
      response.should have_selector('input', :placeholder => 'Password')
      response.should have_selector('div.field.confirmation')
      response.should have_selector('input', :placeholder => 'Confirm')
    end

    it "should have the right title" do
      get :show, :id => @attr
      response.should have_selector('title', :content => "Forgot password")
    end
  end

  describe "POST 'create'" do
    describe "failure" do
      before(:each) do
        @attr = { :email => "" }
      end

      it 'should re-render the new page' do
        post :create, :user => @attr
        response.should render_template('new')
      end

      it 'should have an error message' do
        post :create, :user => @attr
        flash.now[:error].should =~ /invalid/i
      end

      it 'should have the "forgot password" title after failure' do
        post :create, :user => @attr
        response.should have_selector('title', :content => 'Forgot password')
      end
    end

    describe "success" do
      before(:each) do
        @user = FactoryGirl.create(:user)
        @attr = { :email => @user.email }
      end

      it "should redirect to home page" do
        post :create, :user => @attr
        response.should redirect_to(root_path)
      end

      it "should show success flash" do
        post :create, :user => @attr
        flash.now[:success].should =~ /mail with instructions/i
      end

      it "should save the key in the ForgotKey's record" do
        @user.forgot_key.should be_nil
        post :create, :user => @attr
        @user.reload
        @user.forgot_key.key.length.should > 128
      end

      describe "sending reset mail" do
        before(:each) do
          @mail = ActionMailer::Base.deliveries.last
        end
        it "should send mail" do
          post :create, :user => @attr
          ActionMailer::Base.deliveries.should_not be_empty
        end

        it "should have the right subject" do
          post :create, :user => @attr
          @mail.subject.should == "Order Tracking - Reset password instructions"
        end

        it "should send to the right user" do
          post :create, :user => @attr
          @mail.to.size.should == 1
          @mail.to.should include(@attr[:email])
        end

        it "should send only one mail" do
          lambda do
            post :create, :user => @attr
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should have the right body" do
          post :create, :user => @attr
          @user.reload
          ActionMailer::Base.deliveries.last.body.encoded.should =~ eval("/" + @user.forgot_key.key + "/i")
        end

        it "should have the right from" do
          site_url = UserMailer.site_url
          @mail.from.should include("do-not-replay@#{site_url}")
        end

      end
    end
  end

  describe "PUT 'update'" do

    describe "success" do

      before(:each) do
        @user = FactoryGirl.create(:user)
        @attr = { :email => @user.email }
        @new_attr = { :id => @user.id,
                      :email => @user.email,
                      :password => "1234",
                      :password_confirmation => "1234"}
      end

      it "should change the password" do
        @user.has_password?("foobar").should be_true
        post :create, :user => @attr
        put :update, :user => @new_attr, :id => @user.forgot_key.key
        @user.reload
        @user.has_password?("1234").should be_true
      end

      it "should redirect to signin page" do
        post :create, :user => @attr
        put :update, :user => @new_attr, :id => @user.forgot_key.key
        response.should redirect_to signin_path
      end

      it "should display success message" do
        post :create, :user => @attr
        put :update, :user => @new_attr, :id => @user.forgot_key.key
        flash[:success].should =~ /password changed successfully/i
      end

      it "should remove the forgot_key from the user" do
        post :create, :user => @attr
        put :update, :user => @new_attr, :id => @user.forgot_key.key
        @user.reload
        @user.forgot_key.should be_nil
      end
    end

    describe "failure" do

      before(:each) do
        @user = FactoryGirl.create(:user)
        @attr = { :email => @user.email }
        @new_attr = { :id => @user.id,
                      :email => @user.email,
                      :password => "1234",
                      :password_confirmation => "1234"}
      end

      it "should display error message if email and key doesn't match" do
        post :create, :user => @attr
        put :update, :user => @new_attr.merge(:email => "some_other@email.com"), :id => @user.forgot_key.key
        flash[:error].should =~ /mismatch between link and email/i
        response.should redirect_to signin_path
      end

      it "should display error if password and confirmation aren't match" do
        post :create, :user => @attr
        put :update, :user => @new_attr.merge(:password => "12345"), :id => @user.forgot_key.key
        @user.reload
        @user.has_password?("foobar").should be_true
      end

      it "should remove the forgot_key from the user" do
        post :create, :user => @attr
        put :update, :user => @new_attr, :id => @user.forgot_key.key
        @user.reload
        @user.forgot_key.should be_nil
      end
    end
  end
end
