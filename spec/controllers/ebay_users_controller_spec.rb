require 'spec_helper'

describe EbayUsersController do
  #Will need true token, which is problem
  describe "POST create" do

    before(:each) do
      @attr = { :name => "user1", :token => "some token", :expiration => Time.now }
      @user = test_sign_in(FactoryGirl.create(:user))
    end

    it "should create new account" do
      lambda do
        post :create, :ebay_user => @attr
      end.should change(EbayUser, :count).by(1)


    end


  end

  describe "DELETE 'destroy'" do
    before(:each) do
      @attr = { :name => "user1", :token => "some token", :expiration => Time.now }
      @user = test_sign_in(FactoryGirl.create(:user))
      @ebay_user = @user.ebay_users.create(@attr)
    end

    it "should destroy the ebay account" do
      lambda do
        delete :destroy, :id => @ebay_user
      end.should change(EbayUser, :count).by(-1)
    end
  end

end
