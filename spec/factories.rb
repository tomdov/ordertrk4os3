require 'factory_girl'

FactoryGirl.define do
  factory :user do
    name "Tom TT"
    email "tt.tt@tt.net"
    password "foobar"
    password_confirmation "foobar"
  end
end

FactoryGirl.define do
  sequence :email do |n|
  "person-#{n}@example.com"
  end
end

#FactoryGirl.define do
#  factory :micropost do
#    content "Hi There"
#    association :user
#  end
#end

FactoryGirl.define do
  factory :order do
    description "Order"
    site        "Buy4Me"
    status      "Ordered"
    purchase_date Date.current
    status_date Date.current
    association :user
  end
end

FactoryGirl.define do
  factory :ebay_user do
    name          "Nick"
    token         "thisistoken"
    expiration    DateTime.tomorrow
    association :user
  end
end

FactoryGirl.define do
  factory :package_tracking do
    tracking_number  "123456"
    tracking_carrier "UPS"
    association :order
  end
end

FactoryGirl.define do
  factory :subscriber do
    email  "email@mail.com"
    key "1234"
  end
end
