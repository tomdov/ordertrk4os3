require 'spec_helper'

module EbayAPI

describe "requests" do
  describe "set notification preferences" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    it "should ack success" do

      notification1 = Classes::NotificationEnable.new(
          :event_type => "ItemMarkedPaid", :event_enable => "Enable"
      )

      notification2 = Classes::NotificationEnable.new(
          :event_type => "ItemMarkedShipped", :event_enable => "Enable"
      )

      notification3 = EbayAPI::Classes::NotificationEnable.new(
          :event_type => "ItemAddedToWatchList", :event_enable => "Enable"
      )

      user_delivery_preferences = Classes::UserDeliveryPreferenceArray.new(
        :notification_enable => [notification1, notification2, notification3]
      )

      req_cred = Classes::RequesterCredentials.new(
          :ebay_auth_token => @api.auth_token
      )

      app_delivery_opt = Classes::ApplicationDeliveryPreferences.new( :application_enable => 'Enable', :alert_enable => 'Enable' )

      response = @api.set_notification_preferences( :requester_credentials => req_cred,
                                                    :user_delivery_preferences => user_delivery_preferences,
                                                    :application_delivery_preferences =>  app_delivery_opt )
      response.should be_success

    end

  end

  describe "get ebay official time" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    it "should be success" do
      req_cred = Classes::RequesterCredentials.new(
          :ebay_auth_token => @api.auth_token
      )

      response = @api.get_ebay_official_time( :requester_credentials => req_cred )
      response.should be_success
    end
  end

  describe "get notification preferences" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    it "should ack success" do

      req_cred = Classes::RequesterCredentials.new(
          :ebay_auth_token => @api.auth_token
      )

      response = @api.get_notification_preferences( :requester_credentials => req_cred, :preference_level => 'User' )
      response.should be_success
    end
  end

  describe "get client alerts auth token" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    it "should ack success" do

      req_cred = Classes::RequesterCredentials.new(
          :ebay_auth_token => @api.auth_token
      )

      response = @api.get_client_alerts_auth_token( :requester_credentials => req_cred )
      response.client_alerts_auth_token.should_not be_empty
      response.should be_success
    end
  end



  describe "login" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    it "should ack success" do
      req_cred = Classes::RequesterCredentials.new(
          :ebay_auth_token => @api.auth_token
      )

      auth_token_response = @api.get_client_alerts_auth_token( :requester_credentials => req_cred )
      auth_token_response.client_alerts_auth_token.should_not be_empty

      response = @api.login( :client_alerts_auth_token => CGI.escape(auth_token_response.client_alerts_auth_token) )

      #puts response.inspect
      response['Ack'].should == 'Success'

      user_alerts_response = @api.get_user_alerts( :session_id => CGI.escape(response['SessionID']), :session_data => CGI.escape(response['SessionData']))
      user_alerts_response['Ack'].should == 'Success'
      #puts "get alerts response = " + user_alerts_response.inspect

      alerts_array = get_alerts_array_from_response(user_alerts_response)
      handle_alerts(alerts_array)
      #
      #logout_response = @api.logout( :session_id => CGI.escape(response['SessionID']), :session_data => CGI.escape(user_alerts_response['SessionData']) )
      #logout_response['Ack'].should == 'Success'
      #puts "logout_response = " + logout_response.inspect
    end
  end


  describe "get session id" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    it "should ack success" do
      response = @api.get_session_id( :ru_name => YAML.load_file('config/ebay.yml')[Rails.env]["ru_name"])
      response.should be_success
    end
  end

  describe "fetch token" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    describe "failure" do

      it "should not succeed to fetch token if the user hasn't agreed the consent form" do
        response = @api.get_session_id( :ru_name => YAML.load_file('config/ebay.yml')[Rails.env]["ru_name"])
        response.should be_success

        begin
          response = @api.fetch_token( :session_id => response.session_id)
        rescue RequestError
        end
          #response.should_not be_success
      end
    end
  end

  describe "get single item" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    describe "success" do

      it "should get item" do
        response = @api.get_single_item(:item_id => '110139656021')
        response['Ack'].should == "Success"
        puts "response.ViewItemURLForNaturalSearch " + response['Item']['ViewItemURLForNaturalSearch']
        puts "respnse price " + response['Item']['ConvertedCurrentPrice']['Value'].to_s + " " +
                 response['Item']['ConvertedCurrentPrice']['CurrencyID'].to_s

      end
    end
  end

  describe "get orders" do
    before(:each) do
      @api = EbayAPI::Api.new
    end

    describe "success" do
      it "should get order successfully" do
        req_cred = Classes::RequesterCredentials.new(
            :ebay_auth_token => @api.auth_token
        )

        output_selector = "OrderID,ItemID,TransactionID,QuantityPurchased"

        response = @api.get_orders(:requester_credentials => req_cred, :output_selector => output_selector, :create_time_from => Time.now - (1 * 60 * 60 * 24), :create_time_to => Time.now, :order_role => "Buyer", :order_status => "Completed")

        response.orders.each do |order|
          order.transactions.each do |transaction|
            puts "Order " + order.order_id + " Transaction " + transaction.transaction_id + " Item " + transaction.item.item_id
          end
        end
        puts "Order"

        response.should be_success
      end

    end
  end

  #  FUNCTIONS

  def get_alerts_array_from_response(response)
    if response['ClientAlerts'] != nil
      response['ClientAlerts']['ClientAlertEvent']
    else
      nil
    end
  end

  def handle_alerts(alerts_array)
    if alerts_array.nil?
      return
    end

    alerts_array.each do |event|
      handle_event(event)
    end
  end

  def handle_event_item_marked_paid(event)
    puts event.to_s
  end

  def handle_event_item_marked_shipped(event)
    puts event.to_s
  end

  #    TODO remove
  #def handle_event_item_marked_watch_list(event)
  #  event_details = event['ItemAddedToWatchList']
  #  item_id = event_details['ItemID']
  #  title = event_details['Title']
  #  puts "item id = " + item_id
  #  puts "title = " + title
  #  puts "item marked for watch list event = " + event.inspect
  #end


  def handle_event(event)

    case event_type(event)
      when "ItemMarkedPaid"
        handle_event_item_marked_paid(event)
      when "ItemMarkedShipped"
        handle_event_item_marked_shipped(event)
      when "ItemAddedToWatchList"
        #    TODO remove
        #    handle_event_item_marked_watch_list(event)
    end
  end

  def event_type(event)
    event['EventType']
  end

end

end