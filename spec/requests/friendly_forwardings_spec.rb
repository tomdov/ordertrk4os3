require 'spec_helper'

describe "FriendlyForwardings" do
  it 'should forward to the requested user pages after signin' do
    user = FactoryGirl.create(:user)
    visit edit_user_path(user)
    fill_in :session_email,     :with => user.email
    fill_in :session_password,  :with => user.password
    click_button
    response.should render_template('users/edit')

    visit signout_path
    visit signin_path
    fill_in :session_email,     :with => user.email
    fill_in :session_password,  :with => user.password
    click_button
    response.should render_template('users/show')
  end

  it 'should forward to the requested order pages after signin' do
    user = FactoryGirl.create(:user)
    visit new_order_path
    fill_in :session_email,     :with => user.email
    fill_in :session_password,  :with => user.password
    click_button
    response.should render_template('orders/new')
  end

  it 'should forward to new order page after signin' do
    user = FactoryGirl.create(:user)
    visit orders_save_order_path
    fill_in :session_email,     :with => user.email
    fill_in :session_password,  :with => user.password
    click_button
    response.should render_template('orders/new')
  end
end
