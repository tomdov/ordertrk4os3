require 'spec_helper'

describe "Users" do

  describe 'signup' do

    describe 'failure' do
      it 'should not make a new user' do
        lambda do
          visit signup_path
          fill_in 'user_name',         :with => ""
          fill_in 'user_email',        :with => ""
          fill_in 'user_password',     :with => ""
          fill_in 'user_password_confirmation', :with => ""
          click_button
          response.should render_template('users/new')
          response.should have_selector('div#error_explanation')
        end.should_not change(User, :count)
      end

      it 'should not allow passwords to be unmatched' do
        user = FactoryGirl.create(:user)
        lambda do
          visit signup_path
          fill_in 'user_name',         :with => user.name
          fill_in 'user_email',        :with => user.email
          fill_in 'user_password',     :with => "1234"
          fill_in 'user_password_confirmation', :with => "12345"
        end.should_not change(User, :count)
      end
    end

    describe 'success' do
      it 'should make a new user' do
        lambda do
          visit signup_path
          fill_in 'user_name',         :with => 'Exmp'
          fill_in 'user_email',        :with => 'exmp@gmail.com'
          fill_in 'user_password',     :with => '1234'
          fill_in 'user_password_confirmation', :with => '1234'
          click_button
          response.should have_selector('div.flash.success',
                                        :content => 'Welcome')
          response.should render_template('users/show')

        end.should change(User, :count).by(1)
      end
    end
  end

  describe 'admin attribute' do
    before(:each) do
      @attr = { :name => 'new user', :email => "user@example.com",
                :password => "1234", :password_confirmation => "1234" }
      @user = User.create!(@attr)
    end

    it 'should respond to admin' do
      @user.should respond_to(:admin)
    end

    it 'should not be an admin by default' do
      @user.should_not be_admin
    end

    it 'should be convertiable to admin' do
       @user.toggle!(:admin)
      @user.should be_admin
    end
  end
end
