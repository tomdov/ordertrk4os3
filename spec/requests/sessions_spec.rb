require 'spec_helper'

describe "Sessions" do

  describe 'signin' do
    describe 'failure' do
      it 'should not sign the user in' do
        visit signin_path
        fill_in 'session_email',         :with => ""
        fill_in 'session_password',      :with => ""
        click_button
        response.should have_selector('div.flash.alert',
                                      :content => 'Invalid')
        response.should render_template('sessions/new')
      end
    end

    describe 'success' do
      it 'should sign a user in and out' do
        user = FactoryGirl.create(:user)
        visit signin_path
        fill_in 'session_email',        :with => user.email
        fill_in 'session_password',     :with => user.password
        click_button
        controller.should be_signed_in
        click_link "Sign out"
        controller.should_not be_signed_in

      end
    end
  end

end
