require "spec_helper"

describe OrderMailer do

  describe "send days notifications" do
    before(:each) do
      @attr = {:description                 => "example desc",
               :site                        => "Ebay",
               :purchase_date               => Date.current,
               :status                      => "Ordered",
               :status_date                 => Date.current,
               :notes                       => "Bla bla bla",
               :days_notification_enable    => true,
               :days_notification           => 20}

      @user = FactoryGirl.create(:user)
      @order = @user.orders.create(@attr)
    end

    it "should send the right subject" do
      @mail = OrderMailer.days_notification_mail(@order)
      @mail.subject.should == 'Order Tracking - Notification'
    end

    it "should send the right 'from field'" do
      @mail = OrderMailer.days_notification_mail(@order)
      site_url = OrderMailer.site_url
      @mail.from.should include("do-not-replay@#{site_url}")
    end

    it "should send the email to the right user" do
      @mail = OrderMailer.days_notification_mail(@order)
      @mail.to.size.should == 1
      @mail.to.should include(@order.user.email)
    end

    it "should contain notification message" do
      @mail = OrderMailer.days_notification_mail(@order)
      @mail.body.encoded.should =~ /It's been #{ ApplicationHelper.days_diff(@attr[:purchase_date], Date.current)} days since you ordered "#{@attr[:description]}"/
    end

    it "should contain link to the site if link was entered" do
      @link_order = @user.orders.create(@attr.merge(:site => "http://ebay.com"))
      @mail = OrderMailer.days_notification_mail(@link_order)
      @mail.body.encoded.should =~ /Link to the order's site:/
    end

    it "should contain link to 'order tracking'" do
      @order = @user.orders.create(@attr)
      @mail = OrderMailer.days_notification_mail(@order)
      @mail.body.encoded.should =~ eval("/" + OrderMailer.site_url + "/i")
    end

    it "should contain link to edit the order" do
      @order = @user.orders.create(@attr)
      @mail = OrderMailer.days_notification_mail(@order)
      @mail.body.encoded.should =~ eval("/" + 'change the order settings' + "/i")
    end

    it "should not contain link if link wasn't entered" do
      @mail = OrderMailer.days_notification_mail(@order)
      @mail.body.encoded.should_not =~ /Link to the order's site:/
    end

    it "should change the 'day_notification_sent' to true" do
      @mail = OrderMailer.days_notification_mail(@order)
      @order.reload
      @order.days_notification_sent.should be_true
    end

  end


  describe "enforce user order subscribe settings" do
    before(:each) do
      @attr = {:description                 => "example desc",
               :site                        => "Ebay",
               :purchase_date               => Date.current,
               :status                      => "Ordered",
               :status_date                 => Date.current,
               :notes                       => "Bla bla bla",
               :days_notification_enable    => true,
               :days_notification           => 20,
               :price                       => "50 USD"}

      @user = FactoryGirl.create(:user)
      @order = @user.orders.create(@attr)
    end

    describe "enabled" do
      it "should send days notifications mail" do
        lambda do
          OrderMailer.days_notification_mail(@order).deliver
        end.should change(ActionMailer::Base.deliveries, :size).by(1)
      end

      describe "with additional params" do
        before(:each) do
          @orderHash = {}
          @orderHash[:description] = @attr[:description]
          @orderHash[:link]        = @attr[:site]
          @orderHash[:qty]         = 1
          @orderHash[:amount]      = @attr[:price]
        end

        it "should send fail on add new order mail" do
          lambda do
            OrderMailer.new_order_failed(@order.user, @orderHash).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send non complient new order mail" do
          lambda do
            OrderMailer.new_order_failed_not_complient(@order.user, @orderHash).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send add new order from mail Mail" do
          lambda do
            OrderMailer.new_order_success(@order.user, @orderHash).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send order changed status mail" do
          @orderHash[:previous_state] = "ordered"
          @orderHash[:new_state] = "Shipped"
          lambda do
            OrderMailer.order_status_changed(@order.user, @orderHash).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end
      end
    end

    describe "disabled" do
      before(:each) do
        @user.orders_notifications = false
        @user.save
      end

      it "should not send days notifications mail" do
        lambda do
          OrderMailer.days_notification_mail(@order).deliver
        end.should_not change(ActionMailer::Base.deliveries, :size)
      end

      describe "with additional params" do
        before(:each) do
          @orderHash = {}
          @orderHash[:description] = @attr[:description]
          @orderHash[:link]        = @attr[:site]
          @orderHash[:qty]         = 1
          @orderHash[:amount]      = @attr[:price]
        end

        it "should not send fail on add new order mail" do
          lambda do
            OrderMailer.new_order_failed(@order.user, @orderHash).deliver
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end

        it "should not send non complient new order mail" do
          lambda do
            OrderMailer.new_order_failed_not_complient(@order.user, @orderHash).deliver
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end

        it "should not send add new order from mail Mail" do
          lambda do
            OrderMailer.new_order_success(@order.user, @orderHash).deliver
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end

        it "should not send order changed status mail" do
          @orderHash[:previous_state] = "ordered"
          @orderHash[:new_state] = "Shipped"
          lambda do
            OrderMailer.order_status_changed(@order.user, @orderHash).deliver
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end
      end
    end
  end

  describe "mail content" do
    before(:each) do
      @attr = {:description                 => "example desc",
               :site                        => "Ebay",
               :purchase_date               => Date.current,
               :status                      => "Ordered",
               :status_date                 => Date.current,
               :notes                       => "Bla bla bla",
               :days_notification_enable    => true,
               :days_notification           => 20,
               :price                       => "50 USD"}

      @user = FactoryGirl.create(:user)
      @order = @user.orders.create(@attr)

      @orderHash = {}
      @orderHash[:description] = @attr[:description]
      @orderHash[:link]        = @attr[:site]
      @orderHash[:qty]         = 1
      @orderHash[:amount]      = @attr[:price]
    end

    describe "footer" do
      it "should contain thanks line in days notification" do
        @mail = OrderMailer.days_notification_mail(@order)
        @mail.body.encoded.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.body.encoded.should =~ /subscribe settings/i
        @mail.body.encoded.should  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
        @mail.text_part.to_s.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.text_part.to_s.should =~ /subscribe settings/i
        @mail.text_part.to_s.should include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      end

      it "should contain thanks line in add new order mail" do
        @mail = OrderMailer.new_order_failed(@order.user, @orderHash)
        @mail.body.encoded.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.body.encoded.should =~ /subscribe settings/i
        @mail.body.encoded.should  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
        @mail.text_part.to_s.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.text_part.to_s.should =~ /subscribe settings/i
        @mail.text_part.to_s.should include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      end

      it "should contain thanks line in non complient new order mail" do
        @mail = OrderMailer.new_order_failed_not_complient(@order.user, @orderHash).deliver
        @mail.body.encoded.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.body.encoded.should =~ /subscribe settings/i
        @mail.body.encoded.should  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
        @mail.text_part.to_s.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.text_part.to_s.should =~ /subscribe settings/i
        @mail.text_part.to_s.should include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      end

      it "should contain thanks line in  add new order from mail Mail" do
        @mail = OrderMailer.new_order_success(@order.user, @orderHash)
        @mail.body.encoded.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.body.encoded.should =~ /subscribe settings/i
        @mail.body.encoded.should include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
        @mail.text_part.to_s.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.text_part.to_s.should =~ /subscribe settings/i
        @mail.text_part.to_s.should include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      end

      it "should contain thanks line in order changed status mail" do
        @orderHash[:previous_state] = "ordered"
        @orderHash[:new_state] = "Shipped"
        @mail = OrderMailer.order_status_changed(@order.user, @orderHash)
        @mail.body.encoded.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.body.encoded.should =~ /subscribe settings/i
        @mail.body.encoded.should  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
        @mail.text_part.to_s.should =~ /Thanks for organizing your orders with OrderTracking!/i
        @mail.text_part.to_s.should =~ /subscribe settings/i
        @mail.text_part.to_s.should include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      end

    end

  end
end
