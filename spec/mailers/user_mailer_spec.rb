require "spec_helper"

include ApplicationHelper

describe UserMailer do


  describe "mail content" do

    before(:each) do
      @user = FactoryGirl.create(:user)
    end

    it "should include subscribe settings line" do
      @mail = UserMailer.welcome_email(@user)
      @mail.body.encoded.should =~ /subscribe settings/i
      @mail.body.encoded.should  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      @mail.text_part.to_s.should =~ /subscribe settings/i
      @mail.text_part.to_s.should include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
    end

    it "should not include subscribe settings on subscriber_mail" do
      @mail = UserMailer.beta_subscriber_mail(@user.email)
      @mail.body.encoded.should_not =~ /subscribe settings/i
      @mail.body.encoded.should_not  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      @mail.text_part.to_s.should_not =~ /subscribe settings/i
      @mail.text_part.to_s.should_not include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
    end

    it "should not include subscribe settings on subscriber_welcome mail" do
      @mail = UserMailer.beta_subscriber_welcome_mail(@user.email)
      @mail.body.encoded.should_not =~ /subscribe settings/i
      @mail.body.encoded.should_not  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      @mail.text_part.to_s.should_not =~ /subscribe settings/i
      @mail.text_part.to_s.should_not include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
    end

    it "should not include subscribe settings on forgot_password_mail" do
      random_key = SecureRandom.hex(64).insert(16, "#{@user.id}")
      ForgotKey.create(:key => random_key, :user_id => @user.id)
      @user.reload
      @mail = UserMailer.forgot_password_mail(@user)
      @mail.body.encoded.should_not =~ /subscribe settings/i
      @mail.body.encoded.should_not  include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
      @mail.text_part.to_s.should_not =~ /subscribe settings/i
      @mail.text_part.to_s.should_not include("www.ordertrk.com/users/#{@user.id}/subscribe_settings")
    end
  end

  describe "enforce subscribe settings" do
    before(:each) do
      @user = FactoryGirl.create(:user)
    end

    describe "influenced by settings" do
      describe "enabled" do
        it "should send welcome mail" do
          lambda do
            UserMailer.welcome_email(@user).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send first feedback mail" do
          lambda do
            UserMailer.feedback_mail(@user).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end
      end

      describe "disabled" do
        before(:each) do
          @user.messages_notifications = false
          @user.save
        end

        it "should not send welcome mail" do
          lambda do
            UserMailer.welcome_email(@user).deliver
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end

        it "should not send first feedback mail" do
          lambda do
            UserMailer.feedback_mail(@user).deliver
          end.should_not change(ActionMailer::Base.deliveries, :size)
        end
      end
    end

    describe "not influenced by settings" do
      describe "enabled" do
        it "should send subscriber signup mail" do
          lambda do
            UserMailer.beta_subscriber_mail(@user.email).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send subscriber welcome mail" do
          lambda do
            UserMailer.beta_subscriber_welcome_mail(@user.email).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send forgot password mail" do
          random_key = SecureRandom.hex(64).insert(16, "#{@user.id}")
          ForgotKey.create(:key => random_key, :user_id => @user.id)
          @user.reload
          lambda do
            UserMailer.forgot_password_mail(@user).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end
      end

      describe "disabled" do
        it "should send subscriber signup mail" do
          lambda do
            UserMailer.beta_subscriber_mail(@user.email).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send subscriber welcome mail" do
          lambda do
            UserMailer.beta_subscriber_welcome_mail(@user.email).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end

        it "should send forgot password mail" do
          random_key = SecureRandom.hex(64).insert(16, "#{@user.id}")
          ForgotKey.create(:key => random_key, :user_id => @user.id)
          lambda do
            UserMailer.forgot_password_mail(@user).deliver
          end.should change(ActionMailer::Base.deliveries, :size).by(1)
        end
      end
    end
  end
end
